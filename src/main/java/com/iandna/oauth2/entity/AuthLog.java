package com.iandna.oauth2.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Builder
@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "auth_log")
public class AuthLog extends BaseTimeEntity {
    @Id // pk
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;      //자동증가 serial
    @Column(nullable = false, length = 50)
    private String uid;     // unique_id (email 주소)
    @Column(nullable = false, length = 1)
    private String action;

}
