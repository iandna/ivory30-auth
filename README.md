springboot 2.3.12.RELEASE
spring cloud : 2.2.5.RELEASE
mariadb 
client DB 처리, JWT 토큰 적용

#OAuth 로그인
http://localhost:8081/oauth/authorize?client_id=testClientId&redirect_uri=http://localhost:8081/oauth2/callback&response_type=code&scope=read

#token 발급
curl -X POST
'http://localhost:8081/oauth/token'
-H 'Authorization:Basic dGVzdENsaWVudElkOnRlc3RTZWNyZXQ='
-d 'grant_type=authorization_code'
-d 'code=9THJxB'
-d 'redirect_uri=http://localhost:8080/oauth2/callback'

#테스트 계정 생성
test/java/com/repo/UserJpaRepoTest
