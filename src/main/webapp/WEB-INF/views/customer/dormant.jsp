<%@ page language="java" contentType="text/html;charset=utf-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title>Document</title>

    <!-- 기본 베베캠 css -->
    <link href="/oauth/resources/assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="/oauth/resources/assets/css/font.css" rel="stylesheet" type="text/css" />
    <link href="/oauth/resources/assets/css/bebecam-common.css" rel="stylesheet" type="text/css" />
    <link href="/oauth/resources/assets/css/bebecamApply.css" rel="stylesheet" type="text/css" />
    <link href="/oauth/resources/assets/css/alert.css" rel="stylesheet" type="text/css" />
    <link href="/oauth/resources/assets/css/common_ui.css" rel="stylesheet" type="text/css" />
    <link href="/oauth/resources/assets/css/common.css" rel="stylesheet" type="text/css" />

    <!-- 휴면계정 페이지 전용 css -->
    <link href="/oauth/resources/assets/css/dormant.css" rel="stylesheet" type="text/css" />
</head>


<body class="sub">

    <div class="wrapper">
        <div class="stack">
            <div class="content active dormant">
                <div class="stack_item">
                    <div class="flex_start">
                        <h1>휴면 계정 해제 안내</h1>
                        <p class="guide">아이보리 서비스를 1년 이상 이용하지 않아<br />개인 정보 보호를 위해 아이보리 계정이<br />휴면 상태로 전환되었습니다.</p>
                        <p class="guide small">다음 버튼을 누르시면 아이보리 계정 휴면이 해제되고<br />서비스를 정상적으로 이용하실 수 있습니다.</p>
                        <div class="info_box">
                            <p>■ 휴면전환 계정 ｜ all*******@naver.com</p>
                            <p>■ 휴면전환 일자 ｜ 2023.11.28</p>
                        </div>
                    </div>
                    <div class="flex_end">
                        <div class="btn_group">
                            <button type="button" class="btn btn_submit">다음</button>
                            <button type="button" class="btn btn_cancel">취소</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>


</html>