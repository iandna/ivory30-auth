console.log('common.js on load');
var url = window.location.href;
var urlArr = url.split('/');
var SVR_URL = urlArr[0] + '//' + urlArr[1] + urlArr[2];
var USER_AGENT = navigator.userAgent;

function format_date(date) {
	if (date.length == 8) {
		date = date.substring(0, 4) + "." + date.substring(4, 6) + "."
				+ date.substring(6, 8);
	}
	return date;
}

function go_back() {
	var kcp_cert = $('#kcp_cert');
	console.log(typeof (kcp_cert));
	if (typeof (kcp_cert) == 'undefined'
			|| typeof (kcp_cert.css('display')) == 'undefined'
			|| kcp_cert.css('display') == 'none') {
		window.location.href = '/oauth/login';
	} else {
		document.getElementById("cert_info").style.display = "";
		document.getElementById("kcp_cert").style.display = "none";
	}
}

function go_back(return_url) {
	var kcp_cert = $('#kcp_cert');
	if (document.referrer
			.indexOf('i-vory.shop/Api/Member/Oauth2ClientCallback/sso') > 0) {
		history.go(-1);
		return;
	}
	if (typeof (kcp_cert) == 'undefined'
			|| typeof (kcp_cert.css('display')) == 'undefined'
			|| kcp_cert.css('display') == 'none') {
		if(return_url == null || typeof(return_url) == 'undefined' || return_url == '') {
			window.location.href = '/oauth/login';
			return;
		}
		window.location.href = '/oauth/login?return_url=' + return_url;
		return;
	} else {
		document.getElementById("cert_info").style.display = "";
		document.getElementById("kcp_cert").style.display = "none";
		return;
	}
}

function go_login() {
	if (document.referrer
			.indexOf('i-vory.shop/Api/Member/Oauth2ClientCallback/sso') > 0) {
		history.go(-1);
	} else {
		window.location.href = '/oauth/login?return_url='
				+ $('#return_url').val();
	}
}

function get_cookie(key) {
	var c_key = key + "=";
	var result = "";
	var cookie_arr = document.cookie.split(";");
	for (var i = 0; i < cookie_arr.length; i++) {
		if (cookie_arr[i][0] === " ") {
			cookie_arr[i] = cookie_arr[i].substring(1);
		}
		if (cookie_arr[i].indexOf(c_key) === 0) {
			result = cookie_arr[i].slice(c_key.length, cookie_arr[i].length);
			return result;
		}
	}
	return result;
}

/*
 * since : 2020-10-19 오후 9:31 func : 현재 페이지가 앱에서 진행중인지, 웹에서 진행중인지 체크
 */
function isAppRunning() {
	var UserAgent = navigator.userAgent;
	if (UserAgent.indexOf('ivory_3.0') > 0) {
		console.log('앱 접속증입니다.');
		return true;
	} else {
		console.log('앱 접속이 아닙니다. agent : ' + UserAgent);
		return false;
	}
}

/*
 * since : 2020-12-23 func : 안드로이드 외부브라우저 연결(딥링크) des : 플레이스토어
 */
function ivoryOuterBrowser(url) {

	if (!isAppRunning()) {
		location.href = url;
	}

	url = encodeURI(url);
	if (isAndroidDevice()) {
		window.Android.openLinkExternally(url);
	} else {
		location.href = "ivory://showOuterBrowser?" + url + "";
	}
}

/*
 * since : 2020-10-23 오후 2:53 func : 모바일 OS 구분 des : 안드로이드 or 아이폰 2021-10-08
 * 아이패드 기기구분 오류 수정('LIKE MAC OS X')
 */
function isAndroidDevice() {
	var bAndroidDevice = null;
	if (bAndroidDevice === null) {
		var ua = "" + navigator.userAgent;
		if (ua.toUpperCase().indexOf('IPHONE') != -1
				|| ua.toUpperCase().indexOf('IPAD') != -1
				|| ua.toUpperCase().match(/LIKE MAC OS X/i)) {
			bAndroidDevice = false;
		} else {
			bAndroidDevice = true;
		}
	}
	return bAndroidDevice;
}

/**
 * 만 14세 미만인지 체크
 * 
 * @param birthDate
 *            yyyyMMdd
 * @returns true:만14세미만 어린이
 */
function isChild(birthDate) {
	var today = new Date();
	var yyyy = today.getFullYear();
	var mm = today.getMonth() < 9 ? "0" + (today.getMonth() + 1) : (today
			.getMonth() + 1); // getMonth()
	var dd = today.getDate() < 10 ? "0" + today.getDate() : today.getDate();

	return parseInt(yyyy + mm + dd) - parseInt(birthDate) - 140000 < 0;
}

/*
 * since : 2020-09-28 오후 7:06 func : 네이티브에 데이터 로드 des : android key : Android,
 * IOS key : echossHybrid param : keyStr : map에서 불러올 key
 */
function ivoryLoadData(keyStr) {
	if (!isAppRunning())
		return;

	var value = null;
	try {
		if (isAndroidDevice()) {
			value = window.Android.echossLoadData(keyStr);
		} else {
			value = echossHybrid.echossLoadData(keyStr);
		}
	} catch (e) {
		console.log(e);
	}
	return value;
}

/*
 * since : 2020-12-02 오전 11:20 func : IOS 커스텀 스키마 호출 로그인정보 요청 des : 네이티브 웹뷰에
 * 로그인에 필요한 디바이스 정보를 요청
 */
function IOS_RequestDeviceInfo() {
	location.href = "ivory://deviceinfo";
}

/*
 * since : 2020-12-02 오전 11:23 func : 네이티브 웹뷰에서 디바이스 정보를 전달 des : 네이티브 -> HTML
 * 방향으로 호출
 */
function IOS_ResponseDeviceInfio(eqMdlNm, eqNo, pushId, verCode, buildCode) {
// setLoginInfo(eqMdlNm, eqNo, pushId, verCode + ' ' + buildCode);
	var result = btoa
	    (
			"eq_gbn_cd=I" + "&eq_mdl_nm=" + eqMdlNm + "&eq_no=" + eqNo + "&push_id=" + pushId
		    + "&ver_code=" + verCode + "&build_code=" + buildCode + "&url=/index.do"
		);
	$('#state').val(result);
	return;
}
/*
 * since : 2020-09-18 오후 3:43 func : 페이지 뒤로가기 des : 모든 (1)웹페이지와 (2)안드로이드 뒤로가기에서
 * 사용되는 공통함수 (1) - 뒤로가기 금지목록 생성되어있음 (프로세스에 제한이 생길여자가 있는 페이지들) (2) - 안드로이드 네이티브
 * backPress에서도 사용 됨 - 뒤로가기 금지목록 포함되어있음 (프로세스에 제한이 생길여자가 있는 페이지들)
 */

function pageBack() {

/* 뒤로 가기 금지 페이지 목록 확인 */
if (isBlockList())
return;

/* 뒤로가기 가능하다면 */
if (document.referrer) {

/* 리로드 된 케이스 */
if (document.referrer == location.href) {
  history.go(-2);
  ivoryDismissPage();
} else {
  history.back();
}

} else {
ivoryDismissPage();
}
}

/*
 * since : 2022-05-13 오전 11:10 func : 뒤로가기 금지 목록 체크(통합로그인) des : login(로그인/회원가입
 * 화면), profile(회원가입 프로필설정, welcome(회원가입 완료 화면)) join(회원가입), findId(아이디찾기) -
 * 본인인증창으로인해 go_back() 사용 return : true :뒤로 가기 금지 목록, false : 뒤로가기 금지목록 아님
 */
function isBlockList(url) {
/* oauth */
// 본인인증 뒤로가기(회원가입&아이디&비번찾기)
if (location.href.indexOf('oauth/customer/join.do') > 0) {
go_back($('#return_url').val());
return true;
}
if (location.href.indexOf('oauth/customer/findId.do') > 0) {
go_back($('#return_url').val());
return true;
}


if (location.href.indexOf('oauth/customer/profile.do') > 0)
return true;
if (location.href.indexOf('oauth/customer/welcome.do') > 0)
return true;

if (location.href.indexOf('oauth/login') > 0) {

if(navigator.userAgent.indexOf('ivory_3.0_second') >0) {
	   return true;
	} else {
  callBackAlert({title:'', content:'앱을 종료하시겠습니까?', btn:'확인'}, () => {
    console.log("dismissPage");

    if (!isAppRunning())
      return;

    if (isAndroidDevice()) {
      window.Android.exitApplication();
    } else {
      location.href = "ivory://exitApp?";
    }
  });
  return true;
	}
}

return false;
}

/*
 * since : 2020-10-19 오후 9:31 func : 새로운 탭을(웹뷰) 삭제함 des : 네이티브에 jsInterface 호출
 * param : url : 로딩할 페이지 주소
 */
function ivoryDismissPage() {
console.log("dismissPage");

if (!isAppRunning()) {
/* 웹 환경에선 일반적인 history back 처리 */
history.back();
return;
}

if (isAndroidDevice()) {
window.Android.dismissPage();
} else {
// echossHybrid.dismissPage();
location.href = "ivory://dismissPage?";
}
}

function deleteUserToken() {
	var todayDate = new Date();
	todayDate.setDate(todayDate.getDate());
	document.cookie = "UserToken=" + escape('') + "; path=/; expires=" + todayDate.toGMTString() + ";"
	document.cookie = "UserInfo=" + escape('') + "; path=/; expires=" + todayDate.toGMTString() + ";"
}