package com.iandna.oauth2.config.exception;

import org.springframework.security.core.AuthenticationException;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomAuthenticationException extends AuthenticationException {

	private String uid;
	private String password;
	private String code;
	
	public CustomAuthenticationException(String msg) {
		super(msg);
		// TODO Auto-generated constructor stub
	}
	
	public CustomAuthenticationException(String code, String msg) {
		super(msg);
		// TODO Auto-generated constructor stub
	}
	
	public CustomAuthenticationException(String msg, Throwable t) {
		super(msg, t);
	}
	
	public CustomAuthenticationException(String code, String msg, String uid, String password) {
		super(msg);
		// TODO Auto-generated constructor stub
	}
}
