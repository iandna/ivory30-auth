<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
    	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
        />

        <!-- css -->
        <link rel="stylesheet" href="/oauth/resources/assets/css/font.css?date=20221206111700" />
        <link rel="stylesheet" href="/oauth/resources/assets/css/common.css?date=20221206111700" />
        <link rel="stylesheet" href="/oauth/resources/assets/css/animation.css?date=20221206111700" />
        <link rel="stylesheet" href="/oauth/resources/assets/css/profile.css?date=20221206111700" />
        <link rel="stylesheet" href="/oauth/resources/assets/css/common_ui.css?date=20221206111700" />

        <title>Profile</title>
        
        <script type='text/javascript' src='/oauth/resources/js/common.js?date=20221206111700'></script>
		<script type='text/javascript' src='/oauth/resources/js/api.js?date=20221206111700'></script>
		<script type="text/javascript" src='/oauth/resources/js/kcp.js?date=20221206111700'></script>
		<link type="text/css" rel="stylesheet" href="/oauth/resources/css/common.css?date=20221206111700">
		<style>
            .loading_wrap {
                position: fixed;
                top: 0px;
                left: 0px;
                background-color: rgba(255, 255, 255, 1.0);
                width: 100%;
                height: 100%;
                z-index: 990;
            }
            .loading-circle {
                position: absolute;
                margin: auto;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
                width: 54px;
                height: 54px;
                animation: rotate_image 0.6s linear infinite;
                transform-origin: 50% 50%;
            }
            .loading_point {
                position: absolute;
                margin: auto;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
                width: 54px;
                height: 54px;
            }
            @keyframes rotate_image {
                100% {
                    transform: rotate(360deg);
                }
            }
        </style>
    </head>
    <body>
		<form name="form_auth" method="post" action="/oauth/customer/joinComplete.do" enctype="multipart/form-data">
	        <div class="container sub">
	            <!-- sub header -->
	            <div class="sub_header">
	                <a href="/">
	                    <img src="/oauth/resources/assets/images/left_black.svg" alt="뒤로가기" />
	                </a>
	            </div>
	            <!-- sub header -->
	
	            <!-- sub wrapper -->
	            <div class="sub_wrapper profile_wrapper">
	                <h2>프로필 설정</h2>
	
	                <div class="profile_box">
	                    <div class="profile_content">
	                        <div class="text_box">
	                            아이보리에서 사용하실<br /><span>닉네임</span>을
	                            입력해주세요.
	                        </div>
	
                            <div class="img_wrapper">

                                <button type="button" class="btn_delete_img" id="btnDeleteImg">
                                    <img src="/oauth/resources/assets/images/delete_white.svg" alt="" />
                                </button>
                                
                                <div class="img_box">
                                    <img
	                                src="/oauth/resources/assets/images/profile_base.svg"
	                                alt="프로필 이미지"
	                                id="previewImage"
                                    />
                                </div>
                            </div>
	
	                        <div class="btn_box">
	                            <div class="image_uploader button">
	                                <input type="file" id="profileImageUploader" name="profileImageUploader" />
	                                <label type="button" for="profileImageUploader">
	                                    <img
	                                        src="/oauth/resources/assets/images/icon_camera.svg"
	                                        alt="사진 올리기"
	                                    />
	                                    <span>사진 올리기</span>
	                                </label>
	                            </div>
	                            <button type="button" id="profileSelectCharacter">
	                                <img
	                                    src="/oauth/resources/assets/images/icon_character.svg"
	                                    alt="캐릭터 선택하기"
	                                />
	                                <span>캐릭터 선택하기</span>
	                            </button>
	                        </div>
	
	                        <div class="form_group profile_input_box">
	                            <input
	                                class="form_input"
	                                placeholder="닉네임을 입력해주세요.(2~10자)"
	                                type="text"
	                                name="nickname"
	                                id="nickname"
	                            />
	                            <span></span>
	                            <button type="button" class="input_icon delete">
	                                <img
	                                    src="/oauth/resources/assets/images/delete_black.svg"
	                                    alt="입력값 삭제"
	                                />
	                            </button>
	                            <button type="button" class="check_duplication" onclick="nickname_dup_check();">
	                                중복확인
	                            </button>
	                            <div class="error_message"></div>
	                        </div>
	                    </div>
	
	                    <button
	                        type="submit"
	                        class="block_btn"
	                        id="profileComplete"
	                        disabled="true"
	                        onclick="join_complete();"
	                    >
	                        프로필을 완성해주세요
	                    </button>
	                </div>
	            </div>
	            <!-- sub wrapper -->
	
	            <!-- profile modal -->
	            <div class="profile_modal" id="characterSelecter">
	                <div class="character_container">
	                    <button type="button" class="close" id="closeCharacter">
	                        <img src="/oauth/resources/assets/images/delete_black.svg" alt="닫기" />
	                    </button>
	                    <div class="character_preview">
	                        <img
	                            src="/oauth/resources/assets/images/character_01.png"
	                            alt="캐릭터"
	                            id="characterPreview"
	                        />
	                    </div>
	                    <ul class="character_list" id="characterList">
	                    </ul>
	
	                    <button
	                        type="button"
	                        class="block_btn"
	                        id="selectCharacter"
	                    >
	                        캐릭터 선택하기
	                    </button>
	                </div>
	
	                <div class="back"></div>
	            </div>
	            <!-- profile modal -->
	        </div>
	        <input type="hidden" name="csId" value="${ iParam.registId }" />
	        <input type="hidden" name="certDi" value="${ iParam.di_url }" />
	        <input type="hidden" name="certCi" value="${ iParam.ci_url }" />
	        <input type="hidden" name="password" value="${ iParam.registPassword }" />
	        <input type="hidden" name="name" value="${ iParam.name }" />
	        <input type="hidden" name="gender" value="${ iParam.gender }" />
	        <input type="hidden" name="birthDate" value="${ iParam.birthDate }" />
	        <input type="hidden" name="phoneNo" value="${ iParam.phoneNo }" />
	        <input type="hidden" name="agree1" value="${ iParam.agree1 }" />
	        <input type="hidden" name="agree2" value="${ iParam.agree2 }" />
	        <input type="hidden" name="agree3" value="${ iParam.agree3 }" />
	        <input type="hidden" name="agree4" value="${ iParam.agree4 }" />
	        <input type="hidden" name="agree5" value="${ iParam.agree5 }" />
	        <input type="hidden" name="agree6" value="${ iParam.agree6 }" />
	        <input type="hidden" name="agree7" value="${ iParam.agree7 }" />
	        <input type="hidden" name="agree8" value="${ iParam.agree8 }" />
	        <input type="hidden" name="agree9" value="${ iParam.agree9 }" />
	        <input type="hidden" name="veri_up_hash" value="${ iParam.veri_up_hash }" />
	        <input type="hidden" name="characterNo" id="characterNo" />
	        <input type="hidden" name="customerType" id="customerType" />
        </form>

		<div class="loading_wrap container hide">
            <img class="loading-circle" src="https://iandna.s3-ap-northeast-1.amazonaws.com/ivory3.0/common/loading-circle.png" alt="Loading..." />
            <img class="loading_point" src="https://iandna.s3-ap-northeast-1.amazonaws.com/ivory3.0/common/loading_point.png" alt="Loading..." />
        </div>
        <!-- jquery -->
        <script type="text/javascript" src="/oauth/resources/assets/js/jQuery.js"></script>

        <!-- common ui -->
        <script type="text/javascript" src="/oauth/resources/assets/js/common_ui.js"></script>
        <!-- common -->
        <script type="text/javascript" src="/oauth/resources/assets/js/common.js"></script>
        <script type="text/javascript">
	        window.onload = async function () {
	        	console.log(USER_AGENT);
	        	if(USER_AGENT.indexOf("jelly") != -1)
	            	$('#customerType').val("3");
	        	else
	        		$('#customerType').val("2");
	        }
        	make_character_box();
	        function join_complete() {
		    	var auth_form = document.form_auth;
		    	auth_form.action = SVR_URL + "/oauth/customer/joinComplete.do";
		    	$('.loading_wrap').removeClass("hide");
		    	return true;
		    }
	        
	        async function nickname_dup_check() {
	        	var data = {};
	        	var nickname = $('#nickname').val();
	        	if(nickname.length < 2) {
	        		onErrorMessage({message: '<br />두 글자 이상의 닉네임을 설정해야 합니다.', target: '#nickname'});
	        		return;
	        	}
	        	data = await callApi('/oauth/customer/nicknameDuplication.json', "post", { nickname : nickname });
	        	if(data.result == 'y'){
	        		onErrorMessageGreen({message: '<br />사용 가능한 닉네임입니다.', target: '#nickname'});
	        		$('#profileComplete').prop('disabled', false);
	        	}else {
	        		onErrorMessage({message: '<br />중복된 닉네임입니다.', target: '#nickname'});
	        	}
	        }
	        
	        $('#nickname').on("propertychange change keyup paste input", function() {
	        	$('#profileComplete').prop('disabled', true);
		    });
	        
	        function make_character_box() {
	        	for(var i = 1; i < 17; i++) {
	        		var html = "";
		        	html += "<li>";
		        	html += 	"<button type='button' onclick='set_character_val(" + i + ");'>";
		        	html += 		"<img src='/oauth/resources/assets/images/character_" + i + ".png' alt='캐릭터" + i + "' />";
		        	html += 	"</button>";
		        	html += "</li>";
		        	$('#characterList').append(html);
	        	}
	        }
	        
	        function set_character_val(num) {
	        	console.log(num);
	        	$('#characterNo').val(num);
	        }
        </script>
    </body>
</html>