package com.iandna.oauth2.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@Entity
@Getter
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@Table(name = "SNB_CUST_INFO")
@Setter
@ToString
public class Customer extends RegDateEntity implements UserDetails {
    @Id // pk
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(unique = true, name="CUST_NO", length = 61)
    private String uid;     // unique_id (email 주소)

    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="FRCS_CUST_NO", length = 60)
    private String csId;    //csId (AS-IS 고객 key로 일부사용)

    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="CERT_DI", length = 128)
    private String certDi;    //csId (AS-IS 고객 key로 일부사용)
    
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="CERT_CI", length = 128)
    private String certCi;    //csId (AS-IS 고객 key로 일부사용)
    
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(unique = true, name="CUST_SHOP_ID", length = 50)
    private String shopId;    //쇼핑몰id (이메일 @도메인3자리까지만)

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="PASSWORD", length = 100)
    private String password;    //암호화비밀번호

    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="CUST_MAIL_ADD", nullable = false, length = 60)
    private String email;    //이메일주소

    @Column(name="CUST_NAME", nullable = false, length = 70)
    private String name;        //고객명

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="CUST_GENDER", columnDefinition = "char(4)")
    private String gender;    //성별

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="CUST_BIRTH", length = 12)
    private String birthDate;    //생년월일(YYYYMMDD)

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="CUST_NICKNAME", length = 50)
    private String nickname;    //닉네임
    
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="CUST_PHONE_NO", length = 16)
    private String phoneNo;    //고객 전화번호

    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="CUST_GBN_CD", columnDefinition = "char(1) default '2'")
    @Builder.Default
    private String customerType = "2";    //회원구분
    
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="CUST_ST", columnDefinition = "char(1) default '1'")
    @Builder.Default
    private String stCd = "1";    //회원 상태

    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="PUSH_YN", columnDefinition = "char(1) default 'N'")
    @Builder.Default
    private String pushYn = "N";    // 푸시 동의 여부

    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="PUSH_ARM1", columnDefinition = "char(1) default '0'")
    @Builder.Default
    private String pushArm1 = "0";    // 푸시 배치에서 실제 쿼리하는 컬럼
    
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="CUST_IMG", length = 500)
    private String custImg;

    @OneToOne(fetch = FetchType.LAZY ,optional=false, cascade = CascadeType.ALL)
    @JoinColumn(name= "CUST_NO")
    @JsonIgnore
    private AgreeInfo agreeInfo;
    
    @OneToOne(fetch = FetchType.LAZY ,optional=false, cascade = CascadeType.ALL)
    @JoinColumn(name= "CUST_NO")
    @JsonIgnore
    private SnsAcsInfo snsAcsInfo;

    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="EQ_GBN_CD", columnDefinition = "char(1) default 'A'")
    @Builder.Default
    private String eqGbnCd = "A";    // 기기 OS 구분

    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="EQ_MDL_NM", length = 50)
    @Builder.Default
    private String eqMdlNm = "";    // 기기 모델명

    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="EQ_NO", length = 200)
    @Builder.Default
    private String eqNo = "";    // 기기번호

    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="VER_CD", length = 30)
    @Builder.Default
    private String verCd = "";    // 버전 코드

    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="BLD_CD", length = 30)
    @Builder.Default
    private String bldCd = "";    // 빌드 코드

    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="PUSH_ID", length = 200)
    @Builder.Default
    private String pushId = "";    // PUSH ID

    public Customer(String email) {
        this.email = email;
        this.csId = email;
    }
    
    public Customer() {
        onPrePersist();
    }

//    @ElementCollection(fetch = FetchType.EAGER)
    @Builder.Default
    @Transient
    private List<String> roles = new ArrayList<>();     //사용자 ROLE (ROLE_USER 단일사용)

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        //return this.roles.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
        return Collections.singletonList("ROLE_USER").stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Override
    public String getUsername() {
        return this.email;
    }

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Override
    public boolean isEnabled() {
        return true;
    }



}