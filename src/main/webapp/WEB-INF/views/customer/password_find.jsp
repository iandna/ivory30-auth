<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ include file="./auth_call.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="EUC-KR">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
	<title>i&na 패스워드 찾기</title>
	<script type='text/javascript' src='/oauth/resources/js/jquery-3.4.1.min.js'></script>
	<script type='text/javascript' src='/oauth/resources/js/common.js?date=20221206111700'></script>
	<script type='text/javascript' src='/oauth/resources/js/api.js?date=20221206111700'></script>
	<script type="text/javascript" src='/oauth/resources/js/kcp.js?date=20221206111700'></script>
	<link type="text/css" rel="stylesheet" href="/oauth/resources/css/common.css?date=20221206111700">
</head>
<body>
	<button type="button" onclick="go_back()">뒤로</button>
	<h1 id="guide_text">패스워드 찾기</h1>
	<br />

	<div align="center" id="cert_info">
		<form name="form_auth" method="post">
		<input type="email" name="csId" id="csId" placeholder="아이디를 입력해주세요." />
		
		<br />
		<!-- 인증 버튼 -->
		<img src="/oauth/resources/img/인증.PNG" onclick="check_customer_gbn();" />
		<!-- 본인인증 창 호출용 input. type="image"외 다른 것 일 때 <iframe>에 element append가 제대로 되지 않음 -->
		<input type="image" id="auth_call" onclick="return auth_type_check();" style="display: none;"/>
		
		<div id="password_reset_box" class="password_set_box hide">
			<input type="password" id="password" name="password" placeholder="변경할 비밀번호" />
			<br /><span>*8~16자 영문,숫자 및 특수문자 사용</span>
			<br /><input type="password" id="password_confirm" name="password_confirm" placeholder="변경할 비밀번호" />
			<br /><span id="password_confirm_guide"></span>
			<br /><button type="button" onclick="password_reset();">비밀번호 재설정</button>
		</div>
			
			<input type="hidden" name="call_gbn_cd"  value="3" />
			<!-- 본인인증 관련 parameters -->
	        <!-- 요청종류 -->
	        <input type="hidden" name="req_tx"       value="cert"/>
	        <!-- 요청구분 -->
	        <input type="hidden" name="cert_method"  value="01"/>
	        <!-- auth_conf.jsp -->
	        <input type="hidden" name="web_siteid"   value="<%= g_conf_web_siteid %>"/> 
	        <input type="hidden" name="site_cd"      value="<%= g_conf_site_cd %>" />               
	        <input type="hidden" name="Ret_URL"      value="<%= g_conf_Ret_URL %>" />
	        <input type="hidden" name="veri_up_hash" value="<%= veri_up_hash %>" id="veri_up_hash" />
	        <!-- 일련번호생성기 -->
	        <input type="hidden" name="ordr_idxx" class="frminput" value="<%=ordr_idxx%>" size="40" readonly="readonly" maxlength="40"/>
	        <!-- cert_otp_use 필수 ( 메뉴얼 참고)
	             Y : 실명 확인 + OTP 점유 확인 , N : 실명 확인 only
	        -->
	        <input type="hidden" name="cert_otp_use" value="Y"/>
	        <!-- 리턴 암호화 고도화 -->
	        <input type="hidden" name="cert_enc_use_ext" value="Y"/>
	        <input type="hidden" name="res_cd"       value=""/>
	        <input type="hidden" name="res_msg"      value=""/>
	        <!-- 본인확인 input 비활성화 -->
	        <input type="hidden" name="cert_able_yn" value=""/>
	        <!-- web_siteid 검증 을 위한 필드 -->
	        <input type="hidden" name="web_siteid_hashYN" value=""/>
	
	        <!-- 가맹점 사용 필드 (인증완료시 리턴)-->
	        <input type="hidden" name="name" id="name" /> 
	        <input type="hidden" name="birthDate" id="birthDate" /> 
	        <input type="hidden" name="phoneNo" id="phoneNo" /> 
	        <input type="hidden" name="gender" id="gender" /> 
	        
	        <input type="hidden" name="agree1" id="agree1" value="Y" /> 
	        <input type="hidden" name="agree2" id="agree2" value="Y" /> 
	        <input type="hidden" name="agree3" id="agree3" value="Y" /> 
	        
            <input type="hidden" id="param_opt_1" name="param_opt_1"  value="opt1"/> 
            <input type="hidden" name="param_opt_2"  value="opt2"/> 
            <input type="hidden" name="param_opt_3"  value="opt3"/> 
	        
	        <!-- 없으면 PASS 정보입력 창이 안나옴. -->
	        <input type="hidden" name="year" id="year" />
	        <input type="hidden" name="month" id="month" />
	        <input type="hidden" name="day" id="day" />
		</form>
	</div>
	<iframe id="kcp_cert" name="kcp_cert" width="100%" height="700" frameborder="0" scrolling="no" style="display:none"></iframe>
</body>
<script>
	async function check_customer_gbn() {
		var data = {};
		data = await callApi('/oauth/customer/getOne.json', "post", { csId:$('#csId').val() });
		console.log(data);
		switch(data.customerType) {
		case "1":
			
			var param = '{"id":"' + data.csId + '", "name":"' + data.name + '", "phoneNo":"' + data.phoneNo + '"}';
			console.log(param);
			// var url = "https://m.i-vory.shop/member/passwd/find_passwd_info.html?";
			var url = "https://m.i-vory.shop/member/passwd/find_passwd_info_lchtest.html?";
			//var url = "https://m.i-vory.shop/member/passwd/user_verification.html?";
			url += "param=" + btoa(escape(param));
			url += "&state=autotest"
			/* 
			url += "param1=" + data.csId;
			url += "&param2=" + data.name;
			url += "&param3=" + data.phoneNo;
			 */
			console.log('기존 회원입니다.');
			console.log(url);
			window.location.href = url;
			break;
		case "2":
			console.log('통합 회원입니다.');
			$('#param_opt_1').val($('#csId').val());
			$('#auth_call').click();
			break;
		default:
		}
	}
	
	$('#password_confirm').on("propertychange change keyup paste input", function(){
		if($('#password').val() != $('#password_confirm').val()) {
			$('#password_confirm_guide').text('*변경할 비밀번호와 일치하지 않습니다.');
		}else {
			$('#password_confirm_guide').text('');
		}
	});
	
	async function password_reset() {
		if($('#password').val() != $('#password_confirm').val()) {
			return false;
		}
		var data = {};
		data = await callApi('/oauth/customer/resetPassword.json', "post", { csId:$('#csId').val(), upHash:$('#veri_up_hash').val(), password:$('#password').val() });
		console.log(data);
		
		switch(data.resCd) {
		case "0000":
			console.log("success");
			window.location.href = SVR_URL + "/oauth/customer/resetComplete.do";
			break;
		default:
			console.log("error");
			alert(data.resMsg);
			return false;
		}
	}
</script>
</html>