<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
<title>Insert title here</title>
	<script type='text/javascript' src='/oauth/resources/js/jquery-3.4.1.min.js'></script>
	<script type='text/javascript' src='/oauth/resources/js/common.js?date=20221206111700'></script>
	<script type='text/javascript' src='/oauth/resources/js/api.js?date=20221206111700'></script>
	<script type="text/javascript" src='/oauth/resources/js/kcp.js?date=20221206111700'></script>
	<link type="text/css" rel="stylesheet" href="/oauth/resources/css/common.css?date=20221206111700">
</head>
<body>
	<h1>비밀번호 변경 완료</h1>
	<span>비밀번호 재설정이 완료되었습니다.</span>
	<br />
	<button onclick="go_back();">로그인</button>
</body>
<script>
</script>
</html>