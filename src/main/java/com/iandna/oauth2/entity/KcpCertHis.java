package com.iandna.oauth2.entity;

import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.ToString;

@Entity
@Data
@ToString
@Table(name = "SNB_CUST_KCP_CERT_HIS")
public class KcpCertHis extends RegDateEntity {
	@Id
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	@Column(name="SEQ_NO")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long seqNo;
	
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="CUST_NO", length = 50)
    private String uid;     // unique_id (email 주소)
	
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="UP_HASH", length = 64)
    private String upHash;
	
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="DI", length = 128)
    private String di;
	
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="PHONE_NO", length = 11)
    private String phoneNo;
	
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="USER_NAME", length = 30)
    private String userName;
	
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="BIRTH_DAY", length = 12)
    private String birthDay;
	
	public KcpCertHis() {
		onPrePersist();
	}
	
	public KcpCertHis(String upHash) {
		onPrePersist();
		setUpHash(upHash);
	}
}
