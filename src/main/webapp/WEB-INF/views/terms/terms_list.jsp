<%@ page language="java" contentType="text/html;charset=utf-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <title>i&na 약관 동의</title>
    <script src="/oauth/resources/js/jquery-3.4.1.min.js"></script>
    <script type='text/javascript' src='/oauth/resources/js/common.js'></script>
	<script type='text/javascript' src='/oauth/resources/js/api.js'></script>
    
    <!-- CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<link type="text/css" rel="stylesheet" href="/oauth/resources/css/common.css">
    
    <!-- 테마 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

    <!-- 자바스크립트 -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <style>
        body {
            overflow-x: hidden;
            overflow-y: auto;
            display: -webkit-box;
            display: -moz-box;
        }

        span {
            font-family: Helvetica, sans-serif;
        }

        .nav-tabs {
            overflow-x: auto;
            overflow-y: hidden;
            display: -webkit-box;
            display: -moz-box;
            position: relative;
        }
        .nav-tabs>li {
            float: none;
            font-size: 20px;
            position: relative;
            display: -webkit-box;
            display: -moz-box;
        }

        label {
            font-size: 24px;
            position: relative;
            top: -1.5px;
        }

        input {
            zoom: 2;
            position: relative;
            top: 1.5px;
        }

        .radioBtn {
            width: 1px;
            height: 1px;
            visibility: hidden;
        }
    </style>

</head>
<body>
<div id="wrapper" class="col-sm-12">
    <div id="page-wrapper">

        <!-- 약관 동의 페이지 start -->
        <div class="col-sm-12" id="termsAgrPage" align="center">
            <button type="button" onclick="go_back()">뒤로</button>
            <h2>아이보리가 처음이시군요</h2>
            <h2>약관내용에 동의 해주세요</h2>
            <div class="col-sm-12">
                <form id="termsYnForm" method="post" action="/oauth/terms/agreeYn.do">
                    <label for="termsAllAgr">
                        <input type="checkbox" id="termsAllAgr" value="99" onclick="allAgrByTermsPage()"><strong> 모든 운영원칙에 동의</strong>
                    </label><br>
                    <label for="termsAgr1">
                        <input type="checkbox" id="termsAgr1"> 서비스 이용 약관 (필수)
                        <input type="hidden" id="termsValue1" value="1">
                        <button type="button" onclick="openTerms()">></button>
                        <input type="radio" id="agree1" name="agree1" class="radioBtn" value="N" checked>
                    </label><br>
                    <label for="termsAgr2">
                        <input type="checkbox" id="termsAgr2"> 개인정보 처리방침 (필수)
                        <input type="hidden" id="termsValue2" value="2">
                        <button type="button" onclick="openTerms()">></button>
                        <input type="radio" id="agree2" name="agree2" class="radioBtn" value="N" checked>
                    </label><br>
                    <label for="termsAgr6">
                        <input type="checkbox" id="termsAgr6"> 통합회원 가입약관 (필수)
                        <input type="hidden" id="termsValue6" value="6">
                        <button type="button" onclick="openTerms()">></button>
                        <input type="radio" id="agree6" name="agree6" class="radioBtn" value="N" checked>
                    </label><br>
                    <label for="termsAgr3">
                        <input type="checkbox" id="termsAgr3"> 선택 정보 수집 (선택)
                        <input type="hidden" id="termsValue3" value="3">
                        <button type="button" onclick="openTerms()">></button>
                        <input type="radio" id="agree3" name="agree3" class="radioBtn" value="N" checked>
                    </label><br>
                    <label for="termsAgr5">
                        <input type="checkbox" id="termsAgr5"> 정보 활용동의(베베캠) (선택)
                        <input type="hidden" id="termsValue5" value="5">
                        <button type="button" onclick="openTerms()">></button>
                        <input type="radio" id="agree5" name="agree5" class="radioBtn" value="N" checked>
                    </label><br>
                    <label for="termsAgr4">
                        <input type="checkbox" id="termsAgr4"> 개인정보 처리방침 (선택)
                        <input type="hidden" id="termsValue4" value="4">
                        <button type="button" onclick="openTerms()">></button>
                        <input type="radio" id="agree4" name="agree4" class="radioBtn" value="N" checked>
                    </label><br>
                    <button type="button" id="termsYnSubmit">동의하기</button>
                </form>
            </div>
        </div>
        <!-- 약관 동의 페이지 end -->

        <!-- 약관 탭 페이지 start -->
        <div class="tabs-container animated fadeInRight col-sm-12 hs" id="termsTabPage">
            <button type="button" onclick="closeTermsTab()" class="hs"><</button>
            <ul class="nav nav-tabs col-sm-12 tab-ul hs" role="tablist" id="tabUl">
                <li id="termsTab1" class="active hs"><a class="nav-link active hs" data-toggle="tab" href="#tab-1" id="termsA1">서비스 이용 약관 동의</a></li>
                <li id="termsTab2" class="hs"><a class="nav-link hs" data-toggle="tab" href="#tab-2">
                    개인정보 처리방침 동의 </a></li>
                <li id="termsTab3" class="hs"><a class="nav-link hs" data-toggle="tab" href="#tab-3">
                    선택입력 정보 수집 동의 </a></li>
                <li id="termsTab4" class="hs"><a class="nav-link hs" data-toggle="tab" href="#tab-4">
                    개인정보 처리방침 </a></li>
                <li id="termsTab5" class="hs"><a class="nav-link hs" data-toggle="tab" href="#tab-5">
                    정보 활용동의(베베캠) </a></li>
                <li id="termsTab6" class="hs"><a class="nav-link hs" data-toggle="tab" href="#tab-6" id="termsLink6">
                    통합회원 가입약관 </a></li>
            </ul>
            <div class="tab-content hs">
                <div class="tab-pane active hs" id="tab-1" role="tabpanel">
                    <div class="row wrapper border-bottom white-bg page-heading hs">
                        <div class="col-lg-12 col-sm-12 hs">
                            <div class="ibox hs">
                                <div class="ibox-title hs">
                                    <h5 class="hs">서비스 이용 약관 동의</h5>
                                </div>
                                <div class="ibox-content hs">
                                    <span id="content1" class="hs"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane hs" id="tab-2" role="tabpanel">
                    <div class="row wrapper border-bottom white-bg page-heading hs">
                        <div class="col-lg-12 col-sm-12 hs">
                            <div class="ibox hs">
                                <div class="ibox-title hs">
                                    <h5 class="hs">개인정보 처리방침 동의</h5>
                                </div>
                                <div class="ibox-content hs">
                                    <span id="content2" class="hs"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane hs" id="tab-3" role="tabpanel">
                    <div class="row wrapper border-bottom white-bg page-heading hs">
                        <div class="col-lg-12 col-sm-12 hs">
                            <div class="ibox hs">
                                <div class="ibox-title hs">
                                    <h5 class="hs">선택입력 정보 수집 동의</h5>
                                </div>
                                <div class="ibox-content hs">
                                    <span id="content3" class="hs"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane hs" id="tab-4" role="tabpanel">
                    <div class="row wrapper border-bottom white-bg page-heading hs">
                        <div class="col-lg-12 col-sm-12 hs">
                            <div class="ibox hs">
                                <div class="ibox-title hs">
                                    <h5 class="hs">개인정보 처리방침</h5>
                                </div>
                                <div class="ibox-conten ">
                                    <span id="content4" class="hs"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane hs" id="tab-5" role="tabpanel">
                    <div class="row wrapper border-bottom white-bg page-heading hs">
                        <div class="col-lg-12 col-sm-12 hs">
                            <div class="ibox hs">
                                <div class="ibox-title hs">
                                    <h5 class="hs">정보 활용동의(베베캠)</h5>
                                </div>
                                <div class="ibox-content hs">
                                    <span id="content5" class="hs"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane hs" id="tab-6" role="tabpanel">
                    <div class="row wrapper border-bottom white-bg page-heading hs">
                        <div class="col-lg-12 col-sm-12 hs">
                            <div class="ibox hs">
                                <div class="ibox-title hs">
                                    <h5 class="hs">통합회원 가입약관</h5>
                                </div>
                                <div class="ibox-content hs">
                                    <span id="content6" class="hs"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button onclick="allAgrByTermsTab()" class="hs">모든 운영원칙에 동의</button>
        </div>
        <!-- 약관 탭 페이지 end -->
    </div>
</div>
</body>
<script>
    $(document).ready(function() {

        /* checkbox 초기 value setting */
        $('input[id^="termsAgr"]').val('N');

        /* page의 font를 통일하기 위해 content 불러올 때 html tag 사용 */
        $('#content1').html(`${ termsList[0].content}`);
        $('#content2').html(`${ termsList[1].content}`);
        $('#content3').html(`${ termsList[2].content}`);
        $('#content4').html(`${ termsList[3].content}`);
        $('#content5').html(`${ termsList[4].content}`);
        $('#content6').html(`${ termsList[5].content}`);

        document.font = '20px "맑은 고딕 Semilight"';

        /* 약관 보기 버튼 클릭 시 약관을 불러오기 위해 첫 페이지 hide 처리 */
        hiddenContent();

        /* checkbox 클릭 여부에 따라 input value 제어 */
        $('input[id^="termsAgr"]').on('click', function() {
            let checkedBox = document.querySelectorAll('input[id^="termsAgr"]:checked');

            if($(this).is(':checked')) {

                /* 전체 동의일 때 약관 체크박스를 클릭하면 전체 동의 체크박스 해제 */
                $('#termsAllAgr').prop('checked', false);
                $(this).next().next().next().val('Y');
            } else {
                $(this).next().next().next().val('N');
            }

            /* 약관 checkbox가 모두 체크되어 있으면 모두 동의 check true */
            (checkedBox.length == 6) ?
                $('#termsAllAgr').prop('checked', true) : $('#termsAllAgr').prop('checked', false);
        });

        /* 약관 전체 동의 checkbox 제어 */
        $(document).on('click', '#termsAllAgr', function() {
            $(this).is(':checked') ? $('input[name^="agree"]').val('Y') : $('input[name^="agree"]').val('N');
        });

        /* 필수 동의 정보 검증 후 약관 동의 정보 submit */
        $(document).on('click', '#termsYnSubmit', function() {
            if(!$('#termsAgr1').is(':checked') || !$('#termsAgr2').is(':checked') || !$('#termsAgr6').is(':checked')) {
                alert('필수 약관에 모두 동의하셔야 합니다.');
            } else {
                $('#termsYnForm').submit();
            }
        });
    });

    let position = 0;

    /* tab content 숨기기/보이기 function */
    function hiddenContent() {
        $('.hs').css('visibility', 'hidden');
        $('.hs').css('display', 'none');
    }

    function showContent() {
        $('.hs').css('display', '');
        $('.hs').css('visibility', 'visible');
    }

    /* 약관 상세 보기 function */
    function openTerms() {
        let $target = $(event.target);
        let value = $target.prev().val();

        $('#termsAgrPage').hide();
        showContent();

        /* 가로 스크롤의 시작 좌표를 0으로 세팅 */
        $('ul').off().scrollLeft(0);

        /* 약관 구분 코드에 따라 content 노출 */
        if(value === '1') {
            position = $('#termsTab1').position().left;

            $('a[href="#tab-1"]').tab('show');
        } else if(value === '2') {
            position = $('#termsTab2').position().left;

            $('a[href="#tab-2"]').tab('show');
        } else if(value === '3') {
            position = $('#termsTab3').position().left;

            $('a[href="#tab-3"]').tab('show');
        } else if(value === '4') {
            position = $('#termsTab4').position().left;

            $('a[href="#tab-4"]').tab('show');
        } else if(value === '5') {
            position = $('#termsTab5').position().left;

            $('a[href="#tab-5"]').tab('show');
        } else if(value === '6') {
            position = $('#termsTab6').position().left;

            $('a[href="#tab-6"]').tab('show');
        }

        /* 가로 스크롤 시작 좌표를 탭 메뉴의 시작 좌표로 세팅 */
        $('ul').off().scrollLeft(position);
    }

    /* 약관 상세 보기 페이지의 전체 동의 function */
    function allAgrByTermsTab() {
        $('input[id^="terms"]').prop('checked', true);
        $('input[name^="agree"]').val('Y');

        hiddenContent();
        $('#termsAgrPage').show();
    }

    /* 약관 동의 페이지의 전체 동의 function */
    function allAgrByTermsPage() {
        if($('#termsAllAgr').is(':checked')) {
            $('input[id^="terms"]').prop('checked', true);
        } else {
            $('input[id^="terms"]').prop('checked', false);
        }
    }

    /* 약관 상세 보기 페이지 닫기 function */
    function closeTermsTab() {
        hiddenContent();
        $('#termsAgrPage').show();
    }

</script>
</html>