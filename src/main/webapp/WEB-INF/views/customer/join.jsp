<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ include file="./auth_call.jsp" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

    <!-- css -->
    <link rel="stylesheet" href="/oauth/resources/assets/css/font.css?date=20221206111700" />
    <link rel="stylesheet" href="/oauth/resources/assets/css/common.css?date=20221206111700" />
    <link rel="stylesheet" href="/oauth/resources/assets/css/animation.css?date=20221206111700" />
    <link rel="stylesheet" href="/oauth/resources/assets/css/regist.css?date=20221206111700" />
    <link rel="stylesheet" href="/oauth/resources/assets/css/common_ui.css?date=20221206111700" />

    <title>Regist</title>

    <script type='text/javascript' src='/oauth/resources/js/common.js?date=20221206111700'></script>
    <script type='text/javascript' src='/oauth/resources/js/api.js?date=20221206111700'></script>
    <script type="text/javascript" src='/oauth/resources/js/kcp.js?date=20221206111700'></script>
    <link type="text/css" rel="stylesheet" href="/oauth/resources/css/common.css?date=20221206111700">

    <style>
        /*
            220728 ajy
            약관 폰트 정리
        */
        .popup_container .popup_content .text * {
            /* font-size: 1.1em !important; */
            line-height: 1.5em !important;
            text-align: left !important;
            font-family: "MG050", "Noto Sans KR", sans-serif !important;
            /* padding-top: .3em; */
            /* padding-bottom: .3em; */
        }

        .popup_container .popup_content .text .term_title {
            font-size: 1.5em !important;
        }
    </style>
</head>

<body>
<div class="container sub">
    <!-- sub header -->
    <div class="sub_header">
        <a href="#" id="back_btn" onclick="go_back($('#return_url').val());">
            <img src="/oauth/resources/assets/images/left_black.svg" alt="뒤로가기" />
        </a>
    </div>
    <!-- sub header -->


    <!-- sub wrapper -->
    <form name="form_auth" method="post">
        <div class="sub_wrapper regist_box" id="cert_info">
            <h2>회원가입</h2>

            <!-- input group -->
            <div class="input_box">
                <div class="form_group is_guide">
                    <label for="registId">이메일</label>
                    <input class="form_input regist_input require id_input auto_email"
                           placeholder="이메일 주소를 입력해주세요." type="email" name="registId" id="registId" />
                    <button type="button" class="btn_duplicate_email"
                            onclick="id_dup_check();">중복확인</button>
                    <span></span>
                    <button type="button" class="input_icon delete">
                        <img src="/oauth/resources/assets/images/delete_black.svg" alt="입력값 삭제" />
                    </button>
                    <div class="error_message"></div>

                    <!-- 고정 메세지 추가 220706 ajy -->
                    <div class="guide_text">* 재가입시 이전에 탈퇴하신 아이디(이메일주소)는 사용하실 수 없습니다.</div>

                    <ul class="email_list" id="emailList">
                        <li><a href="javascript:void(0);">

                            <span class="input_value"></span><span
                                class="email_address">naver.com</span>
                        </a>
                        </li>
                        <li><a href="javascript:void(0);">

                            <span class="input_value"></span><span
                                class="email_address">daum.net</span>
                        </li>
                        </a>
                        <li><a href="javascript:void(0);">

                            <span class="input_value"></span><span
                                class="email_address">kakao.com</span>
                        </a>
                        </li>
                        <li><a href="javascript:void(0);">

                            <span class="input_value"></span><span
                                class="email_address">nate.com</span>
                        </li>
                        </a>
                        <li><a href="javascript:void(0);">

                            <span class="input_value"></span><span
                                class="email_address">gmail.com</span>
                        </a>
                        </li>
                        <li><a href="javascript:void(0);">

                            <span class="input_value"></span><span
                                class="email_address">hanmail.net</span>
                        </a>
                        </li>
                        <li><a href="javascript:void(0);">

                            <span class="input_value"></span><span
                                class="email_address">hotmail.com</span>
                        </a>
                        </li>
                        <li><a href="javascript:void(0);">

                            <span class="input_value"></span><span
                                class="email_address">korea.kr</span>
                        </li>
                        </a>
                        <li><a href="javascript:void(0);">

                            <span class="input_value"></span><span
                                class="email_address">icloud.com</span>
                        </a>
                        </li>
                    </ul>
                </div>

                <div class="form_group">
                    <label for="registPassword">비밀번호</label>
                    <input class="form_input regist_input require" placeholder="비밀번호를 입력해주세요."
                           type="password" name="registPassword" id="registPassword"
                           style="padding-right: 0;" />
                    <span></span>
                    <button type="button" class="input_icon show_password show">
                        <img class="show" src="/oauth/resources/assets/images/show_password.svg"
                             alt="비밀번호 보이기" />
                        <img class="hide" src="/oauth/resources/assets/images/hide_password.svg"
                             alt="비밀번호 감추기" />
                    </button>
                    <div class="error_message"></div>
                </div>
                <div class="form_group">
                    <label for="registPasswordConfirm">비밀번호 확인</label>
                    <input class="form_input regist_input require" placeholder="위와 동일한 비밀번호를 입력해주세요."
                           type="password" name="registPasswordConfirm" id="registPasswordConfirm"
                           style="padding-right: 0;" />
                    <span></span>
                    <button type="button" class="input_icon show_password show">
                        <img class="show" src="/oauth/resources/assets/images/show_password.svg"
                             alt="비밀번호 보이기" />
                        <img class="hide" src="/oauth/resources/assets/images/hide_password.svg"
                             alt="비밀번호 감추기" />
                    </button>
                    <div class="error_message"></div>
                </div>
            </div>
            <!-- input group -->

            <!-- agree box -->
            <div class="agree_box">
                <details class="all_check">
                    <summary>
                        <div class="form_checkbox">
                            <input type="checkbox" id="agreeBulk" />
                            <label for="agreeBulk" class="fake_box"></label>
                            <label class="form_label" for="agreeBulk">약관 전체동의</label>
                        </div>
                    </summary>
                    <ul class="agree_list" id="agreeList">
                        <li>
                            <a data-popup="agreeService" href="/">서비스 이용약관 (필수)</a>
                            <div class="form_checkbox">
                                <input type="checkbox" id="agreeService" name="agreeService" require />
                                <label for="agreeService" class="fake_box"></label>
                            </div>
                        </li>
                        <li>
                            <a data-popup="agreePrivicy" href="/">개인정보 수집 및 이용동의 (필수)</a>
                            <div class="form_checkbox">
                                <input type="checkbox" id="agreePrivicy" name="agreePrivicy" require />
                                <label for="agreePrivicy" class="fake_box"></label>
                            </div>
                        </li>
                        <li>
                            <a data-popup="agreeTotal" href="/">통합회원가입 약관 (필수)</a>
                            <div class="form_checkbox">
                                <input type="checkbox" id="agreeTotal" name="agreeTotal" require />
                                <label for="agreeTotal" class="fake_box"></label>
                            </div>
                        </li>
                        <li>
                            <a data-popup="agreeInfo" href="/">마케팅 활용 및 광고성 정보 수신 동의(선택)</a>
                            <div class="form_checkbox">
                                <input type="checkbox" id="agreeInfo" name="agreeInfo" />
                                <label for="agreeInfo" class="fake_box"></label>
                            </div>
                        </li>
                        <li>
                            <a data-popup="agreePrivicy2" href="/">개인정보처리방침 (선택)</a>
                            <div class="form_checkbox">
                                <input type="checkbox" id="agreePrivicy2" name="agreePrivicy2" />
                                <label for="agreePrivicy2" class="fake_box"></label>
                            </div>
                        </li>
                    </ul>
                </details>

            </div>
            <!-- agree box -->

            <!-- block btn -->
            <!--
        22.04.19 ajy
        disabled 속성제거, 클래스로 대체
    -->
            <button type="button" class="block_btn disabled" id="btnAuthentication"
                    onclick="go_validation($(this));">
                본인 인증하기
            </button>
            <!-- btn -->
        </div>
        <input type="hidden" name="duplication_state" id="duplication_state" value='n' />
        <input type="hidden" name="call_gbn_cd" id="call_gbn_cd" />
        <!-- 본인인증 관련 parameters -->
        <!-- 요청종류 -->
        <input type="hidden" name="req_tx" value="cert" />
        <!-- 요청구분 -->
        <input type="hidden" name="cert_method" value="01" />
        <!-- auth_conf.jsp -->
        <input type="hidden" name="web_siteid" value="<%= g_conf_web_siteid %>" />
        <input type="hidden" name="site_cd" value="<%= g_conf_site_cd %>" />
        <input type="hidden" name="Ret_URL" value="<%= g_conf_Ret_URL %>" />
        <input type="hidden" name="veri_up_hash" id="veri_up_hash" value="<%= veri_up_hash %>" />
        <!-- 일련번호생성기 -->
        <input type="hidden" name="ordr_idxx" class="frminput" value="<%=ordr_idxx%>" size="40"
               readonly="readonly" maxlength="40" />
        <!-- cert_otp_use 필수 ( 메뉴얼 참고)
         Y : 실명 확인 + OTP 점유 확인 , N : 실명 확인 only
    -->
        <input type="hidden" name="cert_otp_use" value="Y" />
        <!-- 리턴 암호화 고도화 -->
        <input type="hidden" name="cert_enc_use_ext" value="Y" />
        <input type="hidden" name="res_cd" value="" />
        <input type="hidden" name="res_msg" value="" />
        <!-- 본인확인 input 비활성화 -->
        <input type="hidden" name="cert_able_yn" value="" />
        <!-- web_siteid 검증 을 위한 필드 -->
        <input type="hidden" name="web_siteid_hashYN" value="" />

        <!-- 가맹점 사용 필드 (인증완료시 리턴)-->
        <input type="hidden" name="name" id="name" />
        <input type="hidden" name="birthDate" id="birthDate" />
        <input type="hidden" name="phoneNo" id="phoneNo" />
        <input type="hidden" name="gender" id="gender" />
        <input type="hidden" name="ci_url" id="ci_url" />
        <input type="hidden" name="di_url" id="di_url" />
        <input type="hidden" name="certDi" id="certDi" />
        <input type="hidden" name="certCi" id="certCi" />

        <input type="hidden" name="DI" id="DI" />
        <input type="hidden" name="DI_URL" id="DI_URL" />
        <input type="hidden" name="CI" id="CI" />
        <input type="hidden" name="CI_URL" id="CI_URL" />

        <input type="hidden" id="param_opt_1" name="param_opt_1" value="opt1" />
        <input type="hidden" name="param_opt_2" value="opt2" />
        <input type="hidden" name="param_opt_3" value="opt3" />

        <!-- 없으면 PASS 정보입력 창이 안나옴. -->
        <input type="hidden" name="year" id="year" />
        <input type="hidden" name="month" id="month" />
        <input type="hidden" name="day" id="day" />

        <input type="hidden" name="agree1" id="agree1" />
        <input type="hidden" name="agree2" id="agree2" />
        <input type="hidden" name="agree3" id="agree3" />
        <input type="hidden" name="agree4" id="agree4" />
        <input type="hidden" name="agree5" id="agree5" />
        <input type="hidden" name="agree6" id="agree6" />
        <input type="hidden" name="agree7" id="agree7" />
        <input type="hidden" name="agree8" id="agree8" />
        <input type="hidden" name="agree9" id="agree9" value="N" />
        <input type="hidden" name="return_url" id="return_url" value="${ iParam.return_url }" />
    </form>
    <iframe id="kcp_cert" name="kcp_cert" frameborder="0"
            style="display:none; width:100%; height:100vh;"></iframe>
    <!-- sub wrapper -->

    <!-- result modal -->
    <div class="result_modal" id="authDuplication">
        <!--
<button type="button" class="result_close">
    <img src="/oauth/resources/assets/images/delete_black.svg" alt="닫기" />
</button>
 -->
        <h2>본인인증 중복</h2>
        <div class="result_box">
            <div class="result_imgbox">
                <img src="/oauth/resources/assets/images/user_search.svg" alt="유저" />
            </div>
            <p class="text_top">
                회원님의 명의로<br />본인인증된 아이디가 있습니다.
            </p>
            <div class="result_email" id="dup_id"></div>
            <p class="text_bottom">
                본인명의의 여러 휴대전화 번호를 사용하는 경우여도<br />1개의
                계정(아이디)만 실명인증 가능합니다.
            </p>
            <a type="button" class="block_btn" id="btnLogin" href="/oauth/login">
                로그인
            </a>
        </div>
    </div>
    <!-- result modal -->
</div>

<!-- jquery -->
<script type="text/javascript" src="/oauth/resources/assets/js/jQuery.js"></script>

<!-- common ui -->
<script type="text/javascript" src="/oauth/resources/assets/js/common_ui.js"></script>
<!-- common -->
<script type="text/javascript" src="/oauth/resources/assets/js/common.js"></script>

<div id="terms" class="terms" style="display: none;">
    <c:forEach var="item" items="${ data.terms }" varStatus="status" step="1">
        <input id="term_title${ status.index+1 }" value='${ item.subject }'>
        <input id="term_content${ status.index+1 }" value='${ item.content }'>
    </c:forEach>
</div>
</body>
<script>
    window.document.onload = async function (e) {
        resizeIframe($('#kcp_cert'));
    }

    checkBtnAuthentication();
    function go_profile() {
        console.log('go_profile');
        var auth_form = document.form_auth;
        auth_form.action = SVR_URL + "/oauth/customer/profile.do";
        auth_form.target = "_top";
        console.log(auth_form);

        auth_form.submit();
    }

    $('.typing_check').on("propertychange change keyup paste input", function () {
        console.log("typing_check");
        valid_check();
    });

    async function id_dup_check() {
        csId = $('#registId').val();

        if (csId.length == 0) {
            callAlert({ title: '', content: '이메일을 입력해주세요.', btn: '확인' });
            $("#registId").removeClass('error');
        }
        if (csId.indexOf('@') > 13) {
            callAlert({ title: '', content: '이메일 아이디 부분은 12자 이하여야 합니다.', btn: '확인' });
            $("#registId").removeClass('error');
        }
        else if (csId != csId.toLowerCase()) {
            callAlert({ title: '', content: '이메일은 소문자로 구성되어야 합니다.', btn: '확인' });
            $("#registId").removeClass('error');
        }
        else if (validateEmail(csId)) {
            var data = {};
            console.log("id_check");
            data = await callApi('/oauth/customer/idDuplication.json', "post", { csId: csId });
            console.log(data);
            $('#duplication_state').val(data.result);
            $('#param_opt_1').val(csId);
            checkBtnAuthentication();
            if (data.result == 'y') {
                onErrorMessageGreen({
                    message: '사용하실 수 있는 이메일주소입니다.',
                    target: '#registId',
                });
            } else if (data.result == 'n') {
                onErrorMessage({
                    message: '중복되는 이메일 주소입니다.',
                    target: '#registId',
                });
            } else if (data.result == 'd') {
                onErrorMessage({
                    message: '재가입시 새로운 아이디(이메일주소)로 가입해주세요.',
                    target: '#registId',
                });
            }
        } else {
            onErrorMessage({
                message: '이메일 형식을 확인해주세요.',
                target: '#registId',
            });
        }
    }

    $('#password_confirm').on("propertychange change keyup paste input", function () {
        if ($('#password').val() != $('#password_confirm').val()) {
            $('#password_confirm_guide').text('*변경할 비밀번호와 일치하지 않습니다.');
        } else {
            $('#password_confirm_guide').text('');
        }
    });

    function check_dup() {
        console.log('dup');
        if (duplication_state == 'n') {

            return false;
        }
        return true;
    }

    function go_validation(obj) {
        console.log('val');
        if (obj.attr('class').indexOf('disabled') > -1) {
            csId = $("#registId").val();
            pw = $('#registPassword').val();
            pwc = $('#registPasswordConfirm').val();
            agree6 = $('#agree6').val();
            agree7 = $('#agree7').val();
            agree3 = $('#agree3').val();

            if (csId.length == 0) {
                callAlert({ title: '', content: '이메일을 입력해주세요.', btn: '확인' });
                return;
            } else if (pw.length == 0) {
                callAlert({ title: '', content: '비밀번호를 입력해주세요.', btn: '확인' });
                return;
            } else if (pwc.length == 0) {
                callAlert({ title: '', content: '비밀번호 확인을 입력해주세요.', btn: '확인' });
                return;
            } else if ($('#duplication_state').val() != 'y') {
                callAlert({ title: '', content: '이메일 중복확인을 해주세요.', btn: '확인' });
                return;
            } else if (!validateEmail(csId)) {
                callAlert({ title: '', content: '이메일 양식을 확인해주세요.', btn: '확인' });
                return;
            } else if (!validatePassword(pw)) {
                callAlert({ title: '', content: '비밀번호를 다시 입력해주세요.', btn: '확인' });
                return;
            } else if (pw != pwc) {
                callAlert({ title: '', content: '비밀번호 확인을 다시 입력해주세요.', btn: '확인' });
                return;
            } else if (agree6 != 'Y') {
                callAlert({ title: '', content: '필수약관에 모두 동의해주세요.', btn: '확인' });
                return;
            } else if (agree7 != 'Y') {
                callAlert({ title: '', content: '필수약관에 모두 동의해주세요.', btn: '확인' });
                return;
            } else if (agree3 != 'Y') {
                callAlert({ title: '', content: '필수약관에 모두 동의해주세요.', btn: '확인' });
                return;
            }
            return;
        }

        go_kcp(1);
        return false;
    }
</script>

</html>