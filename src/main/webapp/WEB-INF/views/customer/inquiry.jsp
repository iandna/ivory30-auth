<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

        <!-- css -->
        <link type="text/css" rel="stylesheet" href="/oauth/resources/assets/css/qnaFaq.css?date=20231011111300" />
		<link type="text/css" rel="stylesheet" href="/oauth/resources/assets/css/font.css?date=20231011111300" />
	    <link type="text/css" rel="stylesheet" href="/oauth/resources/assets/css/common.css?date=20231011111300" />
	    <link type="text/css" rel="stylesheet" href="/oauth/resources/assets/css/animation.css?date=20231011111300" />
	    <link type="text/css" rel="stylesheet" href="/oauth/resources/assets/css/common_ui.css?date=20231011111300" />
        <title>Qna</title>
        
        <script type='text/javascript' src='/oauth/resources/js/common.js?date=20231011111300'></script>
		<script type='text/javascript' src='/oauth/resources/js/api.js?date=20231011111300'></script>
    </head>
    <body>
        <div class="container sub">
            <!-- sub header -->
            <div class="sub_header">
                <a href="#" id="back_btn" onclick="go_back($('#return_url').val());">
                    <img src="/oauth/resources/assets/images/left_black.svg" alt="뒤로가기" />
                </a>
            </div>
            <!-- sub header -->

            <!-- tab wrapper -->
            <div class="tab_wrapper">
            <form id="form_qna">
                <!-- tab header -->
                <input type="hidden" name="name" value="로그인문의">
                <input type="hidden" name="phone_no" value="로그인문의">
                <input type="hidden" name="return_url" id="return_url" value="${ iParam.return_url }" />
                <div class="one_tab tab_header">
                    <a href="#tab_01" class="tab_btn">
                        <span>자주 묻는 질문</span>
                    </a>
                </div>
                <!-- tab header -->

                <!-- tab content -->
	                <div class="tab_content">
	                    <%--
	                    <div class="tab_item" id="tab_01">
	                        <!-- sub wrapper -->
	                        <div class="sub_wrapper qna_box_wrapper">
	                            <!-- input group -->
	                            <div class="input_box" id="unloggedUser">
	                                <div class="form_group">
	                                    <label for="unLoggedUserId">아이디</label>
	                                    <input
	                                        type="text"
	                                        class="form_input qnafaq_input"
	                                        name="unLoggedUserId"
	                                        id="unLoggedUserId"
	                                        placeholder="회원가입을 하신 경우에 입력해 주세요"
	                                    />
	                                    <span></span>
	                                    <button type="button" class="input_icon delete"><img src="/oauth/resources/assets/images/delete_black.svg" alt="입력값 삭제" /></button>
	                                </div>
	                                <div class="form_group">
	                                    <label for="unLoggedUserEmail">답변받을 이메일</label>
	                                    <input
	                                        type="email"
	                                        class="form_input qnafaq_input"
	                                        name="email"
	                                        id="unLoggedUserEmail"
	                                        placeholder="답변받을 이메일을 입력해 주세요"
	                                    />
	                                    <span></span>
	                                    <button type="button" class="input_icon delete"><img src="/oauth/resources/assets/images/delete_black.svg" alt="입력값 삭제" /></button>
	                                    <ul class="email_list" id="emailList">
	                                        <li><span class="input_value"></span><span class="email_address">naver.com</span></li>
	                                        <li><span class="input_value"></span><span class="email_address">daum.net</span></li>
	                                        <li><span class="input_value"></span><span class="email_address">kakao.com</span></li>
	                                        <li><span class="input_value"></span><span class="email_address">nate.com</span></li>
	                                        <li><span class="input_value"></span><span class="email_address">gmail.com</span></li>
	                                    </ul>
	                                </div>
	                            </div>
	                            <!-- input group -->
	
	                            <!-- radio group -->
	                            <div class="radio_box">
	                                <div class="form_group" id="usedSmartPhoneType">
	                                    <label>사용하고 계신 휴대전화 기기의 종류</label>
	                                    <ul class="radio_list">
	                                        <li>
	                                            <div class="form_radiobox">
	                                                <input type="radio" name="eq_gbn_cd" id="typeIOS" value="I"/>
	                                                <label for="typeIOS" class="fake_box"></label>
	                                                <label for="typeIOS" class="form_label">아이폰 (IOS)</label>
	                                            </div>
	                                        </li>
	                                        <li>
	                                            <div class="form_radiobox">
	                                                <input type="radio" name="eq_gbn_cd" id="typeETC" value="A" />
	                                                <label for="typeETC" class="fake_box"></label>
	                                                <label for="typeETC" class="form_label">안드로이드 (IOS 외 기종)</label>
	                                            </div>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div class="form_group" id="qnaIvory">
	                                    <label>안녕하세요. 아이보리 고객센터입니다. <br />무엇을 도와드릴까요?</label>
	                                    <ul class="radio_list">
	                                        <li>
	                                            <div class="form_radiobox">
	                                                <input type="radio" name="qna_gbn_cd" id="qnaType_01" value="5" />
	                                                <label for="qnaType_01" class="fake_box"></label>
	                                                <label for="qnaType_01" class="form_label">베베캠 문의</label>
	                                            </div>
	                                        </li>
	                                        <li>
	                                            <div class="form_radiobox">
	                                                <input type="radio" name="qna_gbn_cd" id="qnaType_02" value="0" />
	                                                <label for="qnaType_02" class="fake_box"></label>
	                                                <label for="qnaType_02" class="form_label">회원정보 관련 문의</label>
	                                            </div>
	                                        </li>
	                                        <li>
	                                            <div class="form_radiobox">
	                                                <input type="radio" name="qna_gbn_cd" id="qnaType_03" value="1" />
	                                                <label for="qnaType_03" class="fake_box"></label>
	                                                <label for="qnaType_03" class="form_label">체험단 문의</label>
	                                            </div>
	                                        </li>
	                                        <li>
	                                            <div class="form_radiobox">
	                                                <input type="radio" name="qna_gbn_cd" id="qnaType_04" value="4" />
	                                                <label for="qnaType_04" class="fake_box"></label>
	                                                <label for="qnaType_04" class="form_label">스토어 문의</label>
	                                            </div>
	                                        </li>
	                                        <li>
	                                            <div class="form_radiobox">
	                                                <input type="radio" name="qna_gbn_cd" id="qnaType_05" value="2" />
	                                                <label for="qnaType_05" class="fake_box"></label>
	                                                <label for="qnaType_05" class="form_label">광고제휴 문의</label>
	                                            </div>
	                                        </li>
	                                        <li>
	                                            <div class="form_radiobox">
	                                                <input type="radio" name="qna_gbn_cd" id="qnaType_06" value="3" />
	                                                <label for="qnaType_06" class="fake_box"></label>
	                                                <label for="qnaType_06" class="form_label">기타 문의</label>
	                                            </div>
	                                        </li>
	                                    </ul>
	                                </div>
	                            </div>
	                            <!-- radio group -->
	
	                            <!-- textarea -->
	                            <div class="text_area_box">
	                                <div class="form_group">
	                                    <label for="qnaText">문의 내용을 입력해 주세요</label>
	                                    <textarea
	                                        id="qnaText"
	                                        class="form_input textarea_auto_line"
	                                        data-limit-line="4"
	                                        placeholder="내용 입력"
	                                        rows="2"
	                                        name="content"
	                                    ></textarea>
	                                    <span></span>
	                                </div>
	                            </div>
	                            <!-- textarea -->
	
	                            <!-- attach file -->
	                            <div class="attach_file_box">
	                                <button type="button" class="block_btn transparent" id="btnAttachmentFile">파일첨부</button>
	                                <div class="form_group">
	                                    <input name="attachmentFile" type="file" id="attachmentFile" class="form_input" hidden />
	                                    <img class="front_icon" src="/oauth/resources/assets/images/icon_clip.png" alt="아이콘" />
	                                    <input type="text" class="fake_input" readonly />
	                                    <button type="button" class="btn_delete_file" id="btnDeleteFile">
	                                        <img src="/oauth/resources/assets/images/icon_delete.png" alt="아이콘" />
	                                    </button>
	                                </div>
	                            </div>
	                            <!-- attach file -->
	
	                            <!-- send -->
	                            <div class="send_box">
	                                <button type="button" class="block_btn" id="btnSendQna" onclick="qna_regist();">문의 내용 전송</button>
	                            </div>
	                            <!-- send -->
	                        </div>
	                        <!-- sub wrapper -->
	                    </div>
	                    --%>
	                    <div class="tab_item" id="tab_01">
	                        <!-- sub wrapper -->
	                        <div class="sub_wrapper faq_box_wrapper">
	                            <!-- category -->
	                            <ul class="category_list" id="categoryList">
	                            </ul>
	                            <!-- category -->
								<!-- collapse -->
	                            
	                        </div>
	                        <!-- sub wrapper -->
	                    </div>
	                </div>
                
                <!-- tab content -->

                <!-- tab modal -->
                <div class="tab_modal">
                    <div class="img_box">
                        <img src="/oauth/resources/assets/images/password_result.svg" alt="비밀번호 변경 완료" />
                    </div>

                    <p>비밀번호 변경이 완료되었습니다.</p>

                    <a href="login.html" class="block_btn"> 로그인 </a>
                </div>
                <!-- tab modal -->
            </form>
            </div>
            <!-- tab wrapper -->
        </div>

        <!-- jquery -->
        <script type="text/javascript" src="/oauth/resources/assets/js/jQuery.js"></script>
        <!-- alert -->
        <script type="text/javascript" src="/oauth/resources/assets/js/common_ui.js"></script>
        <!-- common -->
        <script type="text/javascript" src="/oauth/resources/assets/js/common.js"></script>
    </body>
    
    <script type="text/javascript">
	    $(async function(){
	    	await get_faq();
	    	$('.faqCategory').on('click', function() {
		    	var index = $(this).val();
		    	$('.collapse_box').hide();
		    	$('#collapse_0'+index).show();
		    });
	    	$('#faqCategory_01').click();
    	});
    
    	function check_form() {
    		if(!validateEmail($('#unLoggedUserEmail').val())) {
    			callAlert({title:'', content:'올바른 이메일을 입력해주세요.', btn:'확인'});
    			return false;
    		}
    		if(typeof($("input:radio[name=eq_gbn_cd]:checked").val()) == 'undefined') {
    			callAlert({title:'', content:'휴대전화 기기 종류를 선택해주세요.', btn:'확인'});
    			return false;
    		}	
    		if(typeof($("input:radio[name=qna_gbn_cd]:checked").val()) == 'undefined') {
    			callAlert({title:'', content:'문의 유형을 선택해주세요.', btn:'확인'});
    			return false;
    		}
    		if($('#qnaText').val().length < 1) {
    			callAlert({title:'', content:'문의 내용을 입력해주세요.', btn:'확인'});
    			return false;
    		}
    		
    		return true;
    	}
    
	    async function qna_regist() {
	    	if(check_form()) {
	    		var form = $('#form_qna')[0];
			    var form_data = new FormData(form);
			    var data = {};
		    	data = await callMultipartApi("https://i-vory.net/customers/qnaReg.json", "post", form_data);
		    	//data = await callMultipartApi("http://localhost:10000/customers/qnaReg.json", "post", form_data);
		    	console.log(data);
		    	if(data.resCd == '0000') {
					callBackAlert({ title: '', content: '문의가 등록되었습니다.', btn: "확인" }, function(){
						location.replace('/oauth/login');
	    		  	});
		    	}else {
		    		callAlert({title:'', content:'문의 등록중 오류가 발생했습니다.', btn:'확인'});
		    	}
	    	}
	    }
	    
	    async function get_faq() {
		    var data = {};
	    	data = await callApi("https://i-vory.net/share/faq/list.json", "get", {});
	    	console.log(data.result.faq_list1);
	    	if(data.resCd == '0000') {
	    		var categoryHtml = '';
	    		var collapseBoxHtml = '';
	    		var gbn_name_list = data.result.gbn_name_list;
	    		for(var i = 1; i <= gbn_name_list.length; i++) {
	    			console.log(gbn_name_list[i-1]);
	    			categoryHtml += '<li><input type="radio" class="faqCategory" name="faqCategory" id="faqCategory_0' + i + '" value="' + i + '"';
	    			if(i == 1)
	    				categoryHtml += ' checked />';
	    			else
	    				categoryHtml += ' />';
	    			categoryHtml += '<label for="faqCategory_0' + i + '">' + gbn_name_list[i-1] + '</label></li>';
	    			collapseBoxHtml += '<div class="collapse_box" id="collapse_0' + i + '"></div>';
	    			
	    			var faq_list;
	    			switch(i) {
	    			case 1:
	    				var faq_list = data.result.faq_list1;
	    				break;
	    			case 2:
	    				var faq_list = data.result.faq_list2;
	    				break;
	    			case 3:
	    				var faq_list = data.result.faq_list3;
	    				break;
	    			case 4:
	    				var faq_list = data.result.faq_list4;
	    				break;
	    			default:
	    			}
	    			
	    			var collapseItemHtml = '';
	    			for(var l = 0; l < faq_list.length; l++) {
	    				collapseItemHtml += '<div class="collapse_item">';
	    				collapseItemHtml += 	'<button type="button" class="header btn_collapse"><div class="title">';
	    				collapseItemHtml += faq_list[l].subject;
	    				collapseItemHtml += 	'</div><div class="img_box"><img src="/oauth/resources/assets/images/collapse_idle.png" alt="열기" /><img src="/oauth/resources/assets/images/collapse_on.png" class="reverse" alt="닫기" /></div></button>'
	    				collapseItemHtml += '<div class="hide_box"><p>';
	    				collapseItemHtml += faq_list[l].content;
	    				collapseItemHtml += '</p></div></div>';
	    			}
	    			$('.faq_box_wrapper').append(collapseBoxHtml);
	    			$('#collapse_0'+i).append(collapseItemHtml);
	    		}
    			$('#categoryList').append(categoryHtml);
	    	}else {
	    		callAlert({title:'', content:'자주 묻는 질문 목록을 가져오는데 실패하였습니다.', btn:'확인'});
	    	}
	    }
	    
    </script>
</html>
