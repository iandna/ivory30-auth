package com.iandna.oauth2.config;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.thymeleaf.extras.springsecurity5.dialect.SpringSecurityDialect;

import com.iandna.oauth2.interceptor.CommonInterceptor;

@Configuration
@EnableWebMvc
public class WebMvcConfig implements WebMvcConfigurer {

    private static final long MAX_AGE_SECONDS = 3600;
    private static final Logger logger = LoggerFactory.getLogger(WebMvcConfigurer.class);
    @Autowired
	private CommonInterceptor commInterceptor;
    private static final List<String> COMMON_URL_PATTERNS = Arrays.asList("/oauth/customer/*");
    private static final String[] CLASSPATH_RESOURCE_LOCATIONS = {
    	      "classpath:/META-INF/resources/",
    	      "classpath:/META-INF/oauth/resources/",
    	      "classpath:/static/",
    	      "classpath:/public/",
    	      "classpath:/META-INF/resources/webjars/",
    	      "classpath:/resources/",
    	      "classpath:/oauth/resources/"
    	      //"/resources/"
    };
    
    @Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(commInterceptor).addPathPatterns(COMMON_URL_PATTERNS);
	}
    
    // web MVC 용 security config
    /*
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
//                .allowedOrigins("*")
                .allowedOrigins("https://i-vory.net", "https://cert.kcp.co.kr")
                .allowedMethods("GET", "POST", "PUT", "DELETE")
                .allowedHeaders("*")
                .allowCredentials(true)
                .maxAge(MAX_AGE_SECONDS);
    }
    */
    
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
    	// 정적 리소스 선언
    	registry.addResourceHandler("/resources/**").addResourceLocations("oauth/resources/");
    	registry.addResourceHandler("/oauth/resources/**").addResourceLocations("oauth/resources/");
    	registry.addResourceHandler("/oauth/resources/assets/**").addResourceLocations("oauth/resources/assets/");
    	registry.addResourceHandler("/resource/**").addResourceLocations("WEB-INF/resources/");
    	if (!registry.hasMappingForPattern("/**")) {
		      registry.addResourceHandler("/**").addResourceLocations(
		          CLASSPATH_RESOURCE_LOCATIONS);
		}
        registry.addResourceHandler("/webjars/**")
        .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/oauth").setViewName("thymeleaf/index");
        registry.addViewController("/oauth/other").setViewName("thymeleaf/other");
        registry.addViewController("/oauth/login").setViewName("thymeleaf/login");
    }

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Bean
    public SpringSecurityDialect springSecurityDialect() {
        return new SpringSecurityDialect();
    }
    
    @Override
	public void configureViewResolvers(ViewResolverRegistry registry) {
		registry.jsp("/WEB-INF/views/", ".jsp");
		
	}
    
    @Bean
    public CommonsMultipartResolver multipartResolver(){
        CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver();
        commonsMultipartResolver.setDefaultEncoding("UTF-8");
        commonsMultipartResolver.setMaxUploadSize(50 * 1024 * 1024);
        return commonsMultipartResolver;
    }
}
