console.log('api.js on load');

async function callApi(url, method, data) {
	var result;
	var status;
	await $.ajax({
	    url: url,
	    data: data,
	    type: method,
	    dataType: "json",
	    contentType: "application/x-www-form-urlencoded",
		cache: false,
	    crossDomain: true,
		success: function (data) {
			console.log('success');
			result = data;
			status = 1;
	    },
	    error: function (xhr, textStatus, errorThrown) {
	    	console.log('error');
	        status = 9;
	    },
	    complete : function() {
	    	
	    }
	});
	if(status = 1){
		return result;
	}else if (status = 9){
		alert('ERROR');
	}
}

async function callMultipartApi(url, method, data) {
	var result;
	var status;
	await $.ajax({
	    type: method,
	    enctype: 'multipart/form-data',
	    url: url,
	    data: data,
	    processData: false,
        contentType: false,
		success: function (data) {
			console.log('success');
			result = data;
			status = 1;
	    },
	    error: function (xhr, textStatus, errorThrown) {
	    	console.log('error');
	        status = 9;
	    },
	    complete : function() {
	    	
	    }
	});
	if(status = 1){
		return result;
	}else if (status = 9){
		alert('ERROR');
	}
}