package com.iandna.oauth2.entity;

import lombok.Getter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Getter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class ModDateEntity {
    @Column(name="MOD_DT", columnDefinition = "char(8)")
    private String modDate;     //수정일자

    @Column(name="MOD_TM", columnDefinition = "char(6)")
    private String modTime;     //수정시간

//    @PrePersist
//    public void onPrePersist(){
//        this.modDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
//        this.modTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));
//    }

    @PreUpdate
    public void onPreUpdate(){
        this.modDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        this.modTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));
    }
}