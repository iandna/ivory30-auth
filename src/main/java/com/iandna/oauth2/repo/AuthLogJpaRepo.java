package com.iandna.oauth2.repo;

import com.iandna.oauth2.entity.AuthLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthLogJpaRepo extends JpaRepository<AuthLog, Long> {
}
