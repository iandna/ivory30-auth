console.log('kcp.js on load');
var kcp_counter = 0;
async function auth_data( frm )
{
	// 정상완료 후 return 받은 경우 kcp 호출횟수 변수를 초기화
	kcp_counter = 0;
	console.log('in auth_data');
    var auth_form     = document.form_auth;
    var nField        = frm.elements.length;
    var response_data = "";

    // up_hash 검증 
    if( frm.up_hash.value != auth_form.veri_up_hash.value )
    {
    	console.log("up_hash 변조 위험있음");
    }              
    
    // 응답 데이터 처리
    var temp_obj = {};
    
	for ( i = 0; i < nField; i++ )
    {
        if( frm.elements[i].value != "" )
        {
            response_data += frm.elements[i].name + " : " + frm.elements[i].value + "\n";
            
            // custom
            var temp_name = frm.elements[i].name;
        	var temp_value = frm.elements[i].value;
        	temp_obj[temp_name] = temp_value;
        }
    }
    
    if( navigator.userAgent.indexOf("Android") > - 1 || navigator.userAgent.indexOf("iPhone") > - 1 )
    {
        document.getElementById( "cert_info" ).style.display = "";
        document.getElementById( "kcp_cert"  ).style.display = "none";
    }
    var data = {};
    // 콜 페이지 구분에 따라 추가 작업
	switch(auth_form.call_gbn_cd.value) {
	case "1": // 회원가입
		$('#customer_info_box').show();
		$('#phoneNo').val(temp_obj.phone_no);
		$('#name').val(temp_obj.user_name);
		$('#birthDate').val(temp_obj.birth_day);
		$('#gender').val(temp_obj.sex_code);
		$('#di_url').val(temp_obj.di_url);
		$('#ci_url').val(temp_obj.ci_url);
		$('#certDi').val(temp_obj.di_url);
		$('#certCi').val(temp_obj.ci_url);

		if(
			temp_obj.phone_no == '01030337933'
		 || temp_obj.phone_no == '01099234921'
		 || temp_obj.phone_no == '01089748198'
		 || temp_obj.phone_no == '01092884247'
		){
			console.log("test pass");
			callAlert({title:'', content:'본인인증이 완료되었습니다.', btn:'확인'});
			go_profile();
		} else if(temp_obj.dup == "Y") {
			document.getElementById("dup_id").innerHTML += temp_obj.csId;
			onCheckDuplication();
		}
		/*
		else if(isChild(temp_obj.birth_day)) {
			callAlert({title:'', content:'만 14세 미만의 경우 회원가입이 제한됩니다.', btn:'확인'});
		}
		*/
		else {
			callAlert({title:'', content:'본인인증이 완료되었습니다.', btn:'확인'});
			go_profile();
		}
		//data = await callApi('/oauth/customer/findId.json', "post", { phoneNo:temp_obj.di_url });
		break;
	case "2": // 아이디 찾기
		data = await callApi('/oauth/customer/findId.json', "post", { phoneNo:temp_obj.phone_no });
		console.log(data);
		if(data.csId == '') {
			callAlert({title:'', content:'찾으시는 아이디가 없습니다.', btn:'확인'});
		}else {
			// callAlert({title:'', content:'본인인증이 완료되었습니다.', btn:'확인'});
			findIdChangeUI();
			$('#customer_id').html(data.csId);
			$('#reg_date').text(format_date(data.regDate) + ' 가입');
		}
		break;
	case "3":
		// callAlert({title:'', content:'본인인증이 완료되었습니다.', btn:'확인'});
		findPassWordChangeUI();
		$('#findPasswordTitle').html('비밀번호 변경');
		$('#passwordFindBox').prop('hidden', true);
		$('#password_reset_box').prop('hidden', false);
		$('#inputFindPassword').focus();
	default:
	}
	
    //console.log(temp_obj);	
    //console.log(frm);
}

// 인증창 호출 함수
function auth_type_check()
{
    var auth_form = document.form_auth;
    console.log(auth_form.call_gbn_cd);
    if( auth_form.ordr_idxx.value == "" )
    {
        console.log( "요청번호는 필수 입니다." );

        return false;
    }
    else
    {
    	/* smartphone source */
    	
    	if( navigator.userAgent.indexOf("Android") > - 1 || navigator.userAgent.indexOf("iPhone") > - 1 )
        {
    		console.log('>-1');
            auth_form.target = "kcp_cert";
            
            document.getElementById( "cert_info" ).style.display = "none";
            document.getElementById( "kcp_cert"  ).style.display = "";
        }
    	
        else
        {
        	console.log('else');
            var return_gubun;
            var width  = 410;
            var height = 500;

            var leftpos = screen.width  / 2 - ( width  / 2 );
            var toppos  = screen.height / 2 - ( height / 2 );

            var winopts  = "width=" + width   + ", height=" + height + ", toolbar=no,status=no,statusbar=no,menubar=no,scrollbars=no,resizable=no";
            var position = ",left=" + leftpos + ", top="    + toppos;
            var AUTH_POP = window.open('','auth_popup', winopts + position);
            
            auth_form.target = "auth_popup";
        }
    	
        auth_form.action = SVR_URL + "/oauth/customer/authReq.do"; // 인증창 호출 및 결과값 리턴 페이지 주소
        console.log(auth_form);
        return true;
        
    }
}

async function go_kcp(call_gbn_cd) {
	if(kcp_counter == 0) {
		kcp_counter++;
		document.getElementById('call_gbn_cd').value=call_gbn_cd;
		await auth_type_check();
		document.form_auth.submit();
	}
	else {
		document.getElementById( "cert_info" ).style.display = "none";
        document.getElementById( "kcp_cert"  ).style.display = "";
	}
}

// iframe auto resize
function resizeIframe(obj) { 
	obj.style.height = (obj.contentWindow.document.form_auth.scrollHeight)+ 'px'; 
}

