package com.iandna.oauth2.controller.terms;

import com.iandna.oauth2.entity.Terms;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/oauth/terms")
public class TermsController {
    private static final Logger logger = LoggerFactory.getLogger(TermsController.class);

    @Autowired
    TermsService termsService;

    @RequestMapping(value = "/list.do")
    public String termsList(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
        logger.debug("####### list.do #######");

        List<Terms> result;
        result = this.termsService.findTerms();

        model.addAttribute("termsList", result);
        return "terms/terms_list";
    }

    @RequestMapping(value = "/agreeYn.do")
    @ResponseStatus(HttpStatus.TEMPORARY_REDIRECT)
    public String termsAgrYn(@RequestParam Map<String, Object> reqParam
            , ModelMap model
            , HttpServletRequest request
            , RedirectAttributesModelMap reModel) {
        reModel.addAttribute("agree1", reqParam.get("agree1"));
        reModel.addAttribute("agree2", reqParam.get("agree2"));
        reModel.addAttribute("agree3", reqParam.get("agree3"));
        reModel.addAttribute("agree4", reqParam.get("agree4"));
        reModel.addAttribute("agree5", reqParam.get("agree5"));
        reModel.addAttribute("agree6", reqParam.get("agree6"));

        return "redirect:/oauth/customer/join.do";
    }
}
