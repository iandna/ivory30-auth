package com.iandna.oauth2.controller.customer;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.iandna.oauth2.cookie.CookieSecurityContextRepository;
import com.iandna.oauth2.cookie.SignedUserInfoCookie;
import com.iandna.oauth2.entity.AgreeInfo;
import com.iandna.oauth2.entity.Customer;
import com.iandna.oauth2.entity.KcpCertHis;
import com.iandna.oauth2.entity.ResponseModel;
import com.iandna.oauth2.entity.SnsAcsInfo;
import com.iandna.oauth2.entity.Terms;
import com.iandna.oauth2.repo.CustomerJpaRepo;
import com.iandna.oauth2.repo.KcpCertHisJpaRepo;
import com.iandna.oauth2.repo.TermsJpaRepo;
import com.iandna.oauth2.repo.UserJpaRepo;
import com.iandna.oauth2.util.ImgUploadUtil;

import kr.co.kcp.CT_CLI;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class CustomerService {
	private static final Logger logger = LoggerFactory.getLogger(CustomerService.class);
    private final UserJpaRepo userJpaRepo;
    private final PasswordEncoder passwordEncoder;

    private final String kcp_site_cd="AGOE4";
    private final String kcp_enckey="5481832a54e776810c3d09c29e1c1ab0e91370260aa50818e9e3a0a0e7406c52";
    private final String kcp_returl="http://localhost:5004/oauth/user/joinReq.do";
    private final String kcp_gwurl="https://cert.kcp.co.kr/kcp_cert/cert_view.jsp";
    
    @Autowired
    private CustomerJpaRepo customer_jpa_repo;
    
    @Autowired
    private TermsJpaRepo terms_jpa_repo;
    
    @Autowired
    private KcpCertHisJpaRepo kcp_cert_his_jpa_repo;
    
    @Autowired
    private CookieSecurityContextRepository cookieSecurityContextRepository;
    
    @Autowired
    ImgUploadUtil imgUploadUtil;
    
    public Map<String, String> decryptKcpRes(HttpServletRequest request, Map<String, Object> param) throws Exception {
    	Map<String, String> invalidParam = new HashMap();
    	Map<String, String> result = new HashMap();
		CT_CLI cc = new CT_CLI();
		
		String site_cd = "";
		String ordr_idxx = "";

		String cert_no = "";
		String enc_cert_data2 = "";
		String enc_info = "";
		String enc_data = "";
		String req_tx = "";

		String tran_cd = "";
		String res_cd = "";
		String res_msg = "";

		String dn_hash = "";
		Enumeration params = request.getParameterNames();
		while (params.hasMoreElements()) {
			String nmParam = (String) params.nextElement();
			String valParam[] = request.getParameterValues(nmParam);

			for (int i = 0; i < valParam.length; i++) {
				if (nmParam.equals("site_cd")) {
					site_cd = f_get_parm_str(valParam[i]);
				}

				if (nmParam.equals("ordr_idxx")) {
					ordr_idxx = f_get_parm_str(valParam[i]);
				}

				if (nmParam.equals("res_cd")) {
					res_cd = f_get_parm_str(valParam[i]);
				}

				if (nmParam.equals("req_tx")) {
					req_tx = f_get_parm_str(valParam[i]);
				}

				if (nmParam.equals("cert_no")) {
					cert_no = f_get_parm_str(valParam[i]);
				}

				if (nmParam.equals("enc_cert_data2")) {
					enc_cert_data2 = f_get_parm_str(valParam[i]);
				}

				if (nmParam.equals("dn_hash")) {
					dn_hash = f_get_parm_str(valParam[i]);
				}
				// 결과 메시지가 한글 데이터 URL decoding 해줘야합니다.
				else {
					param.put(nmParam, valParam[i]);
				}
			}
		}
		if(!cc.makeHashData(kcp_enckey, site_cd + ordr_idxx + cert_no)
				.equals(dn_hash)) {
			invalidParam.put("res_cd", "9999");
			invalidParam.put("res_msg", "변조된 데이터 입니다.");
			return invalidParam;
		}
		
		cc.decryptEncCert( kcp_enckey, kcp_site_cd, param.get("cert_no").toString(), param.get("enc_cert_data2").toString() );
		result.put("comm_id", cc.getKeyValue("comm_id") == null ? "" : cc.getKeyValue("comm_id"));
		result.put("phone_no", cc.getKeyValue("phone_no") == null ? "" : cc.getKeyValue("phone_no"));
		result.put("user_name", cc.getKeyValue("user_name") == null ? "" : cc.getKeyValue("user_name"));
		result.put("birth_day", cc.getKeyValue("birth_day") == null ? "" : cc.getKeyValue("birth_day"));
		result.put("sex_code", cc.getKeyValue("sex_code") == null ? "" : cc.getKeyValue("sex_code"));
		result.put("local_code", cc.getKeyValue("local_code") == null ? "" : cc.getKeyValue("local_code"));
		result.put("ci", cc.getKeyValue("ci") == null ? "" : cc.getKeyValue("ci"));
		result.put("di", cc.getKeyValue("di") == null ? "" : cc.getKeyValue("di"));
		result.put("ci_url", cc.getKeyValue("ci_url") == null ? "" : cc.getKeyValue("ci_url"));
		result.put("di_url", cc.getKeyValue("di_url") == null ? "" : cc.getKeyValue("di_url"));
		result.put("web_siteid", cc.getKeyValue("web_siteid") == null ? "" : cc.getKeyValue("web_siteid"));
		result.put("res_cd", cc.getKeyValue("res_cd") == null ? "" : cc.getKeyValue("res_cd"));
		result.put("res_msg", cc.getKeyValue("res_msg") == null ? "" : cc.getKeyValue("res_msg"));
		result.put("up_hash", cc.makeHashData(kcp_enckey, kcp_site_cd + param.get("ordr_idxx").toString()));
		
		result.put("site_cd", site_cd);
		result.put("ordr_idxx", ordr_idxx);
		result.put("res_cd", res_cd);
		result.put("req_tx", req_tx);
		result.put("cert_no", cert_no);
		result.put("enc_cert_data2", enc_cert_data2);
		result.put("dn_hash", dn_hash);
		
		
		String di = URLDecoder.decode(result.get("di_url").toString());
		Customer dCustomer = customer_jpa_repo.findFirstByCertDiAndStCdOrderByRegDateDescRegTimeDesc(di, "1").orElse(null);
		if(dCustomer != null) {
			result.put("dup", "Y");
			result.put("csId", dCustomer.getCsId());
		}
		else {
			result.put("dup", "N");
		}
		
		try {
			KcpCertHis kcpCertHis = new KcpCertHis();
			kcpCertHis.setDi(di);
			kcpCertHis.setPhoneNo(result.get("phone_no").toString());
			kcpCertHis.setUserName(result.get("user_name").toString());
			kcpCertHis.setBirthDay(result.get("birth_day").toString());
			kcp_cert_his_jpa_repo.save(kcpCertHis);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		logger.info(result.toString());
		return result;
	}
	
	public String f_get_parm_str( String val )
    {
        if ( val == null ) val = "";
        return  val;
    }
	
	public Customer insertUserInfo(Customer customer, AgreeInfo agree, HttpServletRequest request, HttpServletResponse response) throws Exception {

		logger.info("customerType :::::::::: " + customer.getCustomerType());
		
		String id = customer.getCsId();
		
		String upHash = request.getParameter("veri_up_hash");
		KcpCertHis kcpCertHis = kcp_cert_his_jpa_repo.findTopByUidOrderBySeqNoDesc("C" + id).orElse(new KcpCertHis("asvc24asz23l5687kl45635f4avzxc"));
		if( !upHash.equals
				(kcpCertHis.getUpHash()) ) {
			logger.info(upHash);
			logger.info(kcpCertHis.getUpHash());
			throw new Exception("잘못된 요청입니다.");
		}
		customer.setCertDi(URLDecoder.decode(customer.getCertDi()));
		customer.setCertCi(URLDecoder.decode(customer.getCertCi()));


		boolean isTest = false;
		if(
				customer.getCertDi().equals("MC0GCCqGSIb3DQIJAyEAWrZUJqEw8ZGhizZEXA3kik/8VfvDRB5L3NZejNeTWWc=")
			||	customer.getCertDi().equals("MC0GCCqGSIb3DQIJAyEAirwL82sb4+g6zfB3B2cBydTmsFWopTYn0vNtuqn8OCM=")
			||  customer.getCertDi().equals("MC0GCCqGSIb3DQIJAyEADTDGHoBR9E3wj7QpdeYUeMGkoponiY+o1TIq/enxuOg=")
			||  customer.getCertDi().equals("MC0GCCqGSIb3DQIJAyEAIu+kmDQlweuK1FqGE8OAZ8dAMvIoc1Jqikc6et0JBdo=")
		){
			isTest = true;
		}

		Customer diCustomer = customer_jpa_repo.findFirstByCertDiAndStCdOrderByRegDateDescRegTimeDesc(customer.getCertDi(), "1").orElse(null);
		if(diCustomer != null && !isTest) {
			logger.info("중복된 명의입니다.");
			logger.info(diCustomer.toString());
			throw new Exception("중복된 명의입니다.");
		}
		
		customer.setEmail(id);
		customer.setUid("C" + id);
		customer.setPassword(passwordEncoder.encode(customer.getPassword()));
		if(customer.getCustomerType() == null || customer.getCustomerType().isEmpty())
			customer.setCustomerType("2");
		customer.setStCd("1");
		customer.setShopId(
				StringUtils.substring(id, 0, StringUtils.indexOf(id, "@")+4)
				+ "@s");
		switch(customer.getGender()) {
		case "01":
			customer.setGender("0");
			break;
		case "02":
			customer.setGender("1");
			break;
		}
		if(agree.getAgree8().equals("Y")){
			customer.setPushYn("N"); // 기기 자체 푸시 알람여부
			customer.setPushArm1("1"); // 앱 푸시 동의 여부
		}else {
			customer.setPushYn("N"); // 기기 자체 푸시 알람여부
			customer.setPushArm1("0"); // 앱 푸시 동의 여부
		}

		agree.setUid(customer.getUid());
		SnsAcsInfo snsAcsInfo = new SnsAcsInfo();
		snsAcsInfo.setUid(customer.getUid());
		snsAcsInfo.setConnInstagram('0');
		snsAcsInfo.setConnFacebook('0');
		snsAcsInfo.setConnNaver('0');
		snsAcsInfo.setConnYoutube('0');
		customer.setAgreeInfo(agree);
		customer.setSnsAcsInfo(snsAcsInfo);
		String custImg = null;
		String characterNo = request.getParameter("characterNo");
		if(characterNo == null || characterNo.isEmpty()) {
			custImg = "";
		}else if(characterNo.equals("0")) {
			try {
				MultipartFile file = null;
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
				file = multipartRequest.getFile("profileImageUploader");
				custImg = imgUploadUtil.uploadImageFile(file, "ivory40/profile_image/", 
						file.getOriginalFilename(), "user_img_");
			}catch(Exception e) {
				logger.error("########## imageERROR #########");
				custImg = "";
			}
		}else {
			custImg = "https://iandna.s3.ap-northeast-1.amazonaws.com/ivory40/profile_image/character_" + characterNo + ".png";
		}
		customer.setCustImg(custImg);
		Customer result = customer_jpa_repo.save(customer);
		SignedUserInfoCookie sCookie = new SignedUserInfoCookie(customer, cookieSecurityContextRepository.getCookieHmacKey());
		logger.debug("####");
		logger.debug(sCookie.getName());
		logger.debug(sCookie.getValue());
		logger.debug("####");
		response.addCookie(sCookie);
		
		try {
			Cookie nickname = new Cookie("nickname",
					URLEncoder.encode(customer.getNickname()).replace("+", "%20"));
			response.addCookie(nickname);	
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return result;
	}
	
	@Transactional(readOnly=true)
	public Customer findUserInfoByPhoneNo(Customer customer) {
		logger.info(customer.toString());
		Customer result = customer_jpa_repo.findFirstByCertDiAndStCdOrderByRegDateDescRegTimeDesc(customer.getCertDi(), "1")
				.orElse(
					customer_jpa_repo.findFirstByPhoneNoAndStCdOrderByRegDateDescRegTimeDesc(customer.getPhoneNo(), "1").orElse(null) 
				);
		
		if(result == null)
			result = new Customer("");
		return result;
	}
	
	@Transactional(readOnly=true)
	public Customer findUserInfoById(Customer customer) {
		logger.info(customer.toString());
		Customer result = customer_jpa_repo.findByCsId(customer.getCsId()).orElse(new Customer());
		
		logger.info(result.toString());
		logger.info(result.getEmail());
		return result;
	}
	
	public ResponseModel resetPassword(Map<String, Object> map) throws Exception {
		logger.info("#### map : " + map.toString());
		ResponseModel responseModel = new ResponseModel();
		Map<String, Object> result = new HashMap<String, Object>();
		String id = map.get("csId").toString();
		String upHash = map.get("upHash").toString();
		KcpCertHis kcpCertHis = kcp_cert_his_jpa_repo.findTopByUidOrderBySeqNoDesc("C" + id).orElse(new KcpCertHis("asvc24asz23l5687kl45635f4avzxc"));
		if( !upHash.equals
				(kcpCertHis.getUpHash()) ) {
			logger.info(upHash);
			logger.info(kcpCertHis.getUpHash());
			responseModel.setResCd("998");
			responseModel.setResMsg("잘못 된 요청입니다.");
			return responseModel;
		}
		Customer customer = customer_jpa_repo.findByUid("C" + id).get();
		customer.setPassword(passwordEncoder.encode(map.get("password").toString()));
		customer = customer_jpa_repo.save(customer);
		result.put("customer", customer);
		responseModel.setResult(result);
		return responseModel;
	}
	
	public void insertUpHash(Map<String, Object> map) {
		KcpCertHis kcpCertHis = new KcpCertHis();
		kcpCertHis.setUid("C" + map.get("param_opt_1").toString());
		kcpCertHis.setUpHash(map.get("up_hash").toString());
		
		kcp_cert_his_jpa_repo.save(kcpCertHis);
	}
	
	@Transactional(readOnly=true)
	public Map<String, String> idDuplicationCheck(Customer customer) {
		Map<String, String> result = new HashMap<String, String>();
		Customer data = customer_jpa_repo.findByCsId(customer.getCsId()).orElse(null);
		
		String shopId = StringUtils.substring(customer.getCsId(), 0, StringUtils.indexOf(customer.getCsId(), "@")+4)+ "@s";
		
		if(data == null) 
		{
			// 카페24 아이디 겹치는것 방지
			int customerCount = customer_jpa_repo.countByShopId(shopId);
			if(customerCount > 0) 
				result.put("result", "n");
			else
				result.put("result", "y");
		}
			
		else {
			if(data.getStCd().equals("1"))
				result.put("result", "n");
			else
				result.put("result", "d");
		}
		return result;
	}
	
	@Transactional(readOnly=true)
	public Map<String, String> nicknameDuplicationCheck(Customer customer) {
		Map<String, String> result = new HashMap<String, String>();
//		Customer data = customer_jpa_repo.findByNickname(customer.getNickname()).orElse(null);
		List<Customer> customers = customer_jpa_repo.findAllByNickname(customer.getNickname());
		if(customers == null || customers.size() == 0) 
			result.put("result", "y");
		else 
			result.put("result", "n");
		return result;
	}
	
//	@Transactional(readOnly=true)
	public Map<String, Object> getTerms() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Terms> terms = terms_jpa_repo.selectTerms();
//		List<Terms> terms = terms_jpa_repo.findByStCdOrderBySeqNo("1");
		result.put("terms", terms);
		return result;
	}
	
	@Transactional(readOnly=true)
	public Map<String, Object> test() {
		Map<String, Object> result = new HashMap<String, Object>();
//		Customer c1 = customer_jpa_repo.findFirstByCertDi("MC0GCCqGSIb3DQIJAyEAIu+kmDQlweuK1FqGE8OAZ8dAMvIoc1Jqikc6et0JBdo=").orElse(null);
		Customer c2 = customer_jpa_repo.findFirstByCertDiAndStCdOrderByRegDateDescRegTimeDesc("MC0GCCqGSIb3DQIJAyEAIu+kmDQlweuK1FqGE8OAZ8dAMvIoc1Jqikc6et0JBdo=" ,"1")
				.orElse(
					customer_jpa_repo.findFirstByPhoneNoAndStCdOrderByRegDateDescRegTimeDesc("01092884247", "1").orElse(null) 
				);
		result.put("c2", c2);
		
		return result;
	}
}
