package com.iandna.oauth2.config;

import java.util.Arrays;

import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.firewall.DefaultHttpFirewall;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.CorsUtils;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.iandna.oauth2.cookie.CookieSecurityContextRepository;
import com.iandna.oauth2.cookie.CustomLogoutSuccessHandler;
import com.iandna.oauth2.cookie.LoginWithTargetUrlAuthenticationEntryPoint;
import com.iandna.oauth2.cookie.RedirectToOriginalUrlAuthenticationSuccessHandler;
import com.iandna.oauth2.cookie.SignedUserInfoCookie;
import com.iandna.oauth2.cookie.TokenCookie;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    public static final String LOGIN_FORM_URL = "/oauth/login";
    public static final String LOGOUT_URL = "/oauth/logout";
    public static final String TARGET_AFTER_SUCCESSFUL_LOGIN_PARAM = "target";
    public static final String PROVIDER_PARAM = "provider";

    private final CustomAuthenticationProvider authenticationProvider;
    private final CookieSecurityContextRepository cookieSecurityContextRepository;
    private final LoginWithTargetUrlAuthenticationEntryPoint loginWithTargetUrlAuthenticationEntryPoint;
    private final RedirectToOriginalUrlAuthenticationSuccessHandler redirectToOriginalUrlAuthenticationSuccessHandler;
    private final CustomLogoutSuccessHandler logoutSuccessHandler;
    private final LoginFailureHandler loginFailureHandler;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider);
    }
    
    @Override
    public void configure(WebSecurity web) throws Exception {
    	web.ignoring().mvcMatchers("/oauth/resources/**/*");
    	web.ignoring().mvcMatchers("/resources/**/*");
    	web.ignoring().mvcMatchers("/assets/**/*");
    	web.ignoring().mvcMatchers("/**/*.html");
    	web.ignoring().requestMatchers(PathRequest.toStaticResources().atCommonLocations());
    	web.httpFirewall(strictHttpFireWall());
//    	web.ignoring().mvcMatchers("/oauth/resources/*/**");
//    	web.ignoring().mvcMatchers("/oauth/resources/assets/**/*");
//    	web.ignoring().mvcMatchers("/oauth/resources/assets/css/**/*");
    }

    @Override
    protected void configure(HttpSecurity security) throws Exception {
        security
//                .oauth2Login().authorizationEndpoint().authorizationRequestRepository()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().csrf().disable()
                .securityContext().securityContextRepository(cookieSecurityContextRepository)
                .and().logout().permitAll().deleteCookies(SignedUserInfoCookie.NAME, TokenCookie.NAME)
                .logoutUrl(LOGOUT_URL)
                .logoutSuccessHandler(logoutSuccessHandler)
                .and().headers().frameOptions().disable()
                .and().requestCache().disable()
                .exceptionHandling().authenticationEntryPoint(loginWithTargetUrlAuthenticationEntryPoint)
                .and().formLogin()
                .loginPage(LOGIN_FORM_URL)
                .failureHandler(loginFailureHandler)
                .successHandler(redirectToOriginalUrlAuthenticationSuccessHandler)
                .and().authorizeRequests()
                .requestMatchers(CorsUtils::isPreFlightRequest).permitAll()
                .antMatchers(LOGIN_FORM_URL, "/oauth/customer/*").permitAll()
                .antMatchers(LOGIN_FORM_URL, "/oauth/health/*").permitAll()
//                .antMatchers("/resources/**").permitAll()
//                .antMatchers("/oauth/resources/**").permitAll()
//                .antMatchers("/oauth/resources/assets/**").permitAll()
//                .antMatchers("/oauth/resources/assets/css/**").permitAll()
                .antMatchers("/oauth/terms/**").permitAll()
                .antMatchers("/**").authenticated()
                .anyRequest().authenticated()
                .and().cors()
                .and()
                .httpBasic();
    }
    
    // security용 cors config
	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration configuration = new CorsConfiguration();
		configuration.setAllowedOrigins(Arrays.asList("*"));
//		configuration.setAllowedOrigins(Arrays.asList("https://i-vory.net", "https://cert.kcp.co.kr"));
		configuration.setAllowedMethods(Arrays.asList("HEAD", "GET", "POST", "PUT"));
		configuration.setAllowedHeaders(Arrays.asList("Authorization", "Cache-Control", "Content-Type"));
		configuration.setAllowCredentials(true);
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}
	/*
	@Bean
	SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
	    http
	        // ...
	        .redirectToHttps(withDefaults());
	    return http.build();
	}
	 */
	/*
	@Bean
	public HttpFirewall defaultHttpFirewall() {
	    return new DefaultHttpFirewall();
	}
	*/
	@Bean
	public StrictHttpFirewall strictHttpFireWall() {
		StrictHttpFirewall strictHttpFireWall = new StrictHttpFirewall();
		strictHttpFireWall.setAllowSemicolon(true);
		return strictHttpFireWall;
	}

}