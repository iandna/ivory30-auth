package com.iandna.oauth2.cookie;

import java.io.IOException;
import java.util.Collections;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.adobe.xmp.impl.Base64;
import com.iandna.oauth2.config.SecurityConfig;
import com.iandna.oauth2.entity.AuthLog;
import com.iandna.oauth2.entity.Customer;
import com.iandna.oauth2.repo.AuthLogJpaRepo;

@Component
public class RedirectToOriginalUrlAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
    private static final Logger LOG = LoggerFactory.getLogger(RedirectToOriginalUrlAuthenticationSuccessHandler.class);
    private static final String DEFAULT_TARGET_URL = "/";

    @Autowired
    private AuthLogJpaRepo authLogJpaRepo;

    public RedirectToOriginalUrlAuthenticationSuccessHandler() {
        super(DEFAULT_TARGET_URL);
        this.setTargetUrlParameter(SecurityConfig.TARGET_AFTER_SUCCESSFUL_LOGIN_PARAM);
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        Customer userInfo = (Customer) authentication.getPrincipal();
        logger.info("########userInfo : " + userInfo.toString());
        authLogJpaRepo.save(AuthLog.builder()
                .uid(userInfo.getUid())
                .action("1")    //로그인
                .build()
        );

        super.onAuthenticationSuccess(request, response, authentication);
    }

    @Override
    protected String determineTargetUrl(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        String targetUrl = super.determineTargetUrl(request, response, authentication);
        
        String state = request.getParameter("state");
        String referrer = request.getParameter("referrer");
        MultiValueMap<String, String> bypassParams = new LinkedMultiValueMap<>();
        request.getParameterMap().forEach((key,values) -> {
            if (!"username".equals(key) && !"password".equals(key)) {
                for (String value : values) {
                    bypassParams.put(key, Collections.singletonList(value));
                }
            }
        });
        
    	if(request.getServerName().contains("localhost"))
    		targetUrl = "/oauth/authorize?response_type=code&client_id=testClientId&client_secret=$2a$10$1qw9m8GTeZPC2hBAVAJfE.RTh1qAMtBx/LwCqAgDyTA0gkThQPcoK&redirect_url=http://localhost:5004/oauth/callback&state=" + state;
        
        if(request.getParameter("userAgent").toString().contains("_second") ||
        		request.getParameter("client_id").equals("cafe24iandna77")) {
        	// 쇼핑몰 자동로그인(window.open이라 referrer가 없음)
    		targetUrl += "?response_type=" + request.getParameter("response_type") + "&client_id=" + request.getParameter("client_id") + "&redirect_uri=" + request.getParameter("redirect_uri") + "&state=" + state;
        }else {
        	// 앱 관련 자동로그인
        	if(referrer.contains("i-vory.net")) {
        		if(state == null || state.isEmpty() || state.equals("null")) {
        			referrer = referrer.replace("https://i-vory.net", "");
        			if(referrer.contains("oauth") || referrer.contains("gate")) {
        				state = Base64.encode("url=index.do");	
        			}else {
        				state = Base64.encode("url="+referrer);
        			}
        		}
        	}
        	if(request.getServerName().contains("iandna-ivory.com")) {
        		// test server
        		targetUrl = "/oauth/authorize?response_type=code&client_id=testClientId&client_secret=$2a$10$1qw9m8GTeZPC2hBAVAJfE.RTh1qAMtBx/LwCqAgDyTA0gkThQPcoK&redirect_url=https://iandna-ivory.com/oauth/callback&state=" + state;
        	}else {
        		targetUrl = "/oauth/authorize?response_type=code&client_id=iandna77&client_secret=$2a$10$8zrGK5jvv.fYuWrr3qN/8.wYTAVA4u2ocvI1QnMP5nM8yS0uweazu&redirect_url=https://i-vory.net/oauth/callback&state=" + state;	
        	}
        }
        logger.info(targetUrl);
        return targetUrl;
        
    }
}