package com.iandna.oauth2.config;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.iandna.oauth2.util.AES256Cipher;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
public class LoginFailureHandler implements AuthenticationFailureHandler {
	private static final Logger logger = LoggerFactory.getLogger(LoginFailureHandler.class);
	
	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		logger.debug(exception.getMessage());
		logger.debug(request.getParameter("username"));
		logger.debug(request.getParameter("password"));
		logger.debug(request.getParameter("autoClose"));
		Cookie cookie1 = new Cookie("UserToken", "logout");
		Cookie cookie2 = new Cookie("UserInfo", null);
		cookie1.setPath("/");
		cookie2.setPath("/");
		switch(exception.getMessage()) {
		case "997":
			response.sendRedirect("/oauth/login?error=usernotexist");
			break;
		case "998":
			logger.debug("#### is origin user");
			logger.info("serverName : " + request.getServerName());
			String id = AES256Cipher.AES_Encode(request.getParameter("username"));
			String pw = AES256Cipher.AES_Encode(request.getParameter("password"));
			String url = null;
			String referrer = request.getParameter("referrer");
			String userAgent = request.getParameter("userAgent");
			String state = request.getParameter("state");
			
			if(!referrer.contains("application/30/leave.html") && !userAgent.contains("_second") && (userAgent.contains("ivory_3.0") || userAgent.contains("ivory_4.0"))) {
				if(state != null && !state.isEmpty() ) {
		        	Pattern pattern = Pattern.compile("^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?$");
		            Matcher matcher = pattern.matcher(state);
		            if (matcher.find()) {
		            	state = new String(Base64.decodeBase64(state));	
		            }
		        }else {
		        	state = "url=index.do";
		        }
			}
			else if(state != null && !state.isEmpty() ) {
				Pattern pattern = Pattern.compile("^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?$");
	            Matcher matcher = pattern.matcher(state);
	            if (matcher.find()) {
	            	state = new String(Base64.decodeBase64(state));	
	            }
			}
			else if(referrer == null || referrer.isEmpty() || referrer.contains("logout")) {
		        state = "url=index.do";
			}
			/*
			else if(referrer.contains("i-vory.shop") && !referrer.contains("logout")) {
				state = "url=" + referrer;
			}
			else if(referrer.contains("i-vory.net/community")) {
				state = "url=" + referrer;
			}
			*/
			else {
	        	state = "url=" + referrer;
			}
			
			id = URLEncoder.encode(id);
			pw = URLEncoder.encode(pw);
			url = "https://m.i-vory.shop/application/ivory40/oldLogin.html?token1=" + id + "&token2=" + pw;
			url += "&" + state;
			if(request.getParameter("autoClose") != null && request.getParameter("autoClose").equals("Y")) 
				url += "&auto_close=Y";
			if(request.getServerName().contains("iandna-ivory.com")) {
				url += "&test_mode=Y";
			}
			
			response.sendRedirect(url);
			break;
		case "999":
			response.sendRedirect("/oauth/login?error=passwordincorrect");
			break;
		default :
			response.sendRedirect("/oauth/login?error="+exception.getMessage());
		}
		/*
		switch(exception.getCode()) {
		case "998":
			String id = AES256Cipher.AES_Encode(exception.getUid());
			String pw = AES256Cipher.AES_Encode(exception.getPassword());
			response.sendRedirect("https://skin-mobile2--shop2.motiontap.cafe24.com/auto_oldlogin.html?token1=" + id + "&token2=" + pw);
			break;
		case "999":
			break;
		}
		*/
	}
}
