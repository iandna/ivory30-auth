<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%> <%@ include file="./auth_call.jsp"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

        <!-- css -->
        <link rel="stylesheet" href="/oauth/resources/assets/css/font.css?date=20221206111700" />
        <link rel="stylesheet" href="/oauth/resources/assets/css/common.css?date=20221206111700" />
        <link rel="stylesheet" href="/oauth/resources/assets/css/animation.css?date=20221206111700" />
        <link rel="stylesheet" href="/oauth/resources/assets/css/find.css?date=20221206111700" />
        <link rel="stylesheet" href="/oauth/resources/assets/css/common_ui.css?date=20221206111700" />

        <title>Find</title>

        <script type="text/javascript" src="/oauth/resources/js/common.js?date=20221206111700"></script>
        <script type="text/javascript" src="/oauth/resources/js/api.js?date=20221206111700"></script>
        <script type="text/javascript" src="/oauth/resources/js/kcp.js?date=20221206111700"></script>
        <link type="text/css" rel="stylesheet" href="/oauth/resources/css/common.css?date=20221206111700" />
    </head>
    <body>
        <div class="container sub">
            <!-- sub header -->
            <div class="sub_header">
                <a href="#" id="back_btn" onclick="go_back($('#return_url').val());">
                    <img src="/oauth/resources/assets/images/left_black.svg" alt="뒤로가기" />
                </a>
            </div>
            <!-- sub header -->

            <!-- tab wrapper -->
            <div align="center" id="cert_info">
                <form name="form_auth" method="post">
                    <div class="tab_wrapper">
                        <!-- tab header -->
                        <div class="tab_header">
                            <a href="#tab_01" class="tab_btn">
                                <span>아이디 찾기</span>
                            </a>
                            <a href="#tab_02" class="tab_btn">
                                <span>비밀번호 찾기</span>
                            </a>
                        </div>
                        <!-- tab header -->

                        <!-- tab content -->
                        <div class="tab_content">
                            <div class="tab_item" id="tab_01">
                                <!-- sub wrapper -->
                                <div class="sub_wrapper find_box_wrapper">
                                    <h2>아이디 찾기</h2>

                                    <!-- find box -->
                                    <div class="find_box">
                                        <div class="img_box">
                                            <img src="/oauth/resources/assets/images/user.svg" alt="아이디 찾기" id="imgFindId" />
                                            <img src="/oauth/resources/assets/images/user_search.svg" alt="아이디 찾기 결과" id="imgFindIdResult" hidden />
                                        </div>

                                        <div class="message_box" id="findIdMessage" hidden>회원님의 휴대전화로 가입된 아이디가 있습니다.</div>

                                        <div class="user_info" id="findIdUserInfo" hidden>
                                            <p class="user_email" id="customer_id"></p>
                                            <p class="regist_date">
                                                <span>본인인증</span>
                                                <span id="reg_date"></span>
                                            </p>
                                        </div>

                                        <button type="button" class="block_btn" id="btnFindId" onclick="go_kcp(2);">본인 인증하기</button>
                                        <a href="#" onclick="go_back($('#return_url').val());" class="block_btn" id="btnFindIdLogin" hidden> 로그인 </a>
                                    </div>
                                    <!-- find box -->

                                    <!-- btn group -->
                                    <div class="find_login_box">
                                        <a href="#" onclick="go_back($('#return_url').val());" class="btn_login"> 로그인 </a>
                                        <a href="/oauth/customer/join.do" class="btn_regist"> 회원가입 </a>
                                        <button type="button" id="btnFind" hidden>비밀번호 찾기</button>
                                    </div>
                                    <!-- btn group -->
                                </div>
                                <!-- sub wrapper -->
                            </div>
                            <div class="tab_item" id="tab_02">
                                <!-- sub wrapper -->
                                <div class="sub_wrapper find_box_wrapper" style="text-align: left">
                                    <h2 id="findPasswordTitle">비밀번호 찾기</h2>
                                    <!-- find box -->
                                    <div class="find_box" id="passwordFindBox">
                                        <!-- 
		                                <div class="img_box">
		                                    <img
		                                        src="/oauth/resources/assets/images/password_search.svg"
		                                        alt="비밀번호 찾기"
		                                    />
		                                </div>
		 								-->
                                        <div class="input_box" style="text-align: left">
                                            <div class="form_group">
                                                <label for="registId">이메일 또는 아이디</label>
                                                <input
                                                    type="email"
                                                    class="form_input regist_input require id_input"
                                                    placeholder="이메일 또는 아이디를 입력해주세요."
                                                    name="csId"
                                                    id="registId"
                                                />
                                                <span></span>
                                                <button type="button" class="input_icon delete">
                                                    <img src="/oauth/resources/assets/images/delete_black.svg" alt="입력값 삭제" />
                                                </button>
                                            </div>
                                        </div>
                                        <button type="button" class="block_btn" id="btnFindPassword" onclick="check_customer_gbn(3);">본인 인증하기</button>
                                    </div>
                                    <!-- find box -->

                                    <!-- change password -->
                                    <div class="change_password_box" id="password_reset_box" hidden>
                                        <div class="input_box">
                                            <div class="form_group">
                                                <label for="inputFindPassword">비밀번호</label>
                                                <input
                                                    class="form_input regist_input require"
                                                    placeholder="비밀번호를 입력해주세요."
                                                    type="password"
                                                    name="password"
                                                    id="inputFindPassword"
                                                />
                                                <span></span>
                                                <button type="button" class="input_icon show_password show">
                                                    <img class="show" src="/oauth/resources/assets/images/show_password.svg" alt="비밀번호 보이기" />
                                                    <img class="hide" src="/oauth/resources/assets/images/hide_password.svg" alt="비밀번호 감추기" />
                                                </button>
                                                <div class="error_message"></div>
                                            </div>
                                            <div class="form_group">
                                                <label for="inputFindPasswordConfirm">비밀번호 확인</label>
                                                <input
                                                    class="form_input regist_input require"
                                                    placeholder="위와 동일한 비밀번호를 입력해주세요."
                                                    type="password"
                                                    name="inputFindPasswordConfirm"
                                                    id="inputFindPasswordConfirm"
                                                />
                                                <span></span>
                                                <button type="button" class="input_icon show_password show">
                                                    <img class="show" src="/oauth/resources/assets/images/show_password.svg" alt="비밀번호 보이기" />
                                                    <img class="hide" src="/oauth/resources/assets/images/hide_password.svg" alt="비밀번호 감추기" />
                                                </button>
                                                <div class="error_message"></div>
                                            </div>
                                        </div>

                                        <!--
                                            22.04.19 ajy
                                            disabled 속성 클래스 대체
                                        -->
                                        <button type="button" class="block_btn" id="btnChangePassword">비밀번호 변경</button>
                                    </div>
                                    <!-- change password -->

                                    <!-- btn group -->
                                    <div class="find_login_box">
                                        <a href="#" onclick="go_back($('#return_url').val());" class="btn_login"> 로그인 </a>
                                        <a href="/oauth/customer/join.do" class="btn_regist"> 회원가입 </a>
                                    </div>
                                    <!-- btn group -->
                                </div>
                                <!-- sub wrapper -->
                            </div>
                        </div>
                        <!-- tab content -->

                        <!-- tab modal -->
                        <div class="tab_modal">
                            <div class="img_box">
                                <img src="/oauth/resources/assets/images/password_result.svg" alt="비밀번호 변경 완료" />
                            </div>

                            <p>비밀번호 변경이 완료되었습니다.</p>

                            <a href="#" onclick="go_back($('#return_url').val());" class="block_btn"> 로그인 </a>
                        </div>
                        <!-- tab modal -->
                    </div>

                    <input type="hidden" name="call_gbn_cd" id="call_gbn_cd" />
                    <!-- 본인인증 관련 parameters -->
                    <!-- 요청종류 -->
                    <input type="hidden" name="req_tx" value="cert" />
                    <!-- 요청구분 -->
                    <input type="hidden" name="cert_method" value="01" />
                    <!-- auth_conf.jsp -->
                    <input type="hidden" name="web_siteid" value="<%= g_conf_web_siteid %>" />
                    <input type="hidden" name="site_cd" value="<%= g_conf_site_cd %>" />
                    <input type="hidden" name="Ret_URL" value="<%= g_conf_Ret_URL %>" />
                    <input type="hidden" name="veri_up_hash" id="veri_up_hash" value="<%= veri_up_hash %>" />
                    <!-- 일련번호생성기 -->
                    <input type="hidden" name="ordr_idxx" class="frminput" value="<%=ordr_idxx%>" size="40" readonly="readonly" maxlength="40" />
                    <!-- cert_otp_use 필수 ( 메뉴얼 참고)
			             Y : 실명 확인 + OTP 점유 확인 , N : 실명 확인 only
			        -->
                    <input type="hidden" name="cert_otp_use" value="Y" />
                    <!-- 리턴 암호화 고도화 -->
                    <input type="hidden" name="cert_enc_use_ext" value="Y" />
                    <input type="hidden" name="res_cd" value="" />
                    <input type="hidden" name="res_msg" value="" />
                    <!-- 본인확인 input 비활성화 -->
                    <input type="hidden" name="cert_able_yn" value="" />
                    <!-- web_siteid 검증 을 위한 필드 -->
                    <input type="hidden" name="web_siteid_hashYN" value="" />

                    <!-- 가맹점 사용 필드 (인증완료시 리턴)-->
                    <input type="hidden" name="name" id="name" />
                    <input type="hidden" name="birthDate" id="birthDate" />
                    <input type="hidden" name="phoneNo" id="phoneNo" />
                    <input type="hidden" name="gender" id="gender" />
                    <input type="hidden" name="ci_url" id="ci_url" />
                    <input type="hidden" name="di_url" id="di_url" />
                    <input type="hidden" name="certDi" id="certDi" />
                    <input type="hidden" name="certCi" id="certCi" />

                    <input type="hidden" name="DI" id="DI" />
                    <input type="hidden" name="DI_URL" id="DI_URL" />
                    <input type="hidden" name="CI" id="CI" />
                    <input type="hidden" name="CI_URL" id="CI_URL" />

                    <input type="hidden" name="agree1" id="agree1" value="Y" />
                    <input type="hidden" name="agree2" id="agree2" value="Y" />
                    <input type="hidden" name="agree3" id="agree3" value="Y" />

                    <input type="hidden" id="param_opt_1" name="param_opt_1" value="opt1" />
                    <input type="hidden" name="param_opt_2" value="opt2" />
                    <input type="hidden" name="param_opt_3" value="opt3" />

                    <!-- 없으면 PASS 정보입력 창이 안나옴. -->
                    <input type="hidden" name="year" id="year" />
                    <input type="hidden" name="month" id="month" />
                    <input type="hidden" name="day" id="day" />
                    
                    <input type="hidden" name="return_url" id="return_url" value="${ iParam.return_url }" />
                </form>
            </div>
            <!-- tab wrapper -->
            <iframe id="kcp_cert" name="kcp_cert" frameborder="0" 
            style="display: none; width: 100%; height: 100vh"></iframe>
        </div>

        <!-- jquery -->
        <script type="text/javascript" src="/oauth/resources/assets/js/jQuery.js"></script>

        <!-- common ui -->
        <script type="text/javascript" src="/oauth/resources/assets/js/common_ui.js"></script>
        <!-- common -->
        <script type="text/javascript" src="/oauth/resources/assets/js/common.js"></script>

        <script type="text/javascript">
            window.document.onload = function (e) {
                resizeIframe($("#kcp_cert"));
            };
            async function check_customer_gbn(call_gbn_cd) {
                var data = {};
                csId = $("#registId").val();
                if(csId.length == 0) {
                	callAlert({ title: "", content: "이메일 또는 아이디를 입력해주세요.", btn: "확인" });
                	return;
                }
                
                data = await callApi("/oauth/customer/getOne.json", "post", { csId: csId });
                console.log(data);
                if (data.stCd == "9") {
                    callAlert({ title: "", content: "존재하지 않는 회원입니다.", btn: "확인" });
                    return;
                }
                switch (data.customerType) {
                    case "1":
                        var param = '{"id":"' + data.csId + '", "name":"' + data.name + '", "phoneNo":"' + data.phoneNo + '"}';
                        console.log(param);
                        var url = "https://m.i-vory.shop/application/ivory40/find_passwd_auto.html?";
                        url += "param=" + btoa(escape(param));
                        url += "&state=autotest";
                        console.log("기존 회원입니다.");
                        console.log(url);
                        window.location.href = url;
                        break;
                    case "2":
                        console.log("통합 회원입니다.");
                        $("#param_opt_1").val($("#registId").val());
                        go_kcp(call_gbn_cd);
                        break;
                    default:
                        callAlert({ title: "", content: "존재하지 않는 회원입니다.", btn: "확인" });
                }
            }

            $("#password_confirm").on("propertychange change keyup paste input", function () {
                if ($("#password").val() != $("#password_confirm").val()) {
                    $("#password_confirm_guide").text("*변경할 비밀번호와 일치하지 않습니다.");
                } else {
                    $("#password_confirm_guide").text("");
                }
            });

            async function password_reset() {
                if ($("#password").val() != $("#password_confirm").val()) {
                    return false;
                }
                var data = {};
                data = await callApi("/oauth/customer/resetPassword.json", "post", {
                    csId: $("#csId").val(),
                    upHash: $("#veri_up_hash").val(),
                    password: $("#password").val(),
                });
                console.log(data);

                switch (data.resCd) {
                    case "0000":
                        console.log("success");
                        window.location.href = SVR_URL + "/oauth/customer/resetComplete.do";
                        break;
                    default:
                        console.log("error");
                        alert(data.resMsg);
                        return false;
                }
            }
            
            function go_login() {
            	if(document.referrer.indexOf('i-vory.shop/Api/Member/Oauth2ClientCallback/sso') > 0) {
            		history.go(-1);
            	}
            	else {
            		window.location.href='/oauth/login?return_url='+$('#return_url').val();
            	}
            }
        </script>
    </body>
</html>
