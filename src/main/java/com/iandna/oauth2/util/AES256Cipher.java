package com.iandna.oauth2.util;

import java.security.Security;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class AES256Cipher {
	
    private static volatile AES256Cipher INSTANCE;

    final static String secretKey = "sldfj903lfaskfh03kljhfsdf3g3g212"; //32bit
    static String IV = "3fsdf3asw155a22p"; //16bit

    public static AES256Cipher getInstance() {
        if (INSTANCE == null) {
            synchronized (AES256Cipher.class) {
                if (INSTANCE == null)
                    INSTANCE = new AES256Cipher();
            }
        }
        return INSTANCE;
    }

    private AES256Cipher() {
       // IV = secretKey.substring(0, 16);
    }

    //암호화
    public static String AES_Encode(String str) {
        String retEnText  = "";
        try {
        	Security.addProvider(new BouncyCastleProvider());
            byte[] keyData = secretKey.getBytes();
            SecretKey secureKey = new SecretKeySpec(keyData, "AES");
            Cipher c = Cipher.getInstance("AES/CBC/PKCS7Padding", "BC");
            
            c.init(Cipher.ENCRYPT_MODE, secureKey, new IvParameterSpec(IV.getBytes()));

            byte[] encrypted = c.doFinal(str.getBytes("UTF-8"));
//            String enStr = new String(Base64.encode(encrypted, Base64.DEFAULT));
            String enStr = java.util.Base64.getEncoder().encodeToString(encrypted);
            retEnText =  enStr;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retEnText;
    }

    //복호화
    public static String AES_Decode(String str){
        String retText = "";
        try {
        	Security.addProvider(new BouncyCastleProvider());
            byte[] keyData = secretKey.getBytes();
            SecretKey secureKey = new SecretKeySpec(keyData, "AES");
            Cipher c = Cipher.getInstance("AES/CBC/PKCS7Padding", "BC");
            c.init(Cipher.DECRYPT_MODE, secureKey, new IvParameterSpec(IV.getBytes("UTF-8")));

//            byte[] byteStr = Base64.decode(str.getBytes(), Base64.DEFAULT);
            byte[] byteStr = Base64.getDecoder().decode(str.getBytes());
            retText =  new String(c.doFinal(byteStr), "UTF-8");
        }catch (Exception e) {
            e.printStackTrace();
        }
        return retText;
    }
    
    public static void main(String[] args) {
		System.out.println(AES_Encode("easd25"));
		System.out.println(AES_Encode("qqqqqq11"));
		System.out.println(AES_Decode("wFLUx6EmpkJ7j8KBM+3TFw=="));
	}
}
