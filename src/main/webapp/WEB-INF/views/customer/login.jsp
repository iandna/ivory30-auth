<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
    <%@ include file="./auth_call.jsp" %>
        <!DOCTYPE html>
        <html lang="en" xmlns="http://www.w3.org/1999/xhtml"
            xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity5">

        <head>
            <meta charset="UTF-8" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

            <!-- css -->
            <link type="text/css" rel="stylesheet" href="/oauth/resources/assets/css/font.css" />
            <link type="text/css" rel="stylesheet" href="/oauth/resources/assets/css/common.css" />
            <link type="text/css" rel="stylesheet" href="/oauth/resources/assets/css/animation.css" />
            <link type="text/css" rel="stylesheet" href="/oauth/resources/assets/css/login.css" />
            <link type="text/css" rel="stylesheet" href="/oauth/resources/assets/css/common_ui.css" />
            <title>Login</title>
            <style>
                .loading_wrap {
                    position: fixed;
                    top: 0px;
                    left: 0px;
                    background-color: rgba(255, 255, 255, 1.0);
                    width: 100%;
                    height: 100%;
                    z-index: 990;
                }

                .loading-circle {
                    position: absolute;
                    margin: auto;
                    top: 0;
                    bottom: 0;
                    left: 0;
                    right: 0;
                    width: 54px;
                    height: 54px;
                    animation: rotate_image 0.6s linear infinite;
                    transform-origin: 50% 50%;
                }

                .loading_point {
                    position: absolute;
                    margin: auto;
                    top: 0;
                    bottom: 0;
                    left: 0;
                    right: 0;
                    width: 54px;
                    height: 54px;
                }

                @keyframes rotate_image {
                    100% {
                        transform: rotate(360deg);
                    }
                }
            </style>

            <script type="text/javascript" src="/oauth/resources/js/api.js"></script>
        </head>

        <body>
            <div class="container login" sec:authorize="!isAuthenticated()" style="display: none">
                <form action="/oauth/login" method="post" id="login_form">
                    <div class="login_wrapper">
                        <div class="wrapper_top">
                            <!-- login input group -->
                            <div class="login_box">
                                <h1>엄마도 아빠도<br />이번 생은<br />처음이라</h1>
                                <div class="input_box">
                                    <div class="form_group">
                                        <input class="form_input login_input id_input auto_email"
                                            placeholder="이메일 또는 아이디" type="text" name="username" id="username" />
                                        <span></span>
                                        <button type="button" class="input_icon delete">
                                            <img src="/oauth/resources/assets/images/delete_white.svg" alt="입력값 삭제" />
                                        </button>
                                        <ul class="email_list" id="emailList">
                                            <li>
                                                <a href="javascript:void(0);">
                                                    <span class="input_value"></span><span
                                                        class="email_address">naver.com</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);">
                                                    <span class="input_value"></span><span
                                                        class="email_address">daum.net</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);">
                                                    <span class="input_value"></span><span
                                                        class="email_address">kakao.com</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);">
                                                    <span class="input_value"></span><span
                                                        class="email_address">nate.com</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);">
                                                    <span class="input_value"></span><span
                                                        class="email_address">gmail.com</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);">
                                                    <span class="input_value"></span><span
                                                        class="email_address">hanmail.net</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);">
                                                    <span class="input_value"></span><span
                                                        class="email_address">hotmail.com</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);">
                                                    <span class="input_value"></span><span
                                                        class="email_address">korea.kr</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);">
                                                    <span class="input_value"></span><span
                                                        class="email_address">icloud.com</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="form_group">
                                        <input class="form_input login_input" placeholder="비밀번호 입력" type="password"
                                            name="password" id="password" />
                                        <span></span>
                                        <button type="button" class="input_icon show_password show">
                                            <img class="show" src="/oauth/resources/assets/images/show_password.svg"
                                                alt="비밀번호 보이기" />
                                            <img class="hide" src="/oauth/resources/assets/images/hide_password.svg"
                                                alt="비밀번호 감추기" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- login input group -->

                            <!-- login btn group -->
                            <div class="btn_box_login">
                                <button type="button" class="btn_login is_loading"
                                    onclick="info_valid();"><span>로그인</span><img
                                        src="/oauth/resources/assets/images/login_progress.png"
                                        alt="Loading" /></button>
                                <button type="button" onclick="join();" class="btn_join">회원가입</button>
                            </div>
                            <!-- login btn group -->

                            <!-- find group -->
                            <div class="login_find_box">
                                <div class="find_btn_group">
                                    <button type="button" class="find_btn" id="btnMovefindId" onclick="find_id();">아이디
                                        찾기</button>
                                    <button type="button" class="find_btn" id="btnMovefindPassword"
                                        onclick="find_password();">비밀번호 찾기</button>
                                </div>
                                <a href="http://pf.kakao.com/_bDQWT/chat" class="login_guide_box">
                                    <img class="icon_left kakaotalk" src="/oauth/resources/assets/images/kakaotalk.svg"
                                        alt="" />
                                    <span>카카오톡 상담하러 가기</span>
                                    <img class="icon_right" src="/oauth/resources/assets/images/right_white.svg"
                                        alt="" />
                                </a>
                                <a href="/oauth/customer/inquiry.do" class="login_guide_box">
                                    <img class="icon_left more_help" src="/oauth/resources/assets/images/more_help.svg"
                                        alt="" />
                                    <span>도움이 필요하세요?</span>
                                    <img class="icon_right" src="/oauth/resources/assets/images/right_white.svg"
                                        alt="" />
                                </a>
                            </div>
                            <!-- find group -->
                        </div>

                        <!-- logo -->
                        <div class="logo">
                            <img src="/oauth/resources/assets/images/logo.svg" alt="Ivory" />
                        </div>
                        <!-- logo -->
                        <input type="hidden" id="target" name="target" value="" />
                        <input type="hidden" id="state" name="state" />
                        <input type="hidden" id="userAgent" name="userAgent" />
                        <input type="hidden" id="error" name="error" value="" />
                        <input type="hidden" id="autoClose" name="autoClose" value="" />
                        <input type="hidden" id="referrer" name="referrer" />
                        <input type="hidden" id="pathname" name="pathname" />

                        <input type="hidden" id="client_id" name="client_id" value="" />
                        <input type="hidden" id="response_type" name="response_type" value="" />
                        <input type="hidden" id="redirect_uri" name="redirect_uri" value="" />
                        <input type="hidden" id="login_check" name="login_check" value="N">
                    </div>
                </form>
            </div>jsp sec:authorize="isAuthenticated()"

            <!-- jquery -->
            <script type="text/javascript" src="/oauth/resources/assets/js/jQuery.js"></script>
            <!-- alert -->
            <script type="text/javascript" src="/oauth/resources/assets/js/common_ui.js"></script>
            <!-- common -->
            <script type="text/javascript" src="/oauth/resources/assets/js/common.js"></script>

            <div sec:authorize="isAuthenticated()" class="loading_wrap container">
                <input type="hidden" id="target" name="target" value="" />
                <input type="hidden" id="userAgent" name="userAgent" />
                <img class="loading-circle"
                    src="https://iandna.s3-ap-northeast-1.amazonaws.com/ivory3.0/common/loading-circle.png"
                    alt="Loading..." />
                <img class="loading_point"
                    src="https://iandna.s3-ap-northeast-1.amazonaws.com/ivory3.0/common/loading_point.png"
                    alt="Loading..." />
                <input type="hidden" id="error" name="error" value="" />
                <input type="hidden" id="state" name="state" />
                <input type="hidden" id="referrer" name="referrer" />
                <input type="hidden" id="pathname" name="pathname" />

                <input type="hidden" id="client_id" name="client_id" value="" />
                <input type="hidden" id="response_type" name="response_type" value="" />
                <input type="hidden" id="redirect_uri" name="redirect_uri" value="" />
                <input type="hidden" id="login_check" name="login_check" value="Y">
            </div>

            <!-- 기존회원용 로딩창 -->
            <div class="loading_wrap container" id="origin_member_authentication" style="display: none">
                <img class="loading-circle"
                    src="https://iandna.s3-ap-northeast-1.amazonaws.com/ivory3.0/common/loading-circle.png"
                    alt="Loading..." />
                <img class="loading_point"
                    src="https://iandna.s3-ap-northeast-1.amazonaws.com/ivory3.0/common/loading_point.png"
                    alt="Loading..." />
            </div>

            <form method="get" id="location_form"></form>
        </body>
        <script type="text/javascript" src="/oauth/resources/js/common.js"></script>
        <script type="text/javascript">
            window.onload = async function () {
                var userAgent = document.getElementById("userAgent");
                var frcsCustNo = get_cookie('frcsCustNo');
                var referrer = document.referrer;
                if (typeof (frcsCustNo) != 'undefined' && frcsCustNo.length > 0) {
                    localStorage.setItem('frcsCustNo', frcsCustNo);
                } else {
                    frcsCustNo = localStorage.getItem('frcsCustNo');
                }
                $('#state').val(new URLSearchParams(location.search).get('state'));
                $('#pathname').val(new URLSearchParams(location.search).get('pathname'));
                $('#target').val(new URLSearchParams(location.search).get('target'));
                $('#error').val(new URLSearchParams(location.search).get('error'));
                $('#client_id').val(new URLSearchParams(location.search).get('client_id'));
                $('#response_type').val(new URLSearchParams(location.search).get('response_type'));
                $('#redirect_uri').val(new URLSearchParams(location.search).get('redirect_uri'));
                $('#client_id').val(new URLSearchParams(location.search).get('client_id'));

                if (referrer != null && typeof (referrer) != 'undefined' && referrer != '') {
                    $('#referrer').val(referrer + $('#pathname').val());
                }

                // 중복 로그인 체크 후 redirect 시 state 유실 방지를 위해 localStorage 사용함
                if (typeof ($('#state').val()) != 'undefined' && $('#state').val().length > 0) {
                    localStorage.setItem('state', $('#state').val());
                } else {
                    // 웹 cafe24에서 사용하다가 웹 community에서 로그인 하는경우 cafe24에서 사용하던 state를 가져오면 복호화 에러 발생함. 해당부분 예외처리
                    if ($('#redirect_uri').val().indexOf('i-vory.shop') == -1 && localStorage.getItem('state') != null && localStorage.getItem('state') != localStorage.getItem('state').toLowerCase())
                        $('#state').val(localStorage.getItem('state'));
                }

                if (typeof ($('#error').val()) != 'undefined' && $('#error').val().length > 0) {
                    callAlert({ title: '', content: '찾을수 없는 계정입니다.', btn: '확인' });
                }

                if (get_cookie('UserToken') == 'logout') {
                    var todayDate = new Date();
                    todayDate.setDate(todayDate.getDate());
                    document.cookie = "UserToken=" + escape('') + "; path=/; expires=" + todayDate.toGMTString() + ";"
                    document.cookie = "UserInfo=" + escape('') + "; path=/; expires=" + todayDate.toGMTString() + ";"
                }
                // 쇼핑몰 인덱스(auto_login.html)에서 userAgent=ivory4.0일때 SSO 로그인 창을 winSnsLoginApp 이름으로 연다. 
                // 이때는 로그인처리 후 창 자동 종료하도록 파라미터를 추가한다.
                if (window.name == 'winSnsLoginApp' || window.name == 'sso') {
                    $('#autoClose').val('Y');
                }
                if (USER_AGENT.indexOf('ivory_3.0') > 0) {
                    if ($('#login_check') == "Y") {
                        if (window.name == 'sso') {
                            window.location.replace($('#target').val() + '?response_type=' + $('#response_type').val() + '&client_id=' + $('#client_id').val() + '&redirect_uri=' + $('#redirect_uri') + '&state=' + localStorage.getItem('state'));
                        }
                        $('.login').hide();
                        // 중복 로그인 검사
                        if (await login_dup_check(frcsCustNo)) {
                            console.log('oauth login & login_dup_check success');
                            auto_login_app($('#target').val());
                        } else {
                            await callApi(SVR_URL + '/logout.json', 'POST', { frcsCustNo: frcsCustNo });
                            callAlert({ title: '', content: '다른 기기에서 로그인하여 로그아웃됩니다.', btn: '확인' });
                            setTimeout(
                                () => { window.location.reload() }, 1000
                            );
                        }
                    } else {
                        // oauth서버 로그인 되지 않았지만 login_token 쿠키를 갖고 있는 기존회원의 경우 ivory 진입 시도
                        if (typeof get_cookie("login_token") != 'undefined' && get_cookie("login_token").length > 0) {
                            // 중복 로그인 검사
                            if (await login_dup_check(frcsCustNo)) {
                                console.log('old member & login_dup_check success');
                                $('.login').hide();
                                $('#origin_member_authentication').show();
                                window.location.replace('https://i-vory.net/index.do?' + atob(localStorage.getItem('state')));
                            } else {
                                console.log('old member & login_dup_check fail');
                                callAlert({ title: '', content: '다른 기기에서 로그인하여 로그아웃됩니다.', btn: '확인' });
                                setTimeout(() => {
                                    shop_logout(frcsCustNo);
                                    $('.login').show();
                                }, 1000
                                );
                            }
                        } else {
                            console.log('oauth not login & login_token not exist');
                            $('.login').show();
                        }
                    }
                    userAgent.value = "ivory";
                } else {
                    if ($('#login_check') == "Y") {
                        auto_login_shop($('#target').val());
                    } else {
                        $('.login').show();
                    }
                    userAgent.value = "elseWeb";
                }
                console.log("userAgent : " + userAgent.value);
            };

            function find_password() {
                var form = document.getElementById("location_form");
                form.method = 'GET';
                form.action = SVR_URL + '/oauth/customer/findId.do#tab_02';
                form.submit();
            };

            function join() {
                var form = document.getElementById("location_form");
                form.method = 'GET';
                form.action = SVR_URL + '/oauth/customer/join.do';
                form.submit();
            }

            function find_id() {
                var form = document.getElementById("location_form");
                form.method = 'GET';
                form.action = SVR_URL + '/oauth/customer/findId.do';
                form.submit();
            }

            function auto_login_app(target) {
                window.location.replace('/oauth/authorize?response_type=code&client_id=iandna77&client_secret=$2a$10$8zrGK5jvv.fYuWrr3qN/8.wYTAVA4u2ocvI1QnMP5nM8yS0uweazu&redirect_url=https://i-vory.net/oauth/callback&state=' + localStorage.getItem('state'));
            };

            function auto_login_shop(target) {
                var temp;
                if ($('#referrer').val().indexOf('i-vory.net/community') > -1) {
                    temp = $('#referrer').val();
                    temp = temp.replace('https://i-vory.net/', '');
                    temp = "url=" + temp;
                    temp = btoa(temp);
                    window.location.replace('/oauth/authorize?response_type=code&client_id=iandna77&client_secret=$2a$10$8zrGK5jvv.fYuWrr3qN/8.wYTAVA4u2ocvI1QnMP5nM8yS0uweazu&redirect_url=https://i-vory.net/oauth/callback&state=' + temp);
                } else if ($('#redirect_uri').val().indexOf('i-vory.shop') > -1) { // cafe24 sso call
                    window.location.replace($('#target').val() + '?response_type=' + $('#response_type').val() + '&client_id=' + $('#client_id').val() + '&redirect_uri=' + $('#redirect_uri') + '&state=' + localStorage.getItem('state'));
                    //window.location.replace('https://m.i-vory.shop/application/ivory40/ssoLoginShop.html?url='+$('#pathname').val());
                } else {
                    temp = "url=index.do";
                    temp = btoa(temp);
                    window.location.replace('/oauth/authorize?response_type=code&client_id=iandna77&client_secret=$2a$10$8zrGK5jvv.fYuWrr3qN/8.wYTAVA4u2ocvI1QnMP5nM8yS0uweazu&redirect_url=https://i-vory.net/oauth/callback&state=' + temp);
                }

            };

            function logoutTarget() {
                document.getElementById("target").value = "/oauth/login";
                return true;
            }

            function info_valid() {
                console.log('asd');
                var password = $('#password').val();
                var email = $('#username').val();
                if (email.length < 1) {
                    callAlert({ title: '', content: '아이디를 입력해주세요.', btn: '확인' });
                    return;
                }
                if (password.length < 1) {
                    callAlert({ title: '', content: '비밀번호를 입력해주세요.', btn: '확인' });
                    return;
                }

                if ($('#password').val().length > 7 && $('#password').val().length < 17) {
                    $('#login_form').submit();
                } else {
                    callAlert({ title: '', content: '찾을 수 없는 계정입니다.', btn: '확인' });
                    return;
                }
            }

            async function login_dup_check(frcsCustNo) {
                var data;
                var param = { frcsCustNo: frcsCustNo, eqNo: atob(localStorage.getItem('state')).split('&')[2].substring(6) };
                data = await callApi(SVR_URL + '/native/loginCheck.json', 'POST', param);
                //data = await callApi('https://i-vory.net'+'/native/loginCheck.json', 'POST', param);
                if (data.result.checkLoginFlag == 'N')
                    return false;
                if (data.result.checkLoginFlag == 'Y')
                    return true;
                return true;
            }

            function shop_logout(frcsCustNo) {
                window.location.href = "https://i-vory.net/logout.do?frcsCustNo=" + frcsCustNo + "&url=https://m.i-vory.shop/application/30/proxy_logout.html";
            }
        </script>

        </html>