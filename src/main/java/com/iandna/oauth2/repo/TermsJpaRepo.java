package com.iandna.oauth2.repo;

import com.iandna.oauth2.entity.Terms;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TermsJpaRepo extends JpaRepository<Terms, String> {
    @Query("SELECT t FROM Terms t WHERE t.stCd = '1' AND t.stCd <> '7' " +
            "ORDER BY CASE t.gbnCd " +
            "WHEN '6' THEN '1' " +
            "WHEN '10' THEN '2' " +
            "WHEN '3' THEN '3' " +
            "WHEN '4' THEN '4' " +
            "WHEN '5' THEN '5' " +
            "WHEN '9' THEN '6' " +
            "ELSE '7' END")
    List<Terms> selectTerms();
    List<Terms> findByStCdOrderBySeqNo(String stCd);
    
}
