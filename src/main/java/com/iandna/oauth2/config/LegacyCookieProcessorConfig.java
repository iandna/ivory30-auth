//package com.iandna.oauth2.config;
//
//import org.apache.tomcat.util.http.LegacyCookieProcessor;
//import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
//import org.springframework.boot.web.server.WebServerFactoryCustomizer;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration(proxyBeanMethods = false)
//public class LegacyCookieProcessorConfig {
//	@Bean
//    public WebServerFactoryCustomizer<TomcatServletWebServerFactory> cookieProcessorCustomizer() {
//        return (factory) -> factory
//                .addContextCustomizers((context) -> context.setCookieProcessor(new LegacyCookieProcessor()));
//    }
//}
