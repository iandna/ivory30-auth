$(document).ready(function () {
    activeInputBtn();
    deleteInputValue();
    showPassword();
    proposalEmail();
    agreeCheckbox();
    onReadyPopup();
    tabMenu();
    findAuthentication();
    checkChangePasswordBtn();
    previewImageUpload();
    selectCharacter();
    setNickName();

    changeTextAreaLine();
    attachmentFile();
    scrollspyCategory();
    toggleCollapse();
    deleteProfileImg();
    // test 인증중복 모달
    // onCheckDuplication();

    // test alert
    // callAlert({
    //     title: '테스트용 alert',
    //     content: '페이지가 열리면 테스트용 alert이 바로 떠버린다',
    //     btn: '확인',
    // });
    // test error message
    $("registId").removeClass("error");
});

// 001. 인풋 텍스트 제거버튼
function deleteInputValue() {
    $(document).on("click", ".form_input ~ .delete", function () {
        if ($(this).attr("type") !== "password") {
            $(this).siblings(".form_input").val("");
            checkBtnAuthentication();
            $(this).css({
                display: "none",
                opacity: 0,
            });
            $(this).siblings(".form_input").removeClass("error");
        }
    });
}

// 002. 인풋 텍스트 버튼 활성화
function activeInputBtn() {
    $(document).on("keyup", ".form_input", function () {
        var btn = $(this).siblings(".input_icon");
        if ($(this).val().trim() != "") {
            btn.css({
                display: "block",
            });
            btn.animate(
                {
                    opacity: 0.7,
                },
                300
            );
        } else {
            btn.css({
                display: "none",
                opacity: 0,
            });
        }
    });
}

// 003. 비밀번호 보이기
function showPassword() {
    $(".form_input ~ .show_password .show").css("display", "block");
    $(document).on("click", ".form_input ~ .show_password", function () {
        if ($(this).hasClass("show")) {
            $(this).siblings(".form_input").attr("type", "text");
            $(this).siblings(".form_input").focus();
            $(this).find(".show").css("display", "none");
            $(this).find(".hide").css("display", "block");
            $(this).addClass("hide");
            $(this).removeClass("show");
        } else {
            $(this).siblings(".form_input").attr("type", "password");
            $(this).siblings(".form_input").focus();
            $(this).find(".show").css("display", "block");
            $(this).find(".hide").css("display", "none");
            $(this).addClass("show");
            $(this).removeClass("hide");
        }
    });
}

// 004. 이메일
function proposalEmail() {
    // 입력 또는 포커스일때 이메일 리스트 보이기
    // $(document).on('change input', '#registId', function () {
    $(document).on("change input", ".auto_email", function () {
        // console.log('asd');
        var emailValue = $(this).val();
        // var emailList = $('#emailList');
        var emailList = $(this).siblings(".email_list");
        if (emailValue.indexOf("@") > 0 && emailValue.indexOf("@") + 1 >= emailValue.length) {
            // console.log('emailValue');
            for (var i = 0; emailList.length > i; i++) {
                var item = $(emailList[i]).find("li");
                item.find(".input_value").html($(this).val());
            }
            emailList.css("display", "block");
            emailList.animate(
                {
                    height: 185,
                },
                300
            );
        } else {
            if (emailList.css("display") != "none") {
                emailList.animate(
                    {
                        height: 0,
                    },
                    300
                );
                setTimeout(function () {
                    emailList.css("display", "none");
                }, 300);
            }
        }
    });

    // 이메일 리스트 클릭 시 인풋에 적용
    // $(document).on('click', '#emailList li', function () {
    $(document).on("click", ".email_list li", function () {
        // var target = $('#registId');
        var target = $(this).closest(".form_group").find(".auto_email");
        var targetValue = target.val();
        var emailList = $(this).closest(".email_list");
        target.val(targetValue + $(this).find(".email_address").html());

        emailList.animate(
            {
                height: 0,
            },
            300
        );
        setTimeout(function () {
            emailList.css("display", "none");
        }, 300);

        if ($("#registId").val() != null && typeof $("#registId").val() != "undefined") {
            if ($("#registId").val().length == 0) {
                $("#registId").removeClass("error");
            } else if ($("#registId").val().indexOf('@') > 12) {
                onErrorMessage({
                    message: "이메일 주소를 제외한 아이디 길이를 12자 이내로 설정해주세요.",
                    target: "#registId",
                });
            } else if (!validateEmail($("#registId").val())) {
                onErrorMessage({
                    message: "이메일 형식을 확인해주세요.",
                    target: "#registId",
                });
            } else if ($("#duplication_state").length > 0 && $("#duplication_state").val() != "y") {
                onErrorMessage({
                    message: "이메일 중복체크 버튼을 눌러주세요.",
                    target: "#registId",
                });
            }
        }
    });

    // 인풋 포커스 아웃일때 리스트 끄기
    $(document).on("blur", ".auto_email", function () {
        var emailList = $(this).siblings(".email_list");
        emailList.animate(
            {
                height: 0,
            },
            300
        );
        setTimeout(function () {
            emailList.css("display", "none");
        }, 300);

        if ($("#registId").val() != null && typeof $("#registId").val() != "undefined") {
            if ($("#registId").val().length == 0) {
                $("#registId").removeClass("error");
            } else if ($("#registId").val().indexOf('@') > 12) {
                onErrorMessage({
                    message: "이메일 주소를 제외한 아이디 길이를 12자 이내로 설정해주세요.",
                    target: "#registId",
                });
            } else if (!validateEmail($("#registId").val())) {
                onErrorMessage({
                    message: "이메일 형식을 확인해주세요.",
                    target: "#registId",
                });
            } else if ($("#duplication_state").length > 0 && $("#duplication_state").val() != "y") {
                onErrorMessage({
                    message: "이메일 중복체크 버튼을 눌러주세요.",
                    target: "#registId",
                });
            }
        }
    });

    // 이메일 유효성 검사
    $(document).on("propertychange change keyup paste input", "#registId", function () {
        $("#duplication_state").val("n");
        if (!validateEmail($("#registId").val()) || $("#registId").val().indexOf("@") > 13 || $("#registId").val() == $("#registId").val().toLowerCase()) {
            onErrorMessage({
                message: "이메일 형식을 확인해주세요.",
                target: "#registId",
            });
        } else {
            $("#registId").removeClass("error");
        }
        checkBtnAuthentication();
    });

    // 패스워드 유효성 검사
    $(document).on("propertychange change keyup paste input", "#registPassword", function () {
        if (!validatePassword($("#registPassword").val())) {
            onErrorMessage({
                message: "영문 대소문자/숫자/특수문자 중 2가지 이상 조합, 8~16자",
                target: "#registPassword",
            });
        } else {
            $("#registPassword").removeClass("error");
        }
        checkBtnAuthentication();
    });

    // 패스워드 확인 검사
    $(document).on("propertychange change keyup paste input", "#registPasswordConfirm", function () {
        if ($("#registPassword").val() != $("#registPasswordConfirm").val()) {
            onErrorMessage({
                message: "비밀번호와 일치하지 않습니다.",
                target: "#registPasswordConfirm",
            });
        } else {
            $("#registPasswordConfirm").removeClass("error");
        }
        checkBtnAuthentication();
    });
}

// 005. 동의버튼
function agreeCheckbox() {
    var agreeList = $("#agreeList");
    var btnAuthentication = $("#btnAuthentication");

    // 전체동의
    $(document).on("change", "#agreeBulk", function () {
        if ($(this).is(":checked")) {
            agreeList.find("input:checkbox").each(function () {
                this.checked = true;
            });
            /*
            if( $('#registPassword').val() != '' &&
        		$('#registPassword').val() == $('#registPasswordConfirm').val() ) {
                	btnAuthentication.attr('disabled', false);
            }
            */
        } else {
            agreeList.find("input:checkbox").each(function () {
                this.checked = false;
            });
            // btnAuthentication.attr('disabled', true);
        }

        changeAgreeValue();
        checkBtnAuthentication();
    });

    // 필수동의 인증버튼 활성화
    $(document).on("change", "#agreeService, #agreePrivicy, #agreeTotal", function () {
        checkBtnAuthentication();
    });

    // 동의정보 값 개별 체크
    $(document).on("change", "#agreeService, #agreePrivicy, #agreeTotal, #agreeInfo, #agreePrivicy2", function () {
        changeAgreeValue();
    });
    
    $(document).on("change", "#agreeInfo", function () {
        if($("#agree8").val() == "N")
        	callAlert({ title: '', content: '미동의 시 맞춤 콘텐츠 추천 및 혜택·이벤트 등 다양한 정보 안내 서비스를 받으실 수 없습니다.', btn: '확인' });
    });
}

// 005-1. 동의정보 값 변경
function changeAgreeValue() {
    $("#agree6").val($("#agreeService").prop("checked") ? "Y" : "N");
    $("#agree7").val($("#agreePrivicy").prop("checked") ? "Y" : "N");
    $("#agree3").val($("#agreeTotal").prop("checked") ? "Y" : "N");
    //	$('#agree4').val( ($('#agreeInfo').prop('checked')) ? 'Y' : 'N' );
    $("#agree8").val($("#agreeInfo").prop("checked") ? "Y" : "N");
    $("#agree5").val($("#agreePrivicy2").prop("checked") ? "Y" : "N");
}

function agreeAllTerms() {
    document.getElementById("agreeBulk").checked = true;
    document.getElementById("agreeService").checked = true;
    document.getElementById("agreePrivicy").checked = true;
    document.getElementById("agreeTotal").checked = true;
    document.getElementById("agreeInfo").checked = true;
    document.getElementById("agreePrivicy2").checked = true;
}

// 007. 인증버튼 활성화
function checkBtnAuthentication() {
    var btnAuthentication = $("#btnAuthentication");
    if (
        $("#agreeService").prop("checked") &&
        $("#agreePrivicy").prop("checked") &&
        $("#agreeTotal").prop("checked") &&
        $("#registPassword").val() != "" &&
        $("#registPassword").val() == $("#registPasswordConfirm").val() &&
        $("#duplication_state").val() == "y" &&
        validateEmail($("#registId").val()) &&
        validatePassword($("#registPassword").val())
    ) {
        console.log($("#registId").val());
        // 22.04.19 ajy
        // disabled 속성 제거, 클래스로 대체
        // btnAuthentication.attr('disabled', false);
        btnAuthentication.removeClass("disabled");
        console.log(true);
    } else {
        // 22.04.19 ajy
        // disabled 속성 제거, 클래스로 대체
        // btnAuthentication.attr('disabled', true);
        btnAuthentication.addClass("disabled");
        console.log(false);
    }
}

// 008. 발리데이션 메세지
function onErrorMessage({ message, target }) {
    var errorMessage = "<p>" + message + "</p>";
    var errorBox = $(target).siblings(".error_message");

    errorBox.html("");
    errorBox.append(errorMessage);
    $(target).addClass("error");

    // error message는 인풋의 error 클래스 제거로 초기화
}

//008-1. 발리데이션 메세지(색상지정)
function onErrorMessageGreen({ message, target }) {
    var errorMessage = "<p>" + message + "</p>";
    var errorBox = $(target).siblings(".error_message");

    errorBox.html("");
    errorBox.append(errorMessage);
    $(target).addClass("error");
    errorBox.children().css("color", "#68C0B5");
    // error message는 인풋의 error 클래스 제거로 초기화
}

// 009. 약관 팝업
function onReadyPopup() {
    $(document).on("click", "#agreeList > li > a", function (e) {
        e.preventDefault();
        var value = {
            title: "",
            value: "",
            target: "",
        };
        var target = $(this).attr("data-popup");
        switch (target) {
            case "agreeService":
                value.title = $("#term_title1").val();
                value.text = $("#term_content1").val();
                value.target = target;
                break;
            case "agreePrivicy":
                value.title = $("#term_title2").val();
                value.text = $("#term_content2").val();
                value.target = target;
                break;
            case "agreeTotal":
                value.title = $("#term_title3").val();
                value.text = $("#term_content3").val();
                value.target = target;
                break;
            case "agreeInfo":
                value.title = $("#term_title4").val();
                value.text = $("#term_content4").val();
                value.target = target;
                break;
            case "agreePrivicy2":
                value.title = $("#term_title5").val();
                value.text = $("#term_content5").val();
                value.target = target;
                break;
        }
        callPopup(value);
    });
}

// 010. 본인인증 모달팝업
function onCheckDuplication() {
    var modal = $("#authDuplication");
    var body = $("body");
    modal.css("display", "flex");
    body.addClass("modal_on");
    closeCheckDuplication();
}

// 011. 본인인증 모달 닫기
function closeCheckDuplication() {
    var modal = $("#authDuplication");
    var body = $("body");
    $(document).on("click", "#authDuplication .result_close", function () {
        modal.addClass("closing");
        setTimeout(function () {
            modal.css("display", "none");
            modal.removeClass("closing");
            body.removeClass("modal_on");
        }, 300);
    });
}

// 012. 탭메뉴
function tabMenu() {
    $(document).on("click", ".tab_btn", function (e) {
        e.preventDefault();
        var target = $(this).attr("href");
        var siblings = $(this).siblings(".tab_btn");

        // btn
        $(this).addClass("active");
        $(this).closest(".tab_wrapper").removeClass(siblings.attr("href").split("#").pop());
        $(this).closest(".tab_wrapper").addClass(target.split("#").pop());
        siblings.removeClass("active");

        // content box
        var index = target.split("#tab_").pop() * 1 - 1;
        $(".tab_item:eq(0)").animate(
            {
                "margin-left": index * -100 + "%",
            },
            100
        );
    });

    // 활성화탭 설정
    if (location.href.indexOf("#tab_") != -1) {
        var href = location.href.split("#").pop();
        $(".tab_wrapper ").addClass(href);
        $(".tab_header").removeClass("active");
        $('.tab_btn[href="#' + href + '"]').addClass("active");
        $(".tab_item:eq(0)").css({
            "margin-left": (href.split("tab_").pop() * 1 - 1) * -100 + "%",
        });
    } else {
        $(".tab_header .tab_btn:eq(0)").addClass("active");
    }
}

// 013. 아이디비밀번호찾기 본인인증버튼
function findAuthentication() {
    // 임시 버튼 연결
    /*
    $(document).on('click', '#btnFindId', function () {
        if (true) findIdChangeUI();
    });
    $(document).on('click', '#btnFindPassword', function () {
        if (true) findPassWordChangeUI();
    });
    */
}

// 014. 아이디찾기 ui변경
function findIdChangeUI() {
    var testemail = "eeeeee@dddddd.email";
    var testdate = "2005.02.02";

    $("#userEmail").html(testemail);
    $("#registDate").html(testdate + " 가입");

    $("#imgFindId").prop("hidden", true);
    $("#imgFindIdResult").prop("hidden", false);
    $("#findIdMessage").prop("hidden", false);
    $("#findIdUserInfo").prop("hidden", false);
    $("#btnFindId").prop("hidden", true);
    $("#btnFindIdLogin").prop("hidden", false);

    $(".find_login_box a").prop("hidden", true);
    $("#btnFind").prop("hidden", false);

    // 비밀번호 찾기 이동버튼
    $(document).on("click", "#btnFind", function () {
        $(".tab_header .tab_btn:eq(1)").click();
    });
}

// 015. 비밀번호찾기 ui변경
function findPassWordChangeUI() {
    $("#findPasswordTitle").html("비밀번호 변경");

    $("#passwordFindBox").prop("hidden", true);
    $(".change_password_box").prop("hidden", false);
    $("#inputFindPassword").focus();
}

// 016. 비밀번호 변경 버튼체크
function checkChangePasswordBtn() {
    // 22.04.19 ajy
    // disabled 속성 클래스 대체
    // $("#btnChangePassword").attr("disabled", true);
    $("#btnChangePassword").addClass("disabled");
    $(document).on("click", "#btnChangePassword", async function () {
        var password = $("#inputFindPassword").val();
        var passwordConfirm = $("#inputFindPasswordConfirm").val();

        if (password.length == 0) {
            onErrorMessage({
                message: "패스워드를 입력해주세요.",
                target: "#inputFindPassword",
            });
            callAlert({ title: "", content: "패스워드를 입력해주세요.", btn: "확인" });
            $("#btnChangePassword").addClass("disabled");
            return;
        }
        if (!validatePassword(password)) {
            onErrorMessage({
                message: "영문 대소문자/숫자/특수문자 중 2가지 이상 조합, 8~16자",
                target: "#inputFindPassword",
            });
            callAlert({ title: "", content: "비밀번호를 다시 입력해주세요.", btn: "확인" });
            $("#btnChangePassword").addClass("disabled");
            return;
        }
        if (password != passwordConfirm) {
            onErrorMessage({
                message: "비밀번호와 비밀번호 확인이 일치하지 않습니다.",
                target: "#inputFindPassword",
            });
            callAlert({ title: "", content: "비밀번호 확인을 다시 입력해주세요.", btn: "확인" });
            $("#btnChangePassword").addClass("disabled");
            return;
        }
        if (password.trim() != "" && password == passwordConfirm) {
            // validate
            var data = {};
            data = await callApi("/oauth/customer/resetPassword.json", "post", {
                csId: $("#registId").val(),
                upHash: $("#veri_up_hash").val(),
                password: password,
            });
            console.log(data);
            $(".tab_modal").animate(
                {
                    left: 0,
                },
                300
            );
        }
    });

    // 변경버튼 재활성화
    $(document).on("keyup", "#inputFindPassword, #inputFindPasswordConfirm", function () {
        var password = $("#inputFindPassword").val();
        var passwordConfirm = $("#inputFindPasswordConfirm").val();
        if (!validatePassword(password)) {
            onErrorMessage({
                message: "영문 대소문자/숫자/특수문자 중 2가지 이상 조합, 8~16자.",
                target: "#inputFindPassword",
            });
        } else {
            $("#inputFindPassword").removeClass("error");
        }
        if (password != passwordConfirm && passwordConfirm.length > 0) {
            onErrorMessage({
                message: "변경할 비밀번호와 일치하지 않습니다.",
                target: "#inputFindPasswordConfirm",
            });
        } else {
            $("#inputFindPasswordConfirm").removeClass("error");
        }

        if (password.trim() != "" && passwordConfirm.trim() != "" && password == passwordConfirm) {
            // 22.04.19 ajy
            // disabled 속성 클래스 대체
            // $("#btnChangePassword").attr("disabled", false);
            $("#btnChangePassword").removeClass("disabled");
            $("#inputFindPassword").removeClass("error");
            $("#inputFindPasswordConfirm").removeClass("error");
        }
    });
}

// 017. 이미지업로드 프리뷰
function previewImageUpload() {
    $(document).on("change", "#profileImageUploader", function (e) {
        // console.log(e.target.files);
        $("#characterNo").val(0);
        if (e.target.files && e.target.files[0]) {
            const reader = new FileReader();

            reader.onload = (e) => {
                var previewImage = $("#previewImage");
                previewImage.attr("src", e.target.result);
            };
            reader.readAsDataURL(e.target.files[0]);

            $("#btnDeleteImg").css("display", "block");
        }
    });
}

// 018. 프로필 캐릭터
function selectCharacter() {
    // 캐릭터 선택 모달 보이기
    $(document).on("click", "#profileSelectCharacter", function () {
        $("#characterSelecter").css("display", "flex");
        $("body").addClass("modal_on");
    });

    // 캐릭터 선택 모달 닫기
    $(document).on("click", "#closeCharacter, #characterSelecter .back", function () {
        closeCharacter();
    });

    function closeCharacter() {
        $("#characterSelecter").addClass("closing");

        setTimeout(function () {
            $("#characterSelecter").css("display", "none");
            $("#characterSelecter").removeClass("closing");
            $("body").removeClass("modal_on");
        }, 300);
    }

    // 캐릭터 선택
    $(document).on("click", "#characterList button", function (e) {
        var src = $(this).find("img").attr("src");

        $("#characterPreview").attr("src", src);
        $("#characterList button").removeClass("active");
        $(this).addClass("active");
    });

    // 캐릭터 입력
    $(document).on("click", "#selectCharacter", function () {
        var src = $("#characterPreview").attr("src");
        $("#previewImage").attr("src", src);

        $("#btnDeleteImg").css("display", "block");
        closeCharacter();
    });
}

function deleteProfileImg() {
    $(document).on("click", "#btnDeleteImg", function () {
        $(this).css("display", "none");
        $("#previewImage").attr("src", "/oauth/resources/assets/images/profile_base.svg");
        $("#profileImageUploader").val("");
    });
}

// 020. 가입완료 닉네임 설정
function setNickName() {
    // var nickName = '김또띠';
    var nickName = decodeURI(get_cookie("nickname"));
    $("#userName").html(nickName);
}

// 021. 이메일 유효성 체크 함수
function validateEmail(email) {
    var valid = /^[0-9a-zA-Z]([-_]*[0-9a-zA-Z])*[-_]*@[0-9a-zA-Z]([-_.]*[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/;
    return valid.test(email);
}

//022. 패스워드 유효성 체크 함수
function validatePassword(password) {
    var chk = 0;
    if (password.search(/[0-9]/g) != -1) chk++;
    if (password.search(/[a-z]/gi) != -1) chk++;
    if (password.search(/[!@#$%^&*()?_~]/g) != -1) chk++;
    if (chk < 2) return false;
    if (password.length > 16 || password.length < 8) return false;

    return true;
}
//textarea 줄바꿈
function changeTextAreaLine() {
    //console.log(_this.val())
    $(document).on("keyup keydown change", ".textarea_auto_line", (e) => {
        var _this = $(e.target);

        var lineHeight = _this.css("line-height").split("px")[0] * 1;
        var paddingTop = _this.css("padding-top").split("px")[0] * 1;
        var paddingBottom = _this.css("padding-bottom").split("px")[0] * 1;
        var borderTop = _this.css("border-top-width").split("px")[0] * 1;
        var borderBottom = _this.css("border-bottom-width").split("px")[0] * 1;
        var limitLine = _this.attr("data-limit-line") * 1 + 1;

        if (_this.prop("scrollHeight") <= lineHeight * limitLine) {
            _this.height(1).height(_this.prop("scrollHeight") - (paddingTop + paddingBottom + borderTop + borderBottom));
        }
    });
}

// 파일첨부
function attachmentFile() {
    const fileinput = $("#attachmentFile");
    console.log(fileinput);
    // 버튼클릭 첨부
    $(document).on("click", "#btnAttachmentFile", (e) => {
        fileinput.trigger("click");
    });

    // 이름표시
    $(document).on("change", "#attachmentFile", (e) => {
        const target = e.target.files[0];
        var filename;
        if(target == null || target == undefined) 
        	return;
        else
        	filename = target.name;
        if (filename != undefined) fileinput.siblings(".fake_input").val(filename);
        else fileinput.siblings(".fake_input").val("");
    });

    // 삭제
    $(document).on("click", "#btnDeleteFile", (e) => {
        fileinput.siblings(".fake_input").val("");
        fileinput.val("");
    });
}

// 자주하는질문 카테고리 스크롤 스파이
function scrollspyCategory() {
    console.log("a");
    const button = $("[name=faqCategory]").siblings("label");

    button.on("click", (e) => {
        const margin = $(".tab_wrapper").css("margin-left").split("px")[0] * 1;

        const width = $("#categoryList").outerWidth();
        const scroll = $("#categoryList").scrollLeft();
        const offset = $(e.target).offset();

        $("#categoryList").scrollLeft(scroll + (offset.left - margin - width / 2) + ($(e.target).width() / 2 + margin));
    });
}

// 자주하는질문 리스트 컬랩스
function toggleCollapse() {
    $(document).on("click", ".btn_collapse", (e) => {
        const item = $(e.target).closest(".collapse_item");
        const hidebox = item.find(".hide_box");
        const title = $(e.target).find(".title");

        if (!item.hasClass("open")) {
            hidebox.css("display", "flex");
            item.addClass("open");
            hidebox.animate(
                {
                    height: hidebox.prop("scrollHeight"),
                },
                300
            );
            //            2022.05.18 자주하는질문 타이틀 높이 애니메이션 제거
            //            title.animate(
            //                {
            //                    height: title.prop("scrollHeight"),
            //                },
            //                300
            //            );
        } else {
            item.removeClass("open");
            //            title.animate(
            //                {
            //                    height: $(document).width() * (24 / 640),
            //                },
            //                300
            //            );
            hidebox.animate(
                {
                    height: 0,
                },
                300,
                () => {
                    hidebox.css("display", "none");
                }
            );
        }
    });
}
