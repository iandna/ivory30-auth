<%@ page language="java" contentType="text/html;charset=utf-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
        />

        <!-- css -->
        <link rel="stylesheet" href="/oauth/resources/assets/css/font.css?date=20221206111700" />
        <link rel="stylesheet" href="/oauth/resources/assets/css/common.css?date=20221206111700" />
        <link rel="stylesheet" href="/oauth/resources/assets/css/animation.css?date=20221206111700" />
        <link rel="stylesheet" href="/oauth/resources/assets/css/complete.css?date=20221206111700" />

        <title>Regist</title>
        
        <script type='text/javascript' src='/oauth/resources/js/common.js?date=20221206111700'></script>
		<script type='text/javascript' src='/oauth/resources/js/api.js?date=20221206111700'></script>
		<link type="text/css" rel="stylesheet" href="/oauth/resources/css/common.css?date=20221206111700">
    </head>
    <body>
        <div class="container complete">
            <div class="welcome_box">
                <div class="text_1">
                    안녕하세요!<br />
                    <span id="userName"></span>님.
                </div>
                <div class="text_2">
                    회원가입이 완료되었습니다.
                    <br />
                    <br />
                    지금부터 육아의 처음을<br />아이보리와 함께 해요!
                </div>
                <div class="logo">
                    <img src="/oauth/resources/assets/images/logo.svg" alt="Ivory" />
                </div>
            </div>
            <button type="button" id="startIvory" class="btn_start" onclick="ivory_start();">
                아이보리 시작하기
            </button>
        </div>

        <!-- jquery -->
        <script type="text/javascript" src="/oauth/resources/assets/js/jQuery.js"></script>

        <!-- common -->
        <script type="text/javascript" src="/oauth/resources/assets/js/common.js"></script>
    </body>
    <script type="text/javascript">
    	function ivory_start() {
    		if(SVR_URL.indexOf('iandna-ivory.com') > 0) {
    			window.open("https://iandna-ivory.com/oauth/authorize?response_type=code&client_id=testClientId&client_secret=$2a$10$1qw9m8GTeZPC2hBAVAJfE.RTh1qAMtBx/LwCqAgDyTA0gkThQPcoK&redirect_url=https://iandna-ivory.com/oauth/callback&state=" + localStorage.getItem('state'), "_parent");
    		}else {
    			window.open("https://i-vory.net/oauth/authorize?response_type=code&client_id=iandna77&client_secret=$2a$10$8zrGK5jvv.fYuWrr3qN/8.wYTAVA4u2ocvI1QnMP5nM8yS0uweazu&redirect_url=https://i-vory.net/oauth/callback&state=" + localStorage.getItem('state'), "_parent");	
    		}
    	}
    </script>
</html>
