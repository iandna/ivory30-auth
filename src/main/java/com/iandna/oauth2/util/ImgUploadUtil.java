package com.iandna.oauth2.util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Component
public class ImgUploadUtil {

//	@Resource(name = "config")
//	Properties prop;
	private static Logger logger = LoggerFactory.getLogger(ImgUploadUtil.class);
	
	@Value("${server.file.path}")
	private String serverFilePath;
	
	@Value("${s3.upload.bucket}")
	private String s3UploadBucket;

	@Value("${server.file.url}")
	private String serverFileUrl;
	
	@Resource(name = "imgUtil")
	ImageUtil imgUtil;
	
	@Autowired
	FileUtil fileUtil;
	
	public TAData imgUpload(TAData param, HttpServletRequest request) throws Exception {
		  MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
	      
	      String savePath = serverFilePath;
	      
	      String s3UploadPath = param.getString("s3UploadPath");
	      

	      File dir = new File(savePath);
	      if (!dir.exists()) {
	        dir.mkdirs();
	      }
	      

	      long time = System.currentTimeMillis();
	      SimpleDateFormat day = new SimpleDateFormat("yyyyMMdd");
	      SimpleDateFormat dayTime = new SimpleDateFormat("HHmmss");
	      String str = day.format(new Date(time));
	      String str2 = dayTime.format(new Date(time));
	      String imgUrl = null;
	      
	      for(int i = 1; i < 16; i++) {
	    	  
				if (param.getString("imgFileValid" + i).equals("true")) {
					MultipartFile file = multipartRequest.getFile("imgFile" + i);
					String exName = StringUtils.substring(file.getOriginalFilename(), StringUtils.lastIndexOf(file.getOriginalFilename(), "."));
					String fileName = "pcc_" + str + str2 + "_img" + i + exName;

					fileUtil.uploadS3File(savePath, s3UploadBucket, s3UploadPath, file, fileName);
					
					if (i == 1) {
						imgUrl = serverFileUrl + s3UploadPath + fileName;
					} else {
						imgUrl = imgUrl + "^" + serverFileUrl + s3UploadPath + fileName;
					}
				}
	      }
	      param.set("imgUrl", imgUrl);
	      return param;
	}
	
	public String uploadImageFile(HttpServletRequest request, String s3UploadPath){
		String rtnVal = "";
		try{
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
			
			String savePath = serverFilePath;
			
	  
			File dir = new File(savePath);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			
			MultipartFile file = multipartRequest.getFile("file");
			long time = System.currentTimeMillis();
		    SimpleDateFormat day = new SimpleDateFormat("yyyyMMdd");
		    SimpleDateFormat dayTime = new SimpleDateFormat("HHmmss");
		    String str = day.format(new Date(time));
		    String str2 = dayTime.format(new Date(time));
	        String exName = StringUtils.substring(file.getOriginalFilename(), StringUtils.lastIndexOf(file.getOriginalFilename(), "."));
	        String fileName = StringUtils.replace(s3UploadPath, "/", "") + str + str2 + "_img" + exName;
	        
	        fileUtil.uploadS3File(savePath, s3UploadBucket, s3UploadPath, file, fileName);
        	rtnVal = serverFileUrl + s3UploadPath + fileName;
        } catch(Exception e)
        {
        	String test = e.toString();
        	e.printStackTrace();
        }

		return rtnVal;
	}
	
	public String uploadImageFile(HttpServletRequest request, String s3UploadPath, String imgFileName){
		String rtnVal = "";
		try{
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
			
			String savePath = serverFilePath;
			
	  
			File dir = new File(savePath);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			
			MultipartFile file = multipartRequest.getFile(imgFileName);
			long time = System.currentTimeMillis();
		    SimpleDateFormat day = new SimpleDateFormat("yyyyMMdd");
		    SimpleDateFormat dayTime = new SimpleDateFormat("HHmmss");
		    String str = day.format(new Date(time));
		    String str2 = dayTime.format(new Date(time));
	        String exName = StringUtils.substring(file.getOriginalFilename(), StringUtils.lastIndexOf(file.getOriginalFilename(), "."));
	        String fileName = StringUtils.replace(s3UploadPath, "/", "") + str + str2 + "_img" + exName;
	        
	        fileUtil.uploadS3File(savePath, s3UploadBucket, s3UploadPath, file, fileName);
        	rtnVal = serverFileUrl + s3UploadPath + fileName;
        } catch(Exception e)
        {
        	String test = e.toString();
        	e.printStackTrace();
        }
		return rtnVal;
	}
	
	public String uploadImageFile(MultipartFile file, String s3UploadPath, String imgFileName, String headStr){
		String rtnVal = "";
		try{
			String savePath = serverFilePath;
			
	  
			File dir = new File(savePath);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			
			String fileName = makeFileName(imgFileName, headStr);
	        logger.debug("final fileName : " + fileName);
	        fileUtil.uploadS3File(savePath, s3UploadBucket, s3UploadPath, file, fileName);
        	rtnVal = serverFileUrl + s3UploadPath + fileName;
        } catch(Exception e)
        {
        	String test = e.toString();
        	e.printStackTrace();
        }
		return rtnVal;
	}
	
	public String uploadImageFileWithResize(MultipartFile file, String s3UploadPath, String imgFileName, String headStr, int width){
		String rtnVal = "";
		try{
			String savePath = serverFilePath;
			
	  
			File dir = new File(savePath);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			
			String fileName = makeFileName(imgFileName, headStr);
	        logger.debug("final fileName : " + fileName);
	        fileUtil.uploadS3File(savePath, s3UploadBucket, s3UploadPath, file, fileName);
        	rtnVal = serverFileUrl + s3UploadPath + fileName;
        } catch(Exception e)
        {
        	String test = e.toString();
        	e.printStackTrace();
        }
		return rtnVal;
	}
	
	public TAData uploadImageFiles(HttpServletRequest request, String s3UploadPath){
		TAData result = new TAData();
		try{
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;

			String savePath = serverFilePath;
			
			
			File dir = new File(savePath);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			
			Iterator files = multipartRequest.getFileNames();
			String fileName = null;
			StringBuffer resultString = new StringBuffer();
			
			while(files.hasNext()) {
				fileName = (String)files.next();
				/*
				logger.debug("#### fileName : " + fileName);
				if(fileName == null || fileName.isEmpty()) {
					break;
				}
				*/
				
				MultipartFile multiFile = multipartRequest.getFile(fileName);
				long time = System.currentTimeMillis();
			    SimpleDateFormat day = new SimpleDateFormat("yyyyMMdd");
			    SimpleDateFormat dayTime = new SimpleDateFormat("HHmmss");
			    String str = day.format(new Date(time));
			    String str2 = dayTime.format(new Date(time));
		        String exName = StringUtils.substring(multiFile.getOriginalFilename(), StringUtils.lastIndexOf(multiFile.getOriginalFilename(), "."));
		        fileName = StringUtils.replace(s3UploadPath, "/", "") + str + str2 + "_img" + exName;
		        
		        fileUtil.uploadS3File(savePath, s3UploadBucket, s3UploadPath, multiFile, fileName);
	        	result.set(fileName, serverFileUrl + s3UploadPath + fileName);
				resultString.append(result.getString(fileName));
				resultString.append("^");
				logger.debug("#### fileName : " + result.getString(fileName));
			}
			if(resultString.length() > 0){
				resultString.deleteCharAt(resultString.length() - 1);
			}
			result.set("result_string", resultString.toString());
        } catch(Exception e) {
        	e.printStackTrace();
        }
		return result;
	}
	
	public TAData uploadImageMultiFiles(HttpServletRequest request, String s3UploadPath){
		TAData result = new TAData();
		try{
			String savePath = serverFilePath;
			
			
			File dir = new File(savePath);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			String fileName = null;
			StringBuffer resultString = new StringBuffer();
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
			Iterator files = multipartRequest.getFileNames(); // 파일 배열을 가진  input의 name 담김
			while(files.hasNext()) {
				String inputName = (String)files.next();
				List<MultipartFile> file_list = multipartRequest.getFiles(inputName);
				
				if(file_list.size() > 0) {
					for(MultipartFile multiFile : file_list) {
						long time = System.currentTimeMillis();
					    SimpleDateFormat day = new SimpleDateFormat("yyyyMMdd");
					    SimpleDateFormat dayTime = new SimpleDateFormat("HHmmss");
					    String str = day.format(new Date(time));
					    String str2 = dayTime.format(new Date(time));
					    String originName = multiFile.getOriginalFilename();
				        String exName = StringUtils.substring(originName, StringUtils.lastIndexOf(multiFile.getOriginalFilename(), "."));
				        fileName = StringUtils.replace(s3UploadPath, "/", "") + str + str2 + originName + "_img" + exName;
				        
				        fileUtil.uploadS3File(savePath, s3UploadBucket, s3UploadPath, multiFile, fileName);
			        	result.set(fileName, serverFileUrl + s3UploadPath + fileName);
						resultString.append(result.getString(fileName));
						resultString.append("^");
						logger.debug("#### fileName : " + result.getString(fileName));
					}
				}
			}
			if(resultString.length() > 0){
				resultString.deleteCharAt(resultString.length() - 1);
			}
			result.set("result_string", resultString.toString());
        } catch(Exception e) {
        	e.printStackTrace();
        }
		return result;
	}
	
	public TAData uploadImageMultiFilesWithResize(HttpServletRequest request, String s3UploadPath, int width){
		TAData result = new TAData();
		try{
			String savePath = serverFilePath;
			
			
			File dir = new File(savePath);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			String fileName = null;
			StringBuffer resultString = new StringBuffer();
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
			Iterator files = multipartRequest.getFileNames(); // 파일 배열을 가진  input의 name 담김
			
			while(files != null && files.hasNext()) {
				String inputName = (String)files.next();
				MultipartFile multipart_file = //multipartRequest.getFiles(inputName);
				multipartRequest.getFile(inputName);
				
				if(multipart_file != null && !multipart_file.isEmpty()) {
					multipart_file = imgUtil.imgResizeByWidth(multipart_file, width);
					long time = System.currentTimeMillis();
					fileName = makeFileName(inputName + "_" + multipart_file.getName(), "resize_");
					fileUtil.uploadS3File(savePath, s3UploadBucket, s3UploadPath, multipart_file, fileName);
		        	result.set(fileName, serverFileUrl + s3UploadPath + fileName);
					resultString.append(result.getString(fileName));
					resultString.append("^");
				}
			}
			if(resultString.length() > 0){
				resultString.deleteCharAt(resultString.length() - 1);
			}
			result.set("result_string", resultString.toString());
        } catch(Exception e) {
        	e.printStackTrace();
        }
		return result;
	}
	
	public static String makeFileName(String originFileName, String headStr) {
		logger.debug("#### originFileName : " + originFileName);
		long time = System.currentTimeMillis();
        SimpleDateFormat day = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat dayTime = new SimpleDateFormat("HHmmss");
        String str = day.format(new Date(time));
		String str2 = dayTime.format(new Date(time));
        String exName = StringUtils.substring(originFileName,
				StringUtils.lastIndexOf(originFileName, "."), originFileName.length());
		String fileName = headStr + "_" + StringUtils.substring(originFileName, 0, StringUtils.lastIndexOf(originFileName, ".")) + "_" + str + str2 + "_img" + exName;
		logger.debug("#### fileName : " + fileName);
		return fileName;
	}
	
	public String resizeUploadMultipartFile(MultipartFile file, String s3Path, String defaultUrl, int index) {
		String file_name = index + file.getOriginalFilename();
		String file_url = defaultUrl;
		try {
			if(file != null && !file.isEmpty()) {
				MultipartFile mFile = imgUtil.imgResizeByWidth(file, 350);
				file_url = uploadImageFile(mFile, s3Path, 
						file_name, "");
			}
		}catch (Exception e) {
			// TODO: handle exception
			file_url = defaultUrl;
			e.printStackTrace();
		}
		return file_url;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String fileName = "nrc-20201125_102744-stickered";
		
	}

}
