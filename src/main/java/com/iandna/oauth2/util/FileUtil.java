package com.iandna.oauth2.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class FileUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(FileUtil.class);

	private static boolean s3Upload = true;
//	private static String accessKey = "AKIATGV7MAKDN7CCTBUX";
//	private static String secretKey = "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";
	
	@Value("${s3.access.key}")
	String accessKey;
	@Value("${s3.secret.key}")
	String secretKey;

    public static void writeFile(String filePath, MultipartFile file, String fileNm) {
        OutputStream out = null;
        try {
        	File oldfile = new File(filePath + fileNm);
        	if(oldfile.exists()) {
        		oldfile.delete();
        		logger.debug("이미지[" + oldfile.getAbsolutePath() + "]가 이미 등록되있어 삭제처리합니다.");
        	}
        	
            out = new FileOutputStream(filePath + fileNm);
            BufferedInputStream bis = new BufferedInputStream(file.getInputStream());
            byte[] buffer = new byte[8106];
            int read;
            while ((read = bis.read(buffer)) > 0) {
                out.write(buffer, 0, read);
            }
        } catch (IOException ioe) {
        	logger.error("이미지 업로드 중 오류가 발생하였습니다.", ioe);
        } finally {
            IOUtils.closeQuietly(out);
        }
    }
    
    public void uploadS3File(String uploadPath, String s3UploadBucket, String s3UploadPath, MultipartFile multifile, String fileNm) throws Exception {
    	OutputStream out = null;
    	String filePath = uploadPath + fileNm;
    	
        try {
        	File oldfile = new File(filePath);
        	if(oldfile.exists()) {
        		oldfile.delete();
        		logger.debug("이미지[" + oldfile.getAbsolutePath() + "]가 이미 등록되있어 삭제처리합니다.");
        	}
        	
            out = new FileOutputStream(filePath);
            BufferedInputStream bis = new BufferedInputStream(multifile.getInputStream());
            byte[] buffer = new byte[8106];
            int read;
            while ((read = bis.read(buffer)) > 0) {
                out.write(buffer, 0, read);
            }
        } catch (IOException ioe) {
        	logger.error("이미지 업로드 중 오류가 발생하였습니다.", ioe);
        	throw ioe;
        } finally {
            IOUtils.closeQuietly(out);
        }

		logger.debug("filePath : [" + filePath + "]");
		logger.debug("s3Upload : [" + s3Upload + "]");

		if(s3Upload) {
			File file = new File(filePath);

			S3Handler handler = new S3Handler();
			handler.connect(accessKey, secretKey);
			handler.upload(s3UploadBucket, s3UploadPath + fileNm, file, true);
		}
    }
    
}
