package com.iandna.oauth2.config;

import com.iandna.oauth2.entity.MapData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.iandna.oauth2.config.exception.CustomAuthenticationException;
import com.iandna.oauth2.entity.Customer;
import com.iandna.oauth2.repo.CustomerJpaRepo;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    private final PasswordEncoder passwordEncoder;
    private final CustomerJpaRepo customerJpaRepo;

    private static final Logger logger = LoggerFactory.getLogger(CustomAuthenticationProvider.class);
    @Override
    public Authentication authenticate(Authentication authentication) {

        String name = authentication.getName();
        String password = authentication.getCredentials().toString();
        Customer customer = customerJpaRepo.findByUid("C" + name).orElse(null);
        if(customer == null) {
        	throw new CustomAuthenticationException("997");
        }
        if(customer.getCustomerType().equals("1")) {
            logger.debug("#### old customer: " + customer.getUid());
            throw new CustomAuthenticationException("998");
        }
        if (!passwordEncoder.matches(password, customer.getPassword())) {
            throw new BadCredentialsException("999");
        }
        return new UsernamePasswordAuthenticationToken(customer, password, customer.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
//        return authentication.equals(
//                UsernamePasswordAuthenticationToken.class);
//                CustomAuthenticationToken.class);
//        return CustomAuthenticationToken.class.isAssignableFrom(authentication);
        return true;
    }
}
