package com.iandna.oauth2.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.iandna.oauth2.entity.Customer;

public interface CustomerJpaRepo extends JpaRepository<Customer, String> {
	@Query("SELECT c FROM Customer c WHERE uid = :uid AND stCd IN ('1', '2')")
    Optional<Customer> findByUid(@Param("uid") String uid);
    Optional<Customer> findByCsId(String csId);
    int countByShopId(String shopId);
    @Query("SELECT c FROM Customer c WHERE regDate >= '20220512' AND certDi = :di AND stCd = 1 ORDER BY regDate DESC, regTime DESC")
    Optional<Customer> findFirstByCertDi(@Param("di") String di);
//    @Query("SELECT c FROM Customer c WHERE regDate >= '20220512' AND certDi = :di AND stCd = :stCd ORDER BY regDate DESC, regTime DESC")
    Optional<Customer> findFirstByCertDiAndStCdOrderByRegDateDescRegTimeDesc(@Param("di") String di, @Param("stCd") String stCd);
    @Query("SELECT c FROM Customer c WHERE phoneNo = :phoneNo AND stCd = 1 ORDER BY regDate DESC, regTime DESC")
    Optional<Customer> findFirstByPhoneNo(@Param("phoneNo") String phoneNo);
    Optional<Customer> findFirstByPhoneNoAndStCdOrderByRegDateDescRegTimeDesc(@Param("di") String di, @Param("stCd") String stCd);
    Customer save(Customer customer);
    List<Customer> findAllByNickname(String nickname);
}
