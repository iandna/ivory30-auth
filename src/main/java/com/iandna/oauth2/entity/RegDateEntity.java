package com.iandna.oauth2.entity;

import lombok.Getter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Getter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class RegDateEntity {

    @Column(name="REG_DT", columnDefinition = "char(8)", updatable=false)
    private String regDate;     //등록일자

    @Column(name="REG_TM", columnDefinition = "char(6)", updatable=false)
    private String regTime;     //등록시간

    @PrePersist
    public void onPrePersist(){
        this.regDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        this.regTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));
    }

//    @PreUpdate
//    public void onPreUpdate(){
//
//    }
}
