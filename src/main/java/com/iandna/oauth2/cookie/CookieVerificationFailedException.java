package com.iandna.oauth2.cookie;

public class CookieVerificationFailedException extends RuntimeException {
    public CookieVerificationFailedException(String s) {
        super(s);
    }
}
