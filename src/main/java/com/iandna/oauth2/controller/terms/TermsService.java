package com.iandna.oauth2.controller.terms;

import com.iandna.oauth2.cookie.CookieSecurityContextRepository;
import com.iandna.oauth2.entity.Terms;
import com.iandna.oauth2.repo.TermsJpaRepo;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class TermsService {
    private static final Logger logger = LoggerFactory.getLogger(TermsService.class);

    @Autowired
    private TermsJpaRepo terms_jpa_repo;

    @Autowired
    private CookieSecurityContextRepository cookieSecurityContextRepository;

    public List<Terms> findTerms() {
        List<Terms> result = terms_jpa_repo.selectTerms();
        return result;
    }
}
