package com.iandna.oauth2.cookie;

import com.iandna.oauth2.entity.Customer;
import com.iandna.oauth2.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.Cookie;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class SignedUserInfoCookie extends Cookie {
    private final Logger LOG = LoggerFactory.getLogger(SignedUserInfoCookie.class);

    public static final String NAME = "UserInfo";
    private static final String PATH = "/";
//    private static final Pattern UID_PATTERN = Pattern.compile("uid=([A-Za-z0-9@.]*)");
    private static final Pattern UID_PATTERN = Pattern.compile("uid=([A-Za-z0-9@-_.-]*)");
    private static final Pattern ROLES_PATTERN = Pattern.compile("roles=([A-Z0-9_|]*)");
    private static final Pattern PROVIDER_PATTERN = Pattern.compile("colour=([A-Z]*)");
    private static final Pattern HMAC_PATTERN = Pattern.compile("hmac=([A-Za-z0-9+/=]*)");
    private static final String HMAC_SHA_512 = "HmacSHA512";
    private static final int TEN_YEARS = 60 * 60 * 24 * 365 * 10;

    private final Payload payload;
    private final String hmac;

    public SignedUserInfoCookie(Customer userInfo, String cookieHmacKey) {
        super(NAME, "");
        LOG.debug("######### SignedUserInfoCookie Customer userInfo : ");
        LOG.debug(userInfo.toString());
        this.payload = new Payload(
                userInfo.getCsId()
//                userInfo.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()),
//                userInfo.getProvider().orElse(null)
        );
        this.hmac = calculateHmac(this.payload, cookieHmacKey);
        this.setPath(PATH);
//        this.setMaxAge((int) Duration.of(1, ChronoUnit.HOURS).toSeconds());
        this.setMaxAge(TEN_YEARS);
        this.setHttpOnly(true);
    }

    public SignedUserInfoCookie(Cookie cookie, String cookieHmacKey) {
        super(NAME, "");

        if (!NAME.equals(cookie.getName()))
            throw new IllegalArgumentException("No " + NAME + " Cookie");

        this.hmac = parse(cookie.getValue(), HMAC_PATTERN).orElse(null);
        if (hmac == null)
            throw new CookieVerificationFailedException("Cookie not signed (no HMAC)");

        String username = parse(cookie.getValue(), UID_PATTERN).orElseThrow(() -> new IllegalArgumentException(NAME + " Cookie contains no UID"));
        List<String> roles = parse(cookie.getValue(), ROLES_PATTERN).map(s -> Arrays.asList(s.split("\\|"))).orElse(Arrays.asList());
        String provider = parse(cookie.getValue(), PROVIDER_PATTERN).orElse(null);
//        this.payload = new Payload(username, roles, provider);
        this.payload = new Payload(username);

        LOG.info("input signed cookie payload ==> {}" , payload);

        if (!hmac.equals(calculateHmac(payload, cookieHmacKey)))
            throw new CookieVerificationFailedException("Cookie signature (HMAC) invalid");

        this.setPath(cookie.getPath());
        this.setMaxAge(cookie.getMaxAge());
        this.setHttpOnly(cookie.isHttpOnly());
    }

    private static Optional<String> parse(String value, Pattern pattern) {
        Matcher matcher = pattern.matcher(value);
        if (!matcher.find())
            return Optional.empty();

        if (matcher.groupCount() < 1)
            return Optional.empty();

        String match = matcher.group(1);
        if (match == null || match.trim().isEmpty())
            return Optional.empty();

        return Optional.of(match);
    }

    @Override
    public String getValue() {
        return payload.toString() + "&hmac=" + hmac;
    }

    public Customer getUserInfo() {
        return new Customer(
                payload.username
//                ,
//                payload.roles.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toSet()));
//                payload.roles.stream().map(GrantedAuthority::getAuthority).collect(toList())
                );
    }

    private String calculateHmac(Payload payload, String secretKey) {
        byte[] secretKeyBytes = Objects.requireNonNull(secretKey).getBytes(StandardCharsets.UTF_8);
        byte[] valueBytes = Objects.requireNonNull(payload).toString().getBytes(StandardCharsets.UTF_8);

        try {
            Mac mac = Mac.getInstance(HMAC_SHA_512);
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKeyBytes, HMAC_SHA_512);
            mac.init(secretKeySpec);
            byte[] hmacBytes = mac.doFinal(valueBytes);
            return Base64.getEncoder().encodeToString(hmacBytes);

        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException(e);
        }
    }

    private static class Payload {
        private final String username;
//        private final List<String> roles;
//        private final String provider;
//        private Payload(String username, List<String> roles, String provider) {
        private Payload(String username) {
            this.username = username;
//            this.roles = roles;
//            this.provider = provider;
        }

        @Override
        public String toString() {
            return "uid=" + username;
//                    +
//                    "&roles=" + String.join("|", roles) +
//                    (provider != null ? "&provider=" + provider : "");
        }
    }
}
