package com.iandna.oauth2.entity;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.Embeddable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Embeddable
public class Agree {

    private String agree;   //동의여부(Y/N)
    private String modDate; //수정일자
    private String modTime; //수정시각

    public Agree() {
        this.agree = "N";
        this.modDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        this.modTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));
    }

    public Agree(String agree) {
        this.agree = agree;
        this.modDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        this.modTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));
    }

}
