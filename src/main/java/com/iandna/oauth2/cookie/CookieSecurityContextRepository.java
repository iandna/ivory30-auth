package com.iandna.oauth2.cookie;

import java.util.Optional;
import java.util.stream.Stream;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpRequestResponseHolder;
import org.springframework.security.web.context.SaveContextOnUpdateOrErrorResponseWrapper;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.stereotype.Component;

import com.iandna.oauth2.entity.Customer;

import lombok.Getter;

@Getter
@Component
public class CookieSecurityContextRepository implements SecurityContextRepository {

    private static final Logger LOG = LoggerFactory.getLogger(CookieSecurityContextRepository.class);
    private static final String EMPTY_CREDENTIALS = "";
    private static final String ANONYMOUS_USER = "anonymousUser";

    private final String cookieHmacKey;

    public CookieSecurityContextRepository(@Value("${auth.cookie.hmac-key}") String cookieHmacKey) {
        this.cookieHmacKey = cookieHmacKey;
    }

    @Override
    public SecurityContext loadContext(HttpRequestResponseHolder requestResponseHolder) {
        HttpServletRequest request = requestResponseHolder.getRequest();
        HttpServletResponse response = requestResponseHolder.getResponse();
        requestResponseHolder.setResponse(new SaveToCookieResponseWrapper(request, response));

        SecurityContext context = SecurityContextHolder.createEmptyContext();
        
        // 회원가입 완료일 경우엔 유저인포를 쿠키에서 가져오지 않는다
        if(!request.getRequestURI().contains("joinComplete.do")) {
        	Optional<Customer> customer = readUserInfoFromCookie(request);
        	LOG.debug("xxxxxxxxxreadUserInfoFromCookie" + customer.toString());
        	customer.ifPresent(userInfo ->
        	context.setAuthentication(new UsernamePasswordAuthenticationToken(userInfo, EMPTY_CREDENTIALS, userInfo.getAuthorities())));
        }

        return context;
    }

    @Override
    public void saveContext(SecurityContext context, HttpServletRequest request, HttpServletResponse response) {
        SaveToCookieResponseWrapper responseWrapper = (SaveToCookieResponseWrapper) response;
        if (!responseWrapper.isContextSaved()) {
            responseWrapper.saveContext(context);	
        }
    }

    @Override
    public boolean containsContext(HttpServletRequest request) {
        return readUserInfoFromCookie(request).isPresent();
    }

    private Optional<Customer> readUserInfoFromCookie(HttpServletRequest request) {
        return readCookieFromRequest(request)
                .map(this::createUserInfo);
    }

    private Optional<Cookie> readCookieFromRequest(HttpServletRequest request) {
    	Cookie[] cookies = request.getCookies();
    	if (cookies == null) {
            return Optional.empty();
        }
    	for(Cookie ck : cookies) {
    		if(ck.getName().equals("UserToken") && ck.getValue().equals("logout")) {
    			return Optional.empty();
    		}
    	}

        Optional<Cookie> maybeCookie = Stream.of(request.getCookies())
                .filter(c -> SignedUserInfoCookie.NAME.equals(c.getName()))
                .findFirst();

        return maybeCookie;
    }

    private Customer createUserInfo(Cookie cookie) {
    	LOG.info("Create UserInfoCookie Start...");
    	Customer customer = null;
    	try {
    		customer = new SignedUserInfoCookie(cookie, cookieHmacKey).getUserInfo();
    	}catch (CookieVerificationFailedException e) {
			// TODO: handle exception
    		e.printStackTrace();
    		LOG.error("HMAC invalid");
		}
        return customer;
    }

    private class SaveToCookieResponseWrapper extends SaveContextOnUpdateOrErrorResponseWrapper {
        private final Logger LOG = LoggerFactory.getLogger(SaveToCookieResponseWrapper.class);
        private final HttpServletRequest request;

        SaveToCookieResponseWrapper(HttpServletRequest request, HttpServletResponse response) {
            super(response, true);
            this.request = request;
        }

        @Override
        protected void saveContext(SecurityContext securityContext) {
            HttpServletResponse response = (HttpServletResponse) getResponse();
            Authentication authentication = securityContext.getAuthentication();
            if (authentication == null) {
                LOG.debug("No securityContext.authentication, skip saveContext");
                return;
            }

            if (ANONYMOUS_USER.equals(authentication.getPrincipal())) {
                LOG.debug("Anonymous User SecurityContext, skip saveContext");
                return;
            }

//            if (!(authentication.getPrincipal() instanceof User)) {
//                LOG.warn("securityContext.authentication.principal of unexpected type {}, skip saveContext", authentication.getPrincipal().getClass().getCanonicalName());
//                return;
//            }

            Customer userInfo = (Customer) authentication.getPrincipal();
            LOG.info("UserInfo : {}", userInfo.toString());
            SignedUserInfoCookie cookie = new SignedUserInfoCookie(userInfo, cookieHmacKey);
            cookie.setSecure(request.isSecure());
            response.addCookie(cookie);
            LOG.info("SecurityContext for principal '{}' saved in Cookie", userInfo.getCsId());
        }
    }
}