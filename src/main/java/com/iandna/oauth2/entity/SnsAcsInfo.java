package com.iandna.oauth2.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Entity
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "IVORY_SNS_ACS_INFO")
public class SnsAcsInfo extends RegDateEntity {
    @Id
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(unique = true, name="CUST_NO", length = 50)
    private String uid;     // unique_id (email 주소)

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="CONN_INSTAGRAM", length = 1)
    private char connInstagram;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="CONN_FACEBOOK", length = 1)
    private char connFacebook;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="CONN_NAVER", length = 1)
    private char connNaver;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="CONN_YOUTUBE", length = 1)
    private char connYoutube;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="INSTA_USER_ID", length = 35)
    private String instaUserID;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="INSTA_NAME", length = 30)
    private String instaName;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="INSTA_POST_CNT", length = 20)
    private String instaPostCnt;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="INSTA_FOLLOWER", length = 20)
    private String instaFollower;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="INSTA_FOLLOWING", length = 20)
    private String instaFollowing;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="INSTA_TYPE", length = 20)
    private String instaType;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="INSTA_ACS_TOKEN", length = 256)
    private String instaAcsToken;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="INSTA_ACS_EXP_TM", length = 8)
    private String instaAcsExpTm;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="INSTA_ACC_TYPE", length = 1)
    private String instaAccType;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="INSTA_CHK", length = 100)
    private String instaChk;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="FB_USER_ID", length = 35)
    private String fbUserId;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="FB_ACS_TOKEN", length = 256)
    private String fbAcsToken;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="FB_ACS_EXP_TM", length = 8)
    private String fbAcsExpTm;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="NAVER_BLOG", length = 500)
    private String naverBlog;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="NAVER_TOTAL_VST", length = 20)
    private String naverTotalVst;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="NAVER_TODAY_VST", length = 20)
    private String naverTodayVst;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="NAVER_HOOD_CNT", length = 20)
    private String naverHoodCnt;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="NAVER_ACC_TYPE", length = 1)
    private String naverAccType;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="NAVER_CHK", length = 100)
    private String naverChk;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="YOUTUBE_CH_ID", length = 100)
    private String youtubeChId;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="SNS_RANK", length = 1)
    private String snsRank;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="MOD_DT", length = 8)
    private String modDt;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="MOD_TM", length = 6)
    private String modTm;
    
}
