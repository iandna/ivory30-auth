package com.iandna.oauth2.controller.common;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.iandna.oauth2.entity.MapData;
import com.iandna.oauth2.entity.ResponseModel;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.WebUtils;

import com.google.gson.Gson;
import com.iandna.oauth2.cookie.TokenCookie;
import com.iandna.oauth2.model.oauth2.OAuthToken;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Controller
@RequestMapping("/oauth")
public class Oauth2Controller {
	private static final Logger logger = LoggerFactory.getLogger(Oauth2Controller.class);
    private final Gson gson;
    private final RestTemplate restTemplate;
//    @Value("${auth.cookie.hmac-key}")
    private final String cookieHmacKey ="39df3fdd625fc6ee9dfb6a412cb7fc05b9b6cd1b2cb8d97e727a0a7c812fa768";

    @GetMapping(value = "/callback")
    public String callbackSocial(@RequestParam Map<String, Object> reqParam , HttpServletResponse res, RedirectAttributes redirect, HttpServletRequest req) {
    	String authUrl = req.getServerName();
        String returnUrl = null;
        String credentials = null;
        
        
        logger.info("################## authUrl");
        logger.info(authUrl);
        
        // state값 복호화 및 cookie에서 frcsCustNo 추출하기
        String state = req.getParameter("state");
        String userInfo = WebUtils.getCookie(req, "UserInfo").getValue();
        String redirect_uri = req.getParameter("redirect_uri");
        if(userInfo != null) {
        	logger.info(userInfo);
        	userInfo = StringUtils.replace(StringUtils.split(userInfo, "&")[0], "uid=", "");
        	logger.info(userInfo);
        }
        if(state != null && !state.isEmpty() ) {
        	/*
        	Pattern pattern = Pattern.compile("^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?$");
            Matcher matcher = pattern.matcher(state);
            if (matcher.find()) {
            	state = new String(Base64.decodeBase64(state));	
            }
            */
        	
        	if (Base64.isBase64(state)) {
        		if( (redirect_uri == null || !redirect_uri.contains("https://m.i-vory.shop/")) && !state.equals(state.toLowerCase())  ) {
        			state = new String(Base64.decodeBase64(state));
            	}else {
            		state = "state=" + state;
            	}
            }else {
            	state = "state=" + state;
            }
        	
        }else {
        	state = "url=index.do";
        }
        
        // test환경과 실제 환경 분기
    	if(authUrl.equals("localhost")) {
        	authUrl = "http://localhost:5004";
        	returnUrl = "http://localhost:10000/login.do?" + state + "&frcsCustNo=" + userInfo;
        	credentials = "testClientId:testSecret";
        }else if(authUrl.contains("iandna-ivory.com")) {
        	authUrl = "https://iandna-ivory.com";
        	returnUrl = "https://iandna-ivory.com/login.do?" + state + "&frcsCustNo=" + userInfo;
        	credentials = "testClientId:testSecret";
        }else 
        /*if(authUrl.contains("i-vory.net")) */
        {
        	authUrl = "https://i-vory.net";
        	returnUrl = "https://i-vory.net/login.do?" + state + "&frcsCustNo=" + userInfo;
        	credentials = "iandna77:iandnaMain77**99";
        }
        if(redirect_uri != null && redirect_uri.contains("https://m.i-vory.shop/")) {
        	returnUrl = redirect_uri;
        }
        
        String encodedCredentials = new String(Base64.encodeBase64(credentials.getBytes()));
        logger.info("#### returnUrl : " + returnUrl);
        logger.info("#### authUrl : " + authUrl);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Authorization", "Basic " + encodedCredentials);

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("code", ( reqParam.get("code") == null) ? "" : reqParam.get("code").toString());
        params.add("grant_type", "authorization_code");
        params.add("redirect_uri", authUrl+"/oauth/callback");
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(params, headers);
        ResponseEntity<String> tokenResponse = restTemplate.postForEntity(authUrl+"/oauth/token", request, String.class);
    
    	if (tokenResponse.getStatusCode() == HttpStatus.OK) {
            OAuthToken jwtToken = gson.fromJson(tokenResponse.getBody(), OAuthToken.class);
            TokenCookie cookie = new TokenCookie(jwtToken, cookieHmacKey);
            res.addCookie(cookie);
            
        	return "redirect:" + returnUrl;	
        }
		// TODO: handle exception
    	
        
        return "/error";
    }

    @GetMapping(value = "/token-refresh")
    public OAuthToken refreshToken(@RequestParam String refreshToken) {

        String credentials = "testClientId:testSecret";
        String encodedCredentials = new String(Base64.encodeBase64(credentials.getBytes()));

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Authorization", "Basic " + encodedCredentials);

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("refresh_token", refreshToken);
        params.add("grant_type", "refresh_token");
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(params, headers);
        ResponseEntity<String> response = restTemplate.postForEntity("http://localhost:5004/oauth/token", request, String.class);
        if (response.getStatusCode() == HttpStatus.OK) {
            return gson.fromJson(response.getBody(), OAuthToken.class);
        }
        return null;
    }

    @RequestMapping("authorization-code")
    @ResponseBody
    public String authorizationCodeTest(@RequestParam("code") String code) {
        String curl = String.format("curl " +
                "-F \"grant_type=authorization_code\" " +
                "-F \"code=%s\" " +
                "-F \"scope=read\" " +
                "-F \"client_id=foo\" " +
                "-F \"client_secret=bar\" " +
                "-F \"redirect_uri=http://localhost:5004/oauth/authorization-code\" " +
                "\"http://localhost:5004/oauth/token\"", code);
        return curl;
    }

    @RequestMapping("/health/was")
    @ResponseBody
    public ResponseModel healthCheckWas() {
        ResponseModel responseModel = new ResponseModel();
        MapData result = new MapData();
        responseModel.setResult(result);
        return responseModel;
    }

}
