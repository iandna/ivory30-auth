package com.iandna.oauth2.controller.customer;

import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iandna.oauth2.entity.AgreeInfo;
import com.iandna.oauth2.entity.Customer;
import com.iandna.oauth2.entity.ResponseModel;

@Controller
@RequestMapping("/oauth/customer")
public class CustomerController {
	private static final Logger logger = LoggerFactory.getLogger(CustomerController.class);
	
	@Autowired
	CustomerService customerService;

	@RequestMapping(value = "/join.do")
	public String join(@RequestParam Map<String, Object> reqParam, ModelMap model) {
		String key;
		Iterator iterator = reqParam.keySet().iterator();
		while(iterator.hasNext()) {
			key = iterator.next().toString();
			logger.info(key + " : " + reqParam.get(key).toString());
		}
		
		model.addAttribute("data", this.customerService.getTerms());
		model.addAttribute("iParam", reqParam);
		return "customer/join";
	}
	
	@RequestMapping(value = "/profile.do")
	public String profile(@RequestParam Map<String, Object> reqParam, ModelMap model) {
		logger.info("####### profile.do #######");
		model.addAttribute("iParam", reqParam);
		return "customer/profile";
	}
	
	@RequestMapping(value = "/authReq.do")
	public String joinReq(@RequestParam Map<String, Object> reqParam, ModelMap model, HttpServletRequest request) throws Exception
	{
		String key;
		logger.debug("#### authReq.do");
		Iterator iterator = reqParam.keySet().iterator();
		while(iterator.hasNext()) {
			key = iterator.next().toString();
		}
		if(reqParam.get("up_hash")!=null) {
			customerService.insertUpHash(reqParam);
		}
		model.addAttribute(reqParam);
		return "customer/auth_req";
	}
	
	@RequestMapping(value = "/authRes.do")
	public String joinRes(@RequestParam Map<String, Object> reqParam, ModelMap model, HttpServletRequest request) throws Exception
	{
		Map<String, String> result;
		String key;
		logger.debug("#### authRes.do");
		Iterator iterator = reqParam.keySet().iterator();
		while(iterator.hasNext()) {
			key = iterator.next().toString();
			logger.info(key + " : " + reqParam.get(key).toString());
		}
		result = this.customerService.decryptKcpRes(request, reqParam);
		model.addAttribute("result", result);
		return "customer/auth_res";
	}
	
	@RequestMapping(value = "/joinComplete.do")
	public String joinComplete(@RequestParam Map<String, Object> reqParam, Customer customer, AgreeInfo agree, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		String key;
		logger.debug("#### authRes.do");
		Iterator iterator = reqParam.keySet().iterator();
		while(iterator.hasNext()) {
			key = iterator.next().toString();
			logger.info(key + " : " + reqParam.get(key).toString());
		}
		Customer result;
		if(request.getMethod().equals("GET")) {
			logger.info("method:GET");
		}else {
			result = this.customerService.insertUserInfo(customer, agree, request, response);
			model.addAttribute("customer", customer);
		}
		
		return "customer/join_complete";
	}
	
	@PostMapping(value = "/findId.do")
    public String findIdDo(@RequestParam Map<String, Object> reqParam, ModelMap model) {
		logger.debug("####### findId.do #######");
		String key;
		Iterator iterator = reqParam.keySet().iterator();
		while(iterator.hasNext()) {
			key = iterator.next().toString();
			logger.info(key + " : " + reqParam.get(key).toString());
		}
		model.addAttribute("iParam", reqParam);
        return "customer/id_find";
    }
	
	@GetMapping(value = "/findPassword.do")
    public String findPassword() {
		logger.debug("####### findPassword.do #######");
        return "customer/password_find";
    }
	
	@PostMapping(value = "/findId.json")
	@ResponseBody
	public Customer findIdJson(@RequestBody Customer customer, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		Customer result;
		result = this.customerService.findUserInfoByPhoneNo(customer);
		return result;
	}
	
	@PostMapping(value = "/findId.json", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
	@ResponseBody
	public Customer findIdForm(Customer customer, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		Customer result;
		result = this.customerService.findUserInfoByPhoneNo(customer);
		return result;
	}
	
	@PostMapping(value = "/getOne.json")
	@ResponseBody
	public Customer getOneJson(@RequestBody Customer customer, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		Customer result;
		result = this.customerService.findUserInfoById(customer);
		return result;
	}
	
	@PostMapping(value = "/getOne.json", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
	@ResponseBody
	public Customer getOneForm(Customer customer, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		Customer result;
		result = this.customerService.findUserInfoById(customer);
		return result;
	}
	
	@PostMapping(value = "/resetPassword.json")
	@ResponseBody
    public ResponseModel resetPasswordJson(@RequestParam Map<String, Object> reqParam, HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseModel responseModel = customerService.resetPassword(reqParam);
        return responseModel;
    }

	@GetMapping(value = "/resetComplete.do")
    public String resetPasswordView(@RequestParam Map<String, Object> reqParam, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        return "customer/password_reset_complete";
    }
	
	@PostMapping(value = "/idDuplication.json", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
	@ResponseBody
	public Map<String, String> idDuplication(Customer customer, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		Map<String, String> result;
		result = this.customerService.idDuplicationCheck(customer);
		return result;
	}
	
	@PostMapping(value = "/nicknameDuplication.json", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
	@ResponseBody
	public Map<String, String> nicknameDuplication(Customer customer, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		Map<String, String> result;
		result = this.customerService.nicknameDuplicationCheck(customer);
		return result;
	}
	
	@GetMapping(value = "/getTerms.json", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
	@ResponseBody
	public Map<String, Object> getTerms() throws Exception
	{
		Map<String, Object> result;
		result = this.customerService.getTerms();
		return result;
	}
	
	@PostMapping(value = "/inquiry.do")
    public String inquiry(@RequestParam Map<String, Object> reqParam, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		model.addAttribute("iParam", reqParam);
		return "customer/inquiry";
    }
	

	@GetMapping(value = "/welcome.do")
    public String welcome(@RequestParam Map<String, Object> reqParam, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        return "customer/welcome";
    }
	
	@GetMapping(value = "/login.do")
    public String login(@RequestParam Map<String, Object> reqParam, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        return "customer/login";
    }
	

	@GetMapping(value = "/quertTest.do")
	@ResponseBody
    public Map<String, Object> quertTest() throws Exception {
		Map<String, Object> result;
		result = this.customerService.test();
		return result;
    }


	@RequestMapping(value = "/dormant.do")
	public String dormant(@RequestParam Map<String, Object> reqParam, Customer customer, AgreeInfo agree, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		return "customer/dormant";
	}
}