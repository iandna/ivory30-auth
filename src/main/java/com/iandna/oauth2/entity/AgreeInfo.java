package com.iandna.oauth2.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Builder
@Entity
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "SNB_CUST_AGREE_INFO")
public class AgreeInfo extends RegDateEntity {
	
    @Id
    @Column(unique = true, name="CUST_NO", length = 50)
    private String uid;     // unique_id (email 주소)

    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE1", length = 1)
    private String agree1;
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE1_MOD_DT", length = 8)
    private String agree1ModDt = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE1_MOD_TM", length = 6)
    private String agree1ModTm = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));
    
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE2", length = 1)
    private String agree2;
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE2_MOD_DT", length = 8)
    private String agree2ModDt = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE2_MOD_TM", length = 6)
    private String agree2ModTm = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));
    
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE3", length = 1)
    private String agree3;
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE3_MOD_DT", length = 8)
    private String agree3ModDt = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE3_MOD_TM", length = 6)
    private String agree3ModTm = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));
    
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE4", length = 1)
    private String agree4;
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE4_MOD_DT", length = 8)
    private String agree4ModDt = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE4_MOD_TM", length = 6)
    private String agree4ModTm = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));
    
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE5", length = 1)
    private String agree5;
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE5_MOD_DT", length = 8)
    private String agree5ModDt = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE5_MOD_TM", length = 6)
    private String agree5ModTm = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));
    
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE6", length = 1)
    private String agree6;
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE6_MOD_DT", length = 8)
    private String agree6ModDt = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE6_MOD_TM", length = 6)
    private String agree6ModTm = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));
    
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE7", length = 1)
    private String agree7;
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE7_MOD_DT", length = 8)
    private String agree7ModDt = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE7_MOD_TM", length = 6)
    private String agree7ModTm = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));
    
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE8", length = 1)
    private String agree8;
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE8_MOD_DT", length = 8)
    private String agree8ModDt = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE8_MOD_TM", length = 6)
    private String agree8ModTm = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));
    
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE9", length = 1)
    private String agree9;
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE9_MOD_DT", length = 8)
    private String agree9ModDt = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE9_MOD_TM", length = 6)
    private String agree9ModTm = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));
    
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE10", length = 1)
    private String agree10;
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE10_MOD_DT", length = 8)
    private String agreeModDt = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE10_MOD_TM", length = 6)
    private String agree10ModTm = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));
    
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE11", length = 1)
    private String agree11;
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE11_MOD_DT", length = 8)
    private String agree11ModDt = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE11_MOD_TM", length = 6)
    private String agree11ModTm = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));
    
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE12", length = 1)
    private String agree12;
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE12_MOD_DT", length = 8)
    private String agree12ModDt = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE12_MOD_TM", length = 6)
    private String agree12ModTm = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));
    
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE13", length = 1)
    private String agree13;
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE13_MOD_DT", length = 8)
    private String agree13ModDt = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Column(name="AGREE13_MOD_TM", length = 6)
    private String agree13ModTm = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));
    
}
