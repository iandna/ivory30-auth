<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.util.Enumeration" %>
<%@ page import="kr.co.kcp.CT_CLI"%>
<%@ page import="java.net.URLDecoder"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ include file="./auth_conf.jsp"%>
<%
    /* ============================================================================== */
    /* =   인증데이터 수신 및 복호화 페이지                                         = */
    /* = -------------------------------------------------------------------------- = */
    /* =   해당 페이지는 반드시 가맹점 서버에 업로드 되어야 하며                    = */ 
    /* =   가급적 수정없이 사용하시기 바랍니다.                                     = */
    /* ============================================================================== */
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
        <title>*** NHN KCP Online Payment System [Jsp Version] ***</title>
        <script type='text/javascript' src='/oauth/resources/js/common.js?${nowDateCache}'></script>
        <script type="text/javascript">
            /* window.onload=function()
            {
                try
                {
                    opener.auth_data( document.form_auth ); // 부모창으로 값 전달
                    window.close();
                }
                catch(e)
                {
                	alert("error.");
                    alert(e); // 정상적인 부모창의 iframe 를 못찾은 경우임
                }
            } */
            
            window.onload=function()
            {
                try
                {
                    parent.auth_data( document.form_auth ); // 부모창으로 값 전달
                }
                catch(e)
                {
                    alert(e); // 정상적인 부모창의 iframe 를 못찾은 경우임
                }
            }
        </script>
    </head>
    <body oncontextmenu="return false;" ondragstart="return false;" onselectstart="return false;">
        <form name="form_auth" method="post">
	        <input type="hidden" name="comm_id" value="${ result.comm_id }" />
        	<input type="hidden" name="phone_no" value="${ result.phone_no }" />
        	<input type="hidden" name="user_name" value="${ result.user_name }" />
        	<input type="hidden" name="birth_day" value="${ result.birth_day }" />
        	<input type="hidden" name="sex_code" value="${ result.sex_code }" />
        	<input type="hidden" name="local_code" value="${ result.local_code }" />
        	<input type="hidden" name="ci" value="${ result.ci }" />
        	<input type="hidden" name="ci_url" value="${ result.ci_url }" />
        	<input type="hidden" name="di" value="${ result.di }" />
        	<input type="hidden" name="di_url" value="${ result.di_url }" />
        	<input type="hidden" name="res_cd" value="${ result.res_cd }" />
        	<input type="hidden" name="res_msg" value="${ result.res_msg }" />
        	<input type="hidden" name="up_hash" value="${ result.up_hash }" />
        	<input type="hidden" name="dup" value="${ result.dup}" />
        	<input type="hidden" name="csId" value="${ result.csId}" />
        </form>
    </body>
</html>
