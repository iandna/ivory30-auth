package com.iandna.oauth2.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

@Builder
@Entity
@Getter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "IVORY_TERMS_INFO")
@Setter
@ToString
public class Terms extends RegDateEntity {
    @Id
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Column(name = "SEQ_NO")
    private int seqNo;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Column(name = "GBN_CD", columnDefinition = "CHAR")
    private String gbnCd;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Column(name = "ST_CD", columnDefinition = "CHAR")
    private String stCd;
    
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Column(name = "SUBJECT", length = 500)
    private String subject;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Column(name = "CONTENT", columnDefinition = "LONGTEXT")
    private String content;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Column(name = "REG_ID", length = 50)
    private String regId;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Column(name = "VERSION")
    private float version;

}
