// 알럿
function callAlert(value) {
    var body = $("body");
    var alert = "";
    alert += '<div class="alert"><div class="alert_container">';
    alert += '<div class="title">';
    alert += value.title;
    alert += '</div><div class="content">';
    alert += value.content;
    alert += '</div><div class="btn_group"><button type="button" class="alert_conform">';
    alert += value.btn;
    alert += '</button></div></div><div class="back"></div></div>';
    body.addClass("alert_on");
    body.find(".container").append(alert);

    // 알럿 컨펌 버튼
    $(document).on("click", ".alert_conform, .alert .back", function () {
        var body = $("body");
        $(".alert").remove();
        body.removeClass("alert_on");
    });
}

// 알럿, 콜백함수
function callBackAlert(value, callback = function () {}) {
    var body = $("body");
    var alert = "";
    alert += '<div class="alert"><div class="alert_container">';
    alert += value.title == "" || value.title == undefined ? "" : '<div class="title">';
    alert += value.title == "" || value.title == undefined ? "" : value.title;
    alert += value.title == "" || value.title == undefined ? "" : "</div>";
    alert += '<div class="content">';
    alert += value.content;
    alert += '</div><div class="btn_group"><button type="button" class="alert_conform">';
    alert += value.btn;
    alert += '</button></div></div><div class="back"></div></div>';
    body.addClass("alert_on");
    body.append(alert);

    // 알럿 컨펌 버튼
    $(body).on("click", ".alert_conform, .alert .back", function () {
        var body = $("body");
        $(".alert").remove();
        body.removeClass("alert_on");
        callback();
    });
}

// 팝업
function callPopup(value) {
    // 220728 ajy
    // 에디터에서 작성된 들여쓰기 스타일 문제 수정
    var text = value;
    if(text != null || text != undefined)
    	text = text.text;
    else
    	return;
    // var text = value.text.replace(/margin-left:18.0pt/gi, "margin-left: 2.2em").replace(/text-indent:-18.0pt/gi, "text-indent: -2.2em");
    // text = text.replace(/margin-left:20.0pt/gi, "margin-left: 2.5em").replace(/text-indent:-20.0pt/gi, "text-indent: -2.5em");
    // text = text.replace(/margin: 0cm 0cm 0cm 38pt/gi, "margin-left: 3.2em").replace(/text-indent: -18pt/gi, "text-indent: -2.6em");
    // text = text.replace(/margin: 0cm 0cm 0cm 40pt/gi, "margin-left: 3.2em").replace(/text-indent: -20pt/gi, "text-indent: -2.6em");
    // text = text.replace(/margin: 0cm 0cm 0cm 12pt/gi, "margin-left: 1.4em").replace(/text-indent: -12pt/gi, "text-indent: -1.4em");
    // text = text.replace(/margin: 0cm 0cm 0cm 48pt/gi, "margin-left: 4.5em").replace(/text-indent: -12pt/gi, "text-indent: -1.35em");

    var body = $("body");
    var popup = "";
    popup += '<div class="popup"><div class="popup_container"><button type="button" class="popup_close"></button><div class="popup_content">';
    popup += '<h3 class="title">';
    popup += value.title;
    popup += '</h3><pre class="text">';
    popup += text;
    popup += "</pre></div>";
    popup += '<button type="button" data-target="';
    popup += value.target;
    popup += '" class="popup_footer_btn" onclick="agreeAllTerms();">모든 약관에 동의합니다</button></div><div class="back"></div></div>';
    body.find(".container").append(popup);
    body.addClass("popup_on");
}

// 팝업 닫기
function closePopup() {
    var body = $("body");
    var popup = $(".popup");
    popup.find(".popup_container").addClass("closing");

    setTimeout(function () {
        popup.remove();
        body.removeClass("popup_on");
    }, 300);
}

// 팝업 닫기 버튼 클릭
$(document).on("click", ".popup_close, .popup .back", function () {
    closePopup();
});

// 팝업 동의
$(document).on("click", ".popup_footer_btn", function () {
    var target = $(this).attr("data-target");
    $("#" + target).prop("checked", true);
    closePopup();

    // 인증버튼 활성화체크
    checkBtnAuthentication();
});
