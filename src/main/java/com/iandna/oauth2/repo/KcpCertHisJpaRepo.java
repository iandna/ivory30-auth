package com.iandna.oauth2.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.iandna.oauth2.entity.KcpCertHis;

public interface KcpCertHisJpaRepo extends JpaRepository<KcpCertHis, String> {
    Optional<KcpCertHis> findByUid(String uid);
    Optional<KcpCertHis> findTopByUidOrderBySeqNoDesc(String uid);
    KcpCertHis save(KcpCertHis kcpCertHis);
}
