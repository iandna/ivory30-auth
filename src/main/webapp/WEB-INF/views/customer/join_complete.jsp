<%@ page language="java" contentType="text/html;charset=utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=yes" />
<title>Insert title here</title>
<script src="/oauth/resources/js/jquery-3.4.1.min.js"></script>
<script type='text/javascript' src='/oauth/resources/js/common.js?date=20221206111700'></script>
<script type='text/javascript' src='/oauth/resources/js/api.js?date=20221206111700'></script>
<style>
  .loading_wrap{position:fixed; top:0px; left:0px; background-color:rgba(255,255,255,0); width:100%; height:100%; z-index:990;}
  .loading-circle{ position:absolute; margin:auto; top:0; bottom:0; left:0; right:0; width:54px; height:54px; animation: rotate_image .6s linear infinite; transform-origin: 50% 50%;}
  .loading_point{position:absolute; margin:auto; top:0; bottom:0; left:0; right:0; width:54px; height:54px;}
   @keyframes rotate_image{100% { transform: rotate(360deg);}}
</style>
</head>
<body>
	<!-- 
	<iframe id="iframe" src="http://skin-mobile22--shop1.iandna77.cafe24.com/application/ivory40/ssotest.html?status=welcome" 
	sandbox="allow-scripts allow-popups allow-same-origin allow-modals allow-forms" ></iframe>
	 -->
	<div class="loading_wrap">
		<img class="loading-circle"
			src="https://iandna.s3-ap-northeast-1.amazonaws.com/ivory3.0/common/loading-circle.png"
			alt="Loading..." /> <img class="loading_point"
			src="https://iandna.s3-ap-northeast-1.amazonaws.com/ivory3.0/common/loading_point.png"
			alt="Loading..." />
	</div>
</body>
<script>
$(document).ready(function(){
	/* 
    window.addEventListener('message', function(event){
	  if ( event.data.from == 'child'){
	    if ( event.data.join == 'complete') {
	       console.log('complete');
	    } else {
	       console.log('error');
	    }
	  }
	});
     */

	//window.location.replace('https://m.i-vory.shop/application/ivory40/ssotest.html?returnUrl=/application/ivory40/joinProxy.html&toMoveLoginCheckModule');
	if(SVR_URL.indexOf('iandna-ivory.com') > 0) 
		//window.location.replace('https://m.i-vory.shop/application/ivory40/ssoLoginTest.html?returnUrl=/application/ivory40/joinProxyTest.html&toMoveLoginCheckModule');
		window.location.replace('/oauth/authorize?response_type=code&client_id=testClientId&client_secret=$2a$10$1qw9m8GTeZPC2hBAVAJfE.RTh1qAMtBx/LwCqAgDyTA0gkThQPcoK&redirect_url=https://iandna-ivory.com/oauth/callback&state=' + localStorage.getItem('state'));
	else
		window.location.replace('https://m.i-vory.shop/application/ivory40/ssoLogin.html?returnUrl=/application/ivory40/joinProxy.html&toMoveLoginCheckModule');
});
</script>
</html>
