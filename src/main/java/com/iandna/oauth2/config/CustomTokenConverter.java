package com.iandna.oauth2.config;

import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;


@Component
public class CustomTokenConverter extends DefaultAccessTokenConverter {
    private Map checkTokenResult;
    @Override
    public OAuth2Authentication extractAuthentication(Map<String, ?> map) {
        OAuth2Authentication auth = super.extractAuthentication(map);
        auth.setDetails(map);
        this.checkTokenResult = new HashMap(map);
        return auth;
    }
    public Map getCheckTokenResult() {
        return checkTokenResult;
    }
    public void setCheckTokenResult(Map checkTokenResult) {
        this.checkTokenResult = checkTokenResult;
    }
}
