package com.iandna.oauth2.cookie;

import com.iandna.oauth2.entity.AuthLog;
import com.iandna.oauth2.entity.Customer;
import com.iandna.oauth2.repo.AuthLogJpaRepo;
import com.iandna.oauth2.util.CustomHttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CustomLogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler implements LogoutSuccessHandler {
	private static final Logger LOG = LoggerFactory.getLogger(RedirectToOriginalUrlAuthenticationSuccessHandler.class);
	@Autowired
    private AuthLogJpaRepo authLogJpaRepo;

    public CustomLogoutSuccessHandler() {
    	super();
    	this.setTargetUrlParameter("target");
    }

    @Override
    public void onLogoutSuccess(final HttpServletRequest request, final HttpServletResponse response, final Authentication authentication) throws IOException, ServletException {
        /*
    	final Customer userInfo = (Customer) authentication.getPrincipal();

        authLogJpaRepo.save(AuthLog.builder()
                .uid(userInfo.getUsername())
                .action("2")    //logout
                .build()
        );
        */
    	LOG.info("#### logoutSuccessHandler");
    	LOG.info(request.getRequestURI());
    	LOG.info(request.getParameter("target"));
    	LOG.info(response.getHeader("REFERER"));
		CustomHttpServletRequest cRequest = new CustomHttpServletRequest(request);
		if(request.getHeader("REFERER") == null &&
				(request.getParameter("target") == null || request.getParameter("target").isEmpty() || request.getParameter("target").equals("/oauth/login"))
		) {
			cRequest.setParameter("target", "/oauth/login");
			super.onLogoutSuccess(cRequest, response, authentication);
		}else if(request.getHeader("REFERER") != null && request.getHeader("REFERER").contains("i-vory.shop")) {
			cRequest.setParameter("target", "https://m.i-vory.shop/application/30/proxy_logout.html");
			super.onLogoutSuccess(cRequest, response, authentication);
		}
		else
    		super.onLogoutSuccess(request, response, authentication);
    }

}
